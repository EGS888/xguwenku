

/*********************main  页面初始化***********************/
$(function(){

    //诗词全集btnNav==>
    $("#nav-header-id-shici").click(function(){

        $(this).addClass("active");
        $("#nav-header-id-book").removeClass("active");

        //诗词名句 -- 古籍名句
        $("#header-random-text-shici").removeClass("egs-nav-invisible");
        $("#header-random-text-book").addClass("egs-nav-invisible");

        //诗词body -- 古籍body
        $("#mainbody-shici").removeClass("egs-nav-invisible");
        $("#mainbody-book").addClass("egs-nav-invisible");

        //背景色==诗词
        $("body").css("background-color","#f2f5e9");

        //清除
        $("#search-btn-id").removeClass("active");
        $("#search-btn-id .fa-search").removeClass("fa-remove");
        $("#search-box-id-shici").removeClass("active");
        $("#search-box-id-book").removeClass("active");
    });

    //古典书籍btnNav==>
    $("#nav-header-id-book").click(function(){

        $(this).addClass("active");
        $("#nav-header-id-shici").removeClass("active");

        //诗词名句 -- 古籍名句
        $("#header-random-text-shici").addClass("egs-nav-invisible");
        $("#header-random-text-book").removeClass("egs-nav-invisible");

        //诗词body -- 古籍body
        $("#mainbody-shici").addClass("egs-nav-invisible");
        $("#mainbody-book").removeClass("egs-nav-invisible");

        //背景色==书籍
        $("body").css("background-color","rgb(249, 252, 249)");

        //清除
        $("#search-btn-id").removeClass("active");
        $("#search-btn-id .fa-search").removeClass("fa-remove");
        $("#search-box-id-shici").removeClass("active");
        $("#search-box-id-book").removeClass("active");
    });

    //搜索按钮searchBtn==>
    $("#search-btn-id").click(function(){

        if($("#mainbody-shici").hasClass("egs-nav-invisible")){
            //书籍

            if($(this).hasClass("active"))
            {

                $(this).removeClass("active");
                $("#search-btn-id .fa-search").removeClass("fa-remove");
                $("#search-box-id-book").removeClass("active");
            }
            else
            {

                $(this).addClass("active");
                $("#search-btn-id .fa-search").addClass("fa-remove");
                $("#search-box-id-book").addClass("active");
            }


        }
        else{

            //诗词
            if($(this).hasClass("active"))
            {

                $(this).removeClass("active");
                $("#search-btn-id .fa-search").removeClass("fa-remove");
                $("#search-box-id-shici").removeClass("active");
            }
            else
            {

                $(this).addClass("active");
                $("#search-btn-id .fa-search").addClass("fa-remove");
                $("#search-box-id-shici").addClass("active");
            }
        }

    });


    // check if Vocapp is triggered
// if so, lookup and display definition
    $(document).mouseup(function (event) {
        var selectedText = getSelectionText();

        if (clickWithinVocapp(event)) {
            lookupAndSave(selectedText, true, event); // handle lookup within lookup
        } else { // handle standlone lookup (not within Vocapp)
            markSelection();
            //createVocapp();
            lookupAndSave(selectedText, false, event);
        }
    });

    //遮罩层，点击就让set消失&&快捷搜索框---hide
    $("#bg-mask").click(function(){

       $("#bg-mask").css("display","none");
        $(".result-box-all").css("display","none");
    });
});


/********************诗词全库:快速搜索********************/

var int_interval, text_input;

//==诗词，快捷搜索框---show
function focusShiciSearchBox()
{

    $(".result-box-all").css("display","block");
    $("#bg-mask").css("display","block");

    if("undefined" == typeof int_interval
    || 0 == int_interval)
        int_interval = setInterval('searchByAjax()',2000);
}
//==诗词，stop定时器
function blurShiciSearchBox()
{
    clearInterval(int_interval);
    int_interval = 0;
}

//==诗词，快捷索引搜索
function searchByAjax()
{
    var newInputTemp = $("#search-input-shici").val();

    if (text_input != newInputTemp
    &&newInputTemp!=0)
    {
        text_input = newInputTemp;

        $(".result-box").html('<div class="r-section"> <div class="mleft"><span style="margin-left: 15px">诗文</span></div> <div class="mright"> <div class="adiv" id="adiv0"style="background-color: rgb(251, 250, 246);"> <a hidefocus="" id="aa0"href="http://so.gushiwen.org/view_7886.aspx">见京兆韦参军<spanstyle=" color:#275F38; font-weight:bold;">量</span>移东阳二首<span> - 李白</span></a> </div> </div> </div>');

        console.log(int_interval+text_input);
    }
}

/*********************healper method**********************/

function getSelecStr() {

    if (document.getSelection) {

        return document.getSelection().toString();
    }
    else if (document.selection.document.selection
        && document.selection.createRange) {

        return document.selection.createRange.text();

    }
    else {

        return null;
    }
}


