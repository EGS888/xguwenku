

/*********************main  页面初始化***********************/
$(function(){

    //焚香btnNav==>
    $("#nav-header-id-fenxiang").click(function(){

        $('> div',this).addClass("nav-choosed");
        $('> div',"#nav-header-id-huacao").removeClass("nav-choosed");
        $('> div',"#nav-header-id-jiucha").removeClass("nav-choosed");

        //焚香headbar -- 主题背景&摘要
        $("#header-random-text-fenxiang").removeClass("egs-nav-invisible");
        $("#header-random-text-huacao").addClass("egs-nav-invisible");
        $("#header-random-text-jiucha").addClass("egs-nav-invisible");

        //焚香body -- 显示
        $("#mainbody-fenxiang").removeClass("egs-nav-invisible");
        $("#mainbody-huacao").addClass("egs-nav-invisible");
        $("#mainbody-jiucha").addClass("egs-nav-invisible");


    });

    //花草btnNav==>
    $("#nav-header-id-huacao").click(function(){

        $('> div',this).addClass("nav-choosed");
        $('> div',"#nav-header-id-fenxiang").removeClass("nav-choosed");
        $('> div',"#nav-header-id-jiucha").removeClass("nav-choosed");

        //花草headbar -- 主题背景&摘要
        $("#header-random-text-huacao").removeClass("egs-nav-invisible");
        $("#header-random-text-fenxiang").addClass("egs-nav-invisible");
        $("#header-random-text-jiucha").addClass("egs-nav-invisible");

        //花草body -- 显示
        $("#mainbody-huacao").removeClass("egs-nav-invisible");
        $("#mainbody-fenxiang").addClass("egs-nav-invisible");
        $("#mainbody-jiucha").addClass("egs-nav-invisible");

    });

    //酒茶btnNav==>
    $("#nav-header-id-jiucha").click(function(){

        $('> div',this).addClass("nav-choosed");
        $('> div',"#nav-header-id-huacao").removeClass("nav-choosed");
        $('> div',"#nav-header-id-fenxiang").removeClass("nav-choosed");

        //酒茶headbar -- 主题背景&摘要
        $("#header-random-text-jiucha").removeClass("egs-nav-invisible");
        $("#header-random-text-huacao").addClass("egs-nav-invisible");
        $("#header-random-text-fenxiang").addClass("egs-nav-invisible");

        //酒茶body -- 显示
        $("#mainbody-jiucha").removeClass("egs-nav-invisible");
        $("#mainbody-huacao").addClass("egs-nav-invisible");
        $("#mainbody-fenxiang").addClass("egs-nav-invisible");

    });

});


