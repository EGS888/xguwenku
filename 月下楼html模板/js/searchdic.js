/**
 * Created by Egs-Studio on 15/9/19.
 */


/************************************** Global Vars **************************************/

var markerId = "vocapp-marker";
var markerTextChar = "\ufeff";
var markerTextCharEntity = "&#xfeff;";

var selectedTextDimensions;
var vocappViewPortOffsetMin = 10;
var vocappArrowOffsetMin = 5;
var vocappLoadingWidth = 40;
var vocappArrowWidth = 28;
var vocappArrowHeight = 8;

var ajaxRequest;
var language;

/************************************** Init **************************************/



/************************************** Audio **************************************/



/**
 * Play word pronunciation audio
 * @param {String} url for audio
 */
//function playSound(soundfile) {
//    if (document.getElementById("vocapp-audio-dummy").src != soundfile) {
//        document.getElementById("vocapp-audio-dummy").src = soundfile;
//    }
//    document.getElementById('vocapp-audio-dummy').play();
//}

/************************************** Selection text, dimens, marker **************************************/

/**
 * Get currently selected text
 * @return {String} selected text (trimmed, but not lower-cased)
 */
function getSelectionText() {
    var text = "";
    if (window.getSelection) {
        text = window.getSelection().toString();
    } else if (document.selection && document.selection.type != "Control") {
        text = document.selection.createRange().text;
    }

    return text.trim();
}

/**
 * Get selection dimentions
 * @return {Object} {width, height, left viewport offset, right viewport offset}
 */
function getSelectionDimensions() {
    var
        sel = document.selection,
        range;
    var
        width = 0,
        height = 0,
        left = 0,
        top = 0;

    if (sel) {
        if (sel.type != "Control") {
            range = sel.createRange();
            width = range.boundingWidth;
            height = range.boundingHeight;
            left = range.boundingLeft;
            top = range.boundingTop;
        }
    } else if (window.getSelection) {
        sel = window.getSelection();
        if (sel.rangeCount) {
            range = sel.getRangeAt(0).cloneRange();
            if (range.getBoundingClientRect) {
                var rect = range.getBoundingClientRect();
                width = rect.right - rect.left;
                height = rect.bottom - rect.top;
                left = rect.left;
                top = rect.top;
            }
        }
    }
    return {
        width: width,
        height: height,
        left: left,
        top: top
    };
}

/**
 * Create an empty span right next to selected text
 * so Vocapp can keep track of current viewport position of selection
 * when positioning Vocapp box
 */
function markSelection() {
    var markerEl;
    var sel, range;

    if (document.selection && document.selection.createRange) {
        // Clone the TextRange and collapse
        range = document.selection.createRange().duplicate();
        range.collapse(false);
        // Create the marker element containing a single invisible character by creating literal HTML and insert it
        range.pasteHTML(
            '<span id="' + markerId + '" style="position: relative;">' +
            markerTextCharEntity +
            '</span>'
        );
        markerEl = document.getElementById(markerId);
    } else if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt) {
            range = sel.getRangeAt(0).cloneRange();
        } else {
            // Older WebKit doesn't have getRangeAt
            range.setStart(sel.anchorNode, sel.anchorOffset);
            range.setEnd(sel.focusNode, sel.focusOffset);

            // Handle the case when the selection was selected backwards (from the end to the start in the
            // document)
            if (range.collapsed !== sel.isCollapsed) {
                range.setStart(sel.focusNode, sel.focusOffset);
                range.setEnd(sel.anchorNode, sel.anchorOffset);
            }
        }
        range.collapse(false);

        // Create the marker element containing a single invisible character using DOM methods and insert it
        markerEl = document.createElement("span");
        markerEl.id = markerId;
        markerEl.appendChild(document.createTextNode(markerTextChar));
        range.insertNode(markerEl);
    }
}

/************************************** Display / Dismiss / Position **************************************/

/**
 * Create empty vocapp box and add it to document
 * not displayed yet
 */
function createVocapp() {
    //vocapp = document.createElement("div");
    //vocapp.id = "vocapp";
    //vocappContent = document.createElement("div");
    //vocappContent.id = "vocapp-content";
    //vocapp.appendChild(vocappContent);
    //document.body.appendChild(vocapp);
    //
    //// add vocapp arrow and loading images
    //$("#vocapp").html($("#vocapp").html() +
    //"<img id='vocapp-arrow' height='10' width='27'>");
    //$("#vocapp").html($("#vocapp").html() +
    //"<img id='vocapp-within-loading'>");
    //var imgURL = chrome.extension.getURL("img/loading-within.gif");
    //document.getElementById("vocapp-within-loading").src = imgURL;
}

/**
 * Remove loading icon, marker, vocapp box
 * Reset vocapp opcaition and display
 * Clear ajaxRequest, selectedTextDimensions and bottomOffset
 */
function dismissVocapp() {
    //if (ajaxRequest) {
    //    ajaxRequest.abort();
    //    ajaxRequest = null;
    //}
    //
    //$("#vocapp-loading").remove();
    $("#vocapp-marker").remove();
    //$("#vocapp").css("opacity", "0");
    //$("#vocapp").css("display", "none");
    //$("#vocapp").remove();
    selectedTextDimensions = null;
    bottomOffset = null;

    //vocapp-arrow
    $("#vocapp-loading").css("display", "none");

}

/**
 * Fade out loading, then fade in Vocapp box
 */
function displayVocapp() {
    // fade out and remove loading icon
    $("#vocapp-loading").fadeTo('fast', 0);
    //setTimeout(function () {
    //    $("#vocapp-loading").remove()
    //}, 200);

    // fade in vocapp box
    $("#vocapp").css("display", "block");
    $("#vocapp").delay(200).fadeTo('fast', 1);

    // adjust css for chinese translation
    if (language != "english") {
        $("#vocapp").css("line-height", "170%");
        $(".vocapp-speech").css("margin-top", "11px");
        $(".vocapp-syn").css("margin-top", "11px");
    }

    // remind unlogged in user to login every hour
    //chrome.runtime.sendMessage({
    //    method_name: 'check_login'
    //}, function (response) {
    //    if (!response.result) {
    //        alert("Please login Vocapp Chrome extension by clicking the \"v\" icon in your toolbar to enable automatic word list.");
    //    }
    //});
}

/**
 * Display vocapp loading during lookup
 */
function displayLoading(withinVocapp) {
    //if (!withinVocapp) {
    //    var vocappLoading = document.createElement("div");
    //    vocappLoading.id = "vocapp-loading";
    //    document.body.appendChild(vocappLoading);
    //    $("#vocapp-loading").html($("#vocapp-loading").html() +
    //    "<img id='vocapp-loading-img'>");
    //    $("#vocapp-loading").html($("#vocapp-loading").html() +
    //    "<img id='vocapp-loading-arrow'>");
    //
    //    var imgURL = chrome.extension.getURL("img/blue-dictionary-loading.gif");
    //    document.getElementById("vocapp-loading-img").src = imgURL;
    //
    //    imgURL = chrome.extension.getURL("img/V.png");
    //    document.getElementById("vocapp-loading").style.backgroundImage = imgURL;
    //
    //    imgURL = chrome.extension.getURL("img/blue-arrow.png");
    //    document.getElementById("vocapp-loading-arrow").src = imgURL;

    positionVocapp();
    //} else {
    //    $("#vocapp-within-loading").css("left", $("#vocapp-arrow").css("left"));
    //    $("#vocapp-within-loading").css("display", "block");
    //}
}

/**
 * Put vocapp box and vocapp arrow in correct position
 * vocapp box is positioned relatived to viewport
 * vocapp arrow is relative to vocapp box
 */
function positionVocapp() {
    // fetch global var selectedTextDimensions
    // if it doesn't exist
    if (!selectedTextDimensions) {
        selectedTextDimensions = getSelectionDimensions();
    }

    // position vocapp loading icon  #vocapp-loading
    var vocappLoadingEl = $("#vocapp-loading");
    vocappLoadingEl.css("display","block");
    if (vocappLoadingEl) {

        var markerBouding = document.getElementById(markerId).getBoundingClientRect();
        //vocappLoadingEl.style.left =
        //    Math.max(
        //        (markerBouding.left - vocappLoadingWidth / 2 - selectedTextDimensions.width / 2),
        //        vocappViewPortOffsetMin
        //    ) + "px";
        //vocappLoadingEl.style.top =
        //    (markerBouding.top + selectedTextDimensions.height + vocappArrowHeight) + "px";

    //    new settmethod
        vocappLoadingEl.css("left",Math.max(
            (markerBouding.left - vocappLoadingWidth / 2 - selectedTextDimensions.width / 2),
            vocappViewPortOffsetMin
        ) + "px");

        vocappLoadingEl.css("top",(markerBouding.top + selectedTextDimensions.height + vocappArrowHeight) + "px")
    }

    // position vocapp box   #vocapp
    var vocappEl = document.getElementById("vocapp");
    if (vocappEl) {
        var markerBounding = document.getElementById(markerId).getBoundingClientRect();
        var vocappWidth = $("#vocapp").width();

        // left offset for vocapp box
        var vocappOriginalLeftOffset = markerBounding.left - vocappWidth / 2 - selectedTextDimensions.width / 2;
        var vocappAdjustedLeftOffset = Math.max(vocappOriginalLeftOffset, vocappViewPortOffsetMin);
        vocappEl.style.right = "auto";
        vocappEl.style.left = vocappAdjustedLeftOffset + "px";

        // left offset for vocapp arrow
        var vocappArrowLeft =
            Math.max(
                vocappWidth / 2 - vocappArrowWidth / 2 - (vocappAdjustedLeftOffset - vocappOriginalLeftOffset),
                vocappArrowOffsetMin
            );
        vocappArrowLeft = Math.min(vocappArrowLeft, vocappWidth - vocappArrowWidth - vocappArrowOffsetMin);
        $("#vocapp-arrow").css("left", vocappArrowLeft + "px");

        // make sure vocapp box's right side part is within viewport
        if (vocappAdjustedLeftOffset + vocappWidth > $(window).width() - vocappViewPortOffsetMin) {
            vocappEl.style.left = "auto";
            vocappEl.style.right = "10px";

            var leftOffsetOfRightEdgeOfVocappBox = markerBounding.left - selectedTextDimensions.width / 2 + vocappWidth / 2;
            var maxLeftOffsetOfRightEdgeOfVocappBox = $(window).width() - vocappViewPortOffsetMin;
            $("#vocapp-arrow").css("left",
                Math.max(
                    (
                    vocappWidth / 2 - vocappArrowWidth / 2 +
                    (leftOffsetOfRightEdgeOfVocappBox - maxLeftOffsetOfRightEdgeOfVocappBox)
                    ),
                    vocappArrowOffsetMin
                ) + "px");
        }

        $("#vocapp-content").css("max-height", "280px");
        vocappEl.style.top = (markerBounding.top + selectedTextDimensions.height + vocappArrowHeight) + "px";
        vocappEl.style.bottom = "auto";
        //var imgURL = chrome.extension.getURL("img/arrow.png");
        //document.getElementById("vocapp-arrow").src = imgURL;
        $("#vocapp-arrow").css("top", "-10px");
        $("#vocapp-arrow").css("bottom", "auto");
    }
}

/************************************** Lookup and save **************************************/

function lookupAndSave(selectedText, withinVocapp, event) {
    displayLoading();

    var ret = "";
    var url = "https://www.getvocapp.com/lookup_1_3_9/" + language + "/" + selectedText.toLowerCase();

    ajaxRequest = $.ajax({
        url: url,
    }).done(function (obj) {
        var word = obj.word;
        var pronunciation = obj.pronunciation;
        var audio = obj.audio;
        var definitions = obj.definitions;
        var notFound = Object.keys(definitions).length == 0;
        var synonyms = obj.synonyms;

        // add word, pronunciation and audio into HTML
        if (withinVocapp || !notFound) {
            ret = ret + "<div class='vocapp-word'>" + word;
            if (pronunciation) {
                ret = ret +
                "<span class='vocapp-pronunciation'>" +
                "/" + pronunciation + "/" +
                "</span>";
            }
            if (audio) {
                document.getElementById("vocapp-audio-dummy").src = audio;
                ret = ret +
                "<span class='vocapp-audio'>" +
                "&nbsp&nbsp&nbsp" +
                "<input type='hidden' value='" + audio + "'>" +
                "</span>";
            }
            ret = ret + "<div>";

            // add space above definitions for non-english language
            if (language != "english") {
                ret = ret + "<div class='vocapp-speech'></div>";
            }
        }

        // add definitions into HTML
        if (!notFound) {
            for (var pos in definitions) {
                // english
                if (language == "english") { // english
                    // pos
                    ret = ret +
                    "<div class='vocapp-speech'>" +
                    pos +
                    "</div>";
                    // meanings
                    for (i = 0; i < definitions[pos].length; i++) {
                        ret = ret +
                        "<div class='vocapp-def'>" +
                        definitions[pos][i] +
                        "</div>";
                    }
                } else { // chinese
                    // pos + meaning
                    ret = ret +
                    "<div class='vocapp-def'>" +
                    pos + " " + definitions[pos].join("; ") +
                    "</div>";
                }
            }

            // synonym
            if (synonyms.length > 0) {
                var synClass = "vocapp-syn-label";
                if (language != "english") {
                    synClass = "vocapp-syn-label-chin";
                }
                var synLabel = "synonyms: ";
                if (language != "english") {
                    synLabel = "同义词: ";
                }
                var str =
                    "<span class='" + synClass + "'>" +
                    synLabel +
                    "</span>" + synonyms[0];
                for (i = 1; i < synonyms.length; i++) {
                    str = str + ", " + synonyms[i];
                }

                ret = ret +
                "<div class='vocapp-syn'>" +
                str +
                "</div>";
            }
        } else {
            ret = ret + "No result found.";
        }

        if (withinVocapp) {
            // dismiss loading within vocapp
            $("#vocapp-within-loading").css("display", "none");
            // add new words' definiton into vocapp
            $("#vocapp-content").html(ret + "<hr class='vocapp-line'>" + $("#vocapp-content").html());
            // reposition vocapp incase width of box changed
            positionVocapp();
            // scroll to newly looked up word
            $("#vocapp-content").animate({
                scrollTop: 0
            }, 500);
        } else {
            // fill in html and display vocapp box
            if (ret != "") {
                $("#vocapp-content").html(ret);
                displayVocapp();
                positionVocapp();
            }
            // save word and context into database
            page_url = event.currentTarget.URL;
            if (!notFound) {
                context = event.target.innerText;
                chrome.runtime.sendMessage({
                    method_name: 'save_word',
                    selected: selectedText,
                    word: word,
                    pronun: pronunciation,
                    audio: audio,
                    url: page_url,
                    context: context
                }, function (response) {});
            }
        }

        // add click handler to newly added audio button
        $(".vocapp-audio").click(function () {
            playSound($(this).children()[0].value);
        });
    });
}

/************************************** User action handlers **************************************/

/**
 * Check if user clicked within Vocapp box
 * @return {Boolean} true if clicked within Vocapp box
 */
function clickWithinVocapp(event) {
    return $("#vocapp").is(event.target) || $("#vocapp").has(event.target).length != 0;
}

// position Vocapp definition box
$(window).resize(function () {
    positionVocapp();
});

$(window).bind('mousewheel', function (event) {
    positionVocapp();
});

$(window).keydown(function () {
    positionVocapp();
});

$(window).keyup(function () {
    positionVocapp();
});

// remember if mouse down on an input field
var mouseDownInput = false;
$(document).mousedown(function (event) {
    mouseDownInput = $(event.target).is("input") || $(event.target).is("textarea");

    // if left click outside of Vocapp definition box
    // dismiss Vocapp box
    if (!clickWithinVocapp(event) && event.which == 1) {
        dismissVocapp();
    }
});

