//
//  shiciPoetObj.h
//  Script4_gushiwen.org
//
//  Created by Engels on 15-3-4.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface shiciPoetObj : NSObject

@property(nonatomic,strong)NSString *poetName;

@property(nonatomic,assign)NSInteger poetNum;

@property(nonatomic,strong)NSString *poetIntro;

@property(nonatomic,strong)NSString *poetHeadUrl;

@end
