//
//  shiciPoetObj.m
//  Script4_gushiwen.org
//
//  Created by Engels on 15-3-4.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import "shiciPoetObj.h"
#import "LKDBHelper.h"

@implementation shiciPoetObj

//重载选择 使用的LKDBHelper
+(LKDBHelper *)getUsingLKDBHelper
{
    static LKDBHelper* db;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString* dbpath = [NSHomeDirectory() stringByAppendingPathComponent:@"db/TaiXuWen.db"];
        db = [[LKDBHelper alloc] initWithDBPath:dbpath];
    });
    return db;
}

+(NSString *)getTableName
{
    //古书籍 翻译表
    return @"shiciPoetTable";
}


//列属性
+(void)columnAttributeWithProperty:(LKDBProperty *)property
{
    if([property.sqlColumnName isEqualToString:@"poetNum"])
    {
        property.isUnique = YES;
        property.isNotNull = YES;
    }
    
    
    
    
    if([property.sqlColumnName isEqualToString:@"poetName"]
       ||[property.sqlColumnName isEqualToString:@"poetIntro"])//当前篇内容页面内容
    {
        property.isNotNull = YES;
    }
    
    if([property.sqlColumnName isEqualToString:@"rowid"])
    {
        property.isUnique = YES;
        property.isNotNull = YES;
    }
    
}



//已经插入数据库
+(void)dbDidInserted:(NSObject *)entity result:(BOOL)result
{
    LKErrorLog(@"did insert : %@",NSStringFromClass(self));
}


@end
