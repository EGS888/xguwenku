//
//  ApiClient_Spider4Gushiwen.h
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-4.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"
#import "TFHpple.h"

#define  HttpVerClient  [ApiClient_Spider4Gushiwen shareClient]

typedef void(^backArr)(BOOL bol,id responseData);

typedef void(^backArr_3)(id tagWrong ,id tagSon1,id tagSon2);

typedef void(^backArr_son5)(id tagWrong ,id tagSon5);

typedef void(^backArr_gujiChapter)(BOOL bol,id tagSon2,id tagBookcont);

typedef void(^backArr_gujiBookV)(BOOL bol,id Shileft);

typedef void(^backArr_gujiAuthor)(BOOL bol,TFHppleElement * tagSon1,TFHppleElement * tagSon2);

typedef void(^backArr_gujiBFanyiPage)(BOOL bol,TFHppleElement * shangxicontObjSame);

@interface ApiClient_Spider4Gushiwen : AFHTTPRequestOperationManager

+(ApiClient_Spider4Gushiwen *)shareClient;

-(void)GetHTML_Mingju:(NSString *)queryURI
                     :(backArr)blk;

-(void)GetHTML_ShiWen_simple:(NSString *)queryURI
                            :(backArr)blk;



-(void)GetHTML_ShiWen_shiwenView:(NSString *)queryURI
                                :(backArr_3)blk;

-(void)GetHTML_ShiWen_shiwenView_fanyiShangxi:(NSString *)queryURI
                                             :(backArr_son5)blk;

//guji verb
-(void)GetHTML_guji_simple:(NSString *)queryURI
                          :(backArr)blk;

//book 章节页面
-(void)GetHTML_guji_Chapter:(NSString *)queryURI
                           :(backArr_gujiChapter)blk;

//book 篇章内容页面
-(void)GetHTML_guji_BookV:(NSString *)queryURI
                         :(backArr_gujiBookV)blk;
//book 作者 页面
-(void)GetHTML_guji_Author:(NSString *)queryURI
                          :(backArr_gujiAuthor)blk;
//poetry  poet 诗人 页面
-(void)GetHTML_poet_Author:(NSString *)queryURI
                          :(backArr_gujiAuthor)blk;

//book 篇章内容  翻译页面(。。。赏析也可以共用。。。)
-(void)GetHTML_guji_bFanyiPage:(NSString *)queryURI
                              :(backArr_gujiBFanyiPage)blk;

@end
