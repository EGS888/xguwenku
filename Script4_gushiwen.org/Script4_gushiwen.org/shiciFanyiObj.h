//
//  shiciFanyiObj.h
//  Script4_gushiwen.org
//
//  Created by EGS on 15-3-3.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface shiciFanyiObj : NSObject

@property (nonatomic,assign)long shiwenNum;//诗词序号num----从属于

@property (nonatomic,assign)long shiwenFanyiNum;//诗文翻译序号
//诗文翻译标题
@property (nonatomic,strong)NSString * poetryFanyiTitle;//很大可能同名
//正文
@property (nonatomic,strong)NSString * poetryFanyiText;

@end
