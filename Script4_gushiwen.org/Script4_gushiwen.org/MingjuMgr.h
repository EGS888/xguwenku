//
//  MingjuMgr.h
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-9.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MingjuMgr : NSObject

+(MingjuMgr *)shareClient;

 
//启动catch。===main
-(void)runCatchOn:(NSInteger)currPage
                 :(NSInteger)finishPage;

//标签tag 上 ===
-(void)loopTag4Mingju:(NSInteger)indexA
                     :(NSInteger)indexB;

//启动catch。===》分类情况获取
-(void)runCatchOn:(NSInteger)currPage
                 :(NSString *)categoryA
                 :(NSString *)categoryB
                 :(NSInteger)indexA
                 :(NSInteger)indexB;


@end
