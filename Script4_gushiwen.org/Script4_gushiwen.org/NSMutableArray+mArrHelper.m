//
//  NSMutableArray+mArrHelper.m
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-10.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import "NSMutableArray+mArrHelper.h"

@implementation NSArray (mArrHelper)

-(BOOL)checkExist:(NSString *)valueStr
{
    for (NSString *strObj in self) {
        
        if ([strObj isEqualToString:valueStr]) {
            
            return  YES;
        }
        
    }

    return NO;
}

@end
