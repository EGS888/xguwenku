//
//  GujiMgr.h
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-9.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GujiMgr : NSObject

+(GujiMgr *)shareClient;

//注意： 手动切换A B cate
-(void)loopTag4Guji:(NSInteger)indexA;


-(void)runCatchOn:(NSInteger)indexGo;

//==get 篇内容页面
@property(nonatomic,strong)NSArray *allArr_bookV_num;

-(void)loopBookV_byColletionNum;

-(void)runCatchOn_articePage:(NSInteger)bookv_num;

-(void)loopAuthorNum:(NSInteger)indexGo;
-(void)runOnAllBook_authorObj:(NSInteger)authorNum
                             :(NSInteger)indexGo;

@property(nonatomic,strong)NSArray *authorNumArr;

//#pragma mark 获取古籍篇章的  翻译page
-(void)runCatchOn_fanYiPage:(NSInteger)bFanyiNum;

-(void)runFix_fanyiPage;


//下载书籍封面咯，，，
@property(nonatomic,strong)NSMutableArray *mArr_bookcover;
-(void)loop_download_bookcover:(int)index;

//下载书籍 作者头像咯，，，
@property(nonatomic,strong)NSMutableArray *mArr_authorHead;
-(void)loop_download_authorHead:(int)index;

@end
