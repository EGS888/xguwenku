//
//  gujiPageObj.h
//  Script4_gushiwen.org
//
//  Created by EGS on 15-3-1.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 古籍pageContent 内容页面。
 */

@interface gujiPageObj : NSObject

@property (nonatomic,assign)long bookNum;//古籍序号num

@property (nonatomic,assign)long authorNum;//作者序号num

//书籍标题  1. 唯一值。
@property (nonatomic,strong)NSString * bookName;//很大可能同名
//书籍封面
@property (nonatomic,strong)NSString * bookCoverUrl;//很大可能同名

@property (nonatomic,assign)long bookV_num;//古籍篇章序号 bookV_num  !!!!!
@property (nonatomic,strong)NSString * bookV_title;//篇章title
//key:古籍篇内容page。!!!!!!
@property (nonatomic,strong)NSString *bookV_PageText;

//作者
@property (nonatomic,strong)NSString * authorName;
@property (nonatomic,strong)NSString * authorHeadUrl;

//翻译===
@property (nonatomic,strong)NSArray *FanYi_List;

//赏析===
@property (nonatomic,strong)NSArray *Shangxi_List;

@end
