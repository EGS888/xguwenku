//
//  shiciObj.h
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-10.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LKDBHelper.h"


@interface shiciObj : NSObject


@property (nonatomic,assign)long shiwenNum;//诗词序号num

@property (nonatomic,assign)long poetNum;//作者序号num

//诗文标题   
@property (nonatomic,strong)NSString * poetryTitle;//很大可能同名
//摘要
@property (nonatomic,strong)NSString * poetrySnippet;
//正文
@property (nonatomic,strong)NSString * poetryText;
//作者
@property (nonatomic,strong)NSString * poetName;
@property (nonatomic,strong)NSString * poetHeadUrl;

//几种翻译===
@property (nonatomic,strong)NSArray *FanYi_List;
//几种赏析===
@property (nonatomic,strong)NSArray *ShangXi_List;

@property (nonatomic,strong)NSArray *cate_LeiXing;//a分类
@property (nonatomic,strong)NSString *str_ChaoDai;//b分类====只有一种朝代的可能
@property (nonatomic,strong)NSArray *cate_XingShi;//c分类

@end
