//
//  gujiAuthorObj.h
//  Script4_gushiwen.org
//
//  Created by Engels on 15-3-4.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface gujiAuthorObj : NSObject

@property(nonatomic,strong)NSString *authorName;

@property(nonatomic,assign)NSInteger authorNum;

@property(nonatomic,strong)NSString *authorIntro;

@property(nonatomic,strong)NSString *authorHeadUrl;

@end
