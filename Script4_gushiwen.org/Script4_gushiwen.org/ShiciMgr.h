//
//  ShiciMgr.h
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-9.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShiciMgr : NSObject

+(ShiciMgr *)shareClient;

// 普通 get
-(void)runCatchOn:(NSInteger)currPage
                 :(NSInteger)finishPage;

//tag 叠加   get
-(void)loopTag4Shiwen:(NSInteger)indexA
                     :(NSInteger)indexB
                     :(NSInteger)indexC;

//tag 单个   get==========AAAAAAAAAA
-(void)loop_oneTag:(NSInteger)currPage
                  :(NSInteger)finishPage
                  :(NSInteger)indexA;

//tag 在一个tag捕获 objShiwen========BBBBBBBB
-(void)loop_oneTag_B:(NSInteger)currPage
                    :(NSInteger)finishPage
                    :(NSInteger)indexB;

//tag 在一个tag捕获 objShiwen========CCCCCCCC
-(void)loop_oneTag_C:(NSInteger)currPage
                    :(NSInteger)finishPage
                    :(NSInteger)indexC;


//针对唐宋两朝作者===单个作者捕获===》
-(void)loop_oneTag_tangPoetName:(NSInteger)currPage
                           :(NSInteger)indexPoet;
-(void)loop_oneTag_songPoetName:(NSInteger)currPage
                               :(NSInteger)indexPoet;
@property(strong,nonatomic)NSMutableArray *mArr_allPoet_tang;//唐诗
@property(strong,nonatomic)NSString *poetName_first;
@property(nonatomic,assign)NSInteger poetNum_first ;
@property(strong,nonatomic)NSString *poetHeadImgUrl_first;

@property(strong,nonatomic)NSMutableArray *mArr_allPoet_song;//宋词
@property(strong,nonatomic)NSString *poetName_first_song;
@property(nonatomic,assign)NSInteger poetNum_first_song ;
@property(strong,nonatomic)NSString *poetHeadImgUrl_first_song;

//====轮训  shiwenNum   1---->72738  （目标总计是：共72109篇）
-(void)loop_shiwenNum:(NSInteger)indexGo;
@property (strong,nonatomic)NSArray *Arr_notExistNum;

//====全部轮训  shiwenNum    （目标总计是：共72109+篇）
-(void)loop_shiwenNum_getMainText:(NSInteger)indexGo;
@property (strong,nonatomic)NSArray *Arr_ExistNum;

//***************
//zzzzzzzz222222222222
//====轮训  诗文的翻译和赏析   1---->72738  （目标总计是：共72109篇）
//***************
-(void)loop_shiwenNum_getFanyiShangxi:(NSInteger)indexGo;


//循环get 诗人Obj
-(void)runCatchOn_poetNum:(NSInteger)poetNum;
//@property(strong,nonatomic)NSMutableArray *allPoet_mArr;

#pragma mark 获取 shiwen的  翻译page
-(void)runCatchOn_shiciFanYiPage:(NSInteger)fanyiNum;


//获取 诗人 头像

@property(nonatomic,strong)NSMutableArray *mArr_poetHead;

-(void)loop_download_poetHead:(int)index;

@end
