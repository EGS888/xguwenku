//
//  gujiFanyiObj.h
//  Script4_gushiwen.org
//
//  Created by Engels on 15-3-2.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface gujiFanyiObj : NSObject

@property (nonatomic,assign)long book_num;//古籍序号 ====>从属于此

@property (nonatomic,assign)long bookV_num;//古籍篇章序号 bookV_num  ====>从属于此

@property (nonatomic,assign)long bookV_fanyi_num;//翻译篇章序号

@property (nonatomic,strong)NSString *bookV_fanyi_title;//翻译篇章title

@property (nonatomic,strong)NSString *fanyiMainText;//翻译

@end
