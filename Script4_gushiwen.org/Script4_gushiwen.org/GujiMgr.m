//
//  GujiMgr.m
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-9.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import "GujiMgr.h"
#import "ApiClient_Spider4Gushiwen.h"
#import "TFHpple.h"
#import "NSString+Extensions.h"
#import "NSMutableArray+mArrHelper.h"

#import "gujiObj.h"
#import "gujiPageObj.h"
#import "gujiFanyiObj.h"
#import "gujiShangxiObj.h"
#import "gujiAuthorObj.h"

#import "SDWebImageManager.h"

#import <UIKit/UIKit.h>

@implementation GujiMgr

+(GujiMgr *)shareClient
{
    static GujiMgr *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[GujiMgr alloc]  init];
    });
    
    return _sharedClient;
}

-(void)loopTag4Guji:(NSInteger)indexA
{
//    NSArray *cateA  = @[@"经部",@"史部",@"子部",@"集部"];//
    
    
    NSArray *cateA  = @[@"易类",@"书类",@"诗类",@"礼类",@"春秋类",@"孝经类",@"五经总义类",@"四书类",@"乐类",@"小学类",
                        @"正史类",@"编年类",@"纪事本末类",@"别史类",@"杂史类",@"诏令奏议类",@"传记类",@"史钞类",@"载记类",@"时令类",@"地理类",@"职官类",@"政书类",@"目录类",@"史评",
                        @"儒家类",@"兵家类",@"法家类",@"农家类",@"医家类",@"天文算法类",@"术数类",@"艺术类",@"诸录类",@"杂家类",@"类书类",@"小说家类",@"释家类",@"道家类",
                        @"楚辞类",@"别集类",@"总集类",@"诗文评类",@"词曲类"];
 
    
    if (indexA < [cateA count]) {
        
        NSString *cateStrA = [cateA objectAtIndex:indexA];
        
        [self runCatchOn:1
                        :cateStrA
                        :indexA];
    }
    else
    {
        NSLog(@"finish all");
    }
    
}

//启动catch。===》分类情况获取
-(void)runCatchOn:(NSInteger)currPage
                 :(NSString *)categoryA
                 :(NSInteger)indexA
{
    NSString *uriStr = [NSString stringWithFormat:@"/guwen/Default.aspx?p=%ld&type=%@",currPage,categoryA];
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [HttpVerClient GetHTML_guji_simple:encodedValue
                                        :^(BOOL bol, id responseData) {
                                            
                                            if (![responseData isKindOfClass:[NSArray class]]) {
                                                sleep(8);
                                                
                                                //网关超时  重新来
                                                [self runCatchOn:currPage
                                                                :categoryA
                                                                :indexA];
                                            }
                                            
                                            for (TFHppleElement *element in responseData) {
                                                
                                                //区域share Var
                                                NSString *bookName;
                                                NSInteger bookNum;
                                                NSString *bookCoverUrl;
                                                NSString *bookIntroTxt;
                                                NSString *authorName;
                                                
                                                //1111===book标题  bookNum
                                                NSArray *a1Arr = [element searchWithXPathQuery:@"//a[@style='font-size:14px;']"];
                                                
                                                if ([a1Arr count]>0) {
                                                    TFHppleElement *obj = [a1Arr firstObject];
                                                    
                                                    bookName = obj.text;
                                                    
                                                    NSString *rawNum = [[obj attributes] objectForKey:@"href"];
                                                    
                                                    bookNum =  [[rawNum get_num_fromTxt] integerValue];
                                                    
                                                }
                                                
                                                //2222====book摘要 简介
                                                NSArray *p1Arr = [element searchWithXPathQuery:@"//p[@style='margin-bottom:0px;']"];
                                                
                                                if ([p1Arr count]==1) {
                                                    TFHppleElement *obj = [p1Arr firstObject];
                                                    
                                                    bookIntroTxt = obj.text;
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                //3333=== 作者名字
                                                NSArray *p2Arr = [element searchWithXPathQuery:@"//p[@style='color:#676767;']"];
                                                
                                                if ([p2Arr count]==1) {
                                                    TFHppleElement *obj = [p2Arr firstObject];
                                                    
                                                    authorName = [obj.text get_poetName];
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行作者");
                                                }
                                                
                                                
                                                //44444 bookCover url=====有时可能不存在
                                                NSArray *imgArr = [element searchWithXPathQuery:@"//img"];
                                                
                                                if ([imgArr count]!=0) {
                                                    
                                                    //==bookCover url
                                                    TFHppleElement *elem2 = [imgArr objectAtIndex:0];
                                                    
                                                    bookCoverUrl = [[elem2 attributes] objectForKey:@"src"];
                                                }
                                                else
                                                {
                                                    NSLog(@"不存在 书本封面");
                                                }
                                                
                                                //===key 检索数据库中是否已经存入这篇  booknum
                                                
                                                NSString *whereStr = [NSString stringWithFormat:@"bookNum = %ld",bookNum];
                                                
                                                NSMutableArray *mArr = [gujiObj searchWithWhere:whereStr
                                                                                        orderBy:nil
                                                                                         offset:0
                                                                                          count:5] ;//@"rowid = 1"
                                                
                                                if ([mArr count]==0) {
                                                    //未存在  生成一条新数据
                                                    gujiObj *gujiObjX = ({
                                                        gujiObj *gujiTemp = [gujiObj new];
                                                        
                                                        gujiTemp.bookNum = bookNum;
                                                        gujiTemp.bookName = bookName;
                                                        gujiTemp.bookCoverUrl = bookCoverUrl!=nil?bookCoverUrl:nil;
                                                        gujiTemp.bookIntroTxt = bookIntroTxt;
                                                        gujiTemp.authorName = authorName;
                                                        
                                                        
                                                        //cate A==============
                                                        /*
                                                        if (gujiTemp.cate_JSZJ==nil) {
                                                            gujiTemp.cate_JSZJ = @[categoryA];
                                                            
                                                        }
                                                        else if ([gujiTemp.cate_JSZJ count]>0)
                                                        {
                                                            if (![gujiTemp.cate_JSZJ checkExist:categoryA]) {
                                                                
                                                                NSMutableArray *mArrTemp = [NSMutableArray arrayWithArray:gujiTemp.cate_JSZJ];
                                                                
                                                                [mArrTemp addObject:categoryA];
                                                                
                                                                gujiTemp.cate_JSZJ = [mArrTemp copy];
                                                                
                                                                
                                                            }
                                                        }
                                                         */
                                                        
                                                        //cate B=============手动打开
                                                        if (gujiTemp.cate_subJSZJ==nil) {
                                                            gujiTemp.cate_subJSZJ = @[categoryA];
 
                                                        }
                                                        else if ([gujiTemp.cate_subJSZJ count]>0)
                                                        {
                                                            if (![gujiTemp.cate_subJSZJ checkExist:categoryA]) {
                                                                
                                                                NSMutableArray *mArrTemp = [NSMutableArray arrayWithArray:gujiTemp.cate_subJSZJ];
                                                                
                                                                [mArrTemp addObject:categoryA];
                                                                
                                                                gujiTemp.cate_subJSZJ = [mArrTemp copy];

                                                            }
                                                        }
                                                        
                                                        gujiTemp;
                                                    });
                                                    
                                                    //首次  存入=====
                                                    if ([gujiObjX saveToDB]) {
                                                        NSLog(@"save: %@",gujiObjX.bookName);
                                                    }
                                                    else
                                                    {
                                                         NSLog(@"save: err");
                                                    }
                                                }
                                                else if ([mArr count]==1)
                                                {//已经存在一条。 正常情况。
                                                    gujiObj *gujiObjY= (gujiObj *)[mArr firstObject];
                                                    //cate A==============
                                                    /*
                                                    if (gujiObjY.cate_JSZJ==nil) {
                                                        gujiObjY.cate_JSZJ = @[categoryA];
                                                        
                                                        [gujiObjY updateToDB];
                                                        NSLog(@"add cate_JSZJ：%@",gujiObjY.cate_JSZJ);
                                                    }
                                                    else if ([gujiObjY.cate_JSZJ count]>0)
                                                    {
                                                        if (![gujiObjY.cate_JSZJ checkExist:categoryA]) {
                                                            
                                                            NSMutableArray *mArrTemp = [NSMutableArray arrayWithArray:gujiObjY.cate_JSZJ];
                                                            
                                                            [mArrTemp addObject:categoryA];
                                                            
                                                            gujiObjY.cate_JSZJ = [mArrTemp copy];
                                                            
                                                            [gujiObjY updateToDB];
                                                            NSLog(@"更新gujiTemp.cate_JSZJ：%@",gujiObjY.cate_JSZJ);
                                                        }
                                                    }
                                                    */
                                                    
                                                    //cate B=============手动打开
                                                        if (gujiObjY.cate_subJSZJ==nil) {
                                                            gujiObjY.cate_subJSZJ = @[categoryA];

                                                            [gujiObjY updateToDB];
                                                            NSLog(@"add cate_subJSZJ：%@",gujiObjY.cate_subJSZJ);
                                                        }
                                                        else if ([gujiObjY.cate_subJSZJ count]>0)
                                                        {
                                                            if (![gujiObjY.cate_subJSZJ checkExist:categoryA]) {

                                                                NSMutableArray *mArrTemp = [NSMutableArray arrayWithArray:gujiObjY.cate_subJSZJ];

                                                                [mArrTemp addObject:categoryA];
                                                                
                                                                gujiObjY.cate_subJSZJ = [mArrTemp copy];
                                                                
                                                                [gujiObjY updateToDB];
                                                                NSLog(@"更新cate_subJSZJ：%@",gujiObjY.cate_subJSZJ);
                                                            }
                                                        }
                                                    
                                                }
                                                else if ([mArr count]>1)
                                                {// 不应该出现  两条一样的数据！！
                                                    NSLog(@" 不应该出现  两条一样的数据！！");
                                                }
                                                
                                            }
                                            
                                            if ([responseData count]>0) {
                                                [self runCatchOn:currPage+1
                                                                :categoryA
                                                                :indexA];//loop on page
                                            }
                                            else if ([responseData count]==0)
                                            {
                                                NSLog(@"cate%@ get finish",categoryA);
                                                
                                                [self loopTag4Guji:indexA+1];
                                            }
                                        }];
}

//===获取==//正文: 章名chapterName-->节名sectionName-->节pageUrl (第二种情况  节名--->节pageUrl)
-(void)runCatchOn:(NSInteger)indexGo
{
    if (indexGo>100) {
        return;
    }
    
    NSString *uriStr = [NSString stringWithFormat:@"/guwen/book_%ld.aspx",indexGo];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [HttpVerClient GetHTML_guji_Chapter:encodedValue
                                      :^(BOOL bol, id tagSon2, id tagBookcont) {
                                          
                                          if (!bol) {
                                              sleep(8);
                                              
                                              //网关超时  重新来
                                              [self runCatchOn:indexGo];
                                          }
                                          
                                          NSString * bookIntroTxt ;
                                          NSInteger  authorNum = 0;
                                          
                                          //===book 简介
                                          if ([tagSon2 count]==1) {
                                              
                                              TFHppleElement *eleIntro = [tagSon2 firstObject];
                                              NSArray *childTxtArr = [eleIntro childrenWithTagName:@"text"];
                                              
                                              NSMutableArray *mArrchild = [NSMutableArray arrayWithArray:childTxtArr];
                                              [mArrchild removeObjectAtIndex:0];
                                              [mArrchild removeObjectAtIndex:0];
                                              
                                              NSMutableString *shiwenText_m = [NSMutableString new];
                                              
                                              //content 在div son2
                                              for (TFHppleElement *eleX in mArrchild) {
                                                  
                                                  NSString *clearSpace = [eleX.content stringByReplacingOccurrencesOfString:@" "
                                                                                                                 withString:@""];
                                                  
                                                  if ([clearSpace length]>2) {
                                                      
                                                      [shiwenText_m appendString:clearSpace];
                                                  }
                                              }
                                              
                                              bookIntroTxt = shiwenText_m;// 简介
                                              
                                              NSArray *authorArr = [eleIntro searchWithXPathQuery:@"//p[@style='margin:0px; font-size:12px;line-height:160%;']//a"];
                                              
                                              if ([authorArr count]==1) {
                                                  TFHppleElement *ele_P_a = [authorArr firstObject];
                                                  authorNum = [[[[ele_P_a attributes] objectForKey:@"href"] get_num_fromTxt] integerValue];
                                                  
                                              }
                                          }
                                          else
                                          {
                                              NSLog(@"不应该出现两段简介");
                                          }
                                          
                                          //===book 章 节 页  mArr
                                          /*
                                          NSMutableArray *m_chapterSectionPage = [NSMutableArray new];

                                          for (TFHppleElement *element in tagBookcont) {
                                              
                                              NSArray *chapterArr = [element searchWithXPathQuery:@"//strong"];
                                              
                                              NSString *chapterName ;
                                              
                                              if ([chapterArr count]>0) {
                                                  
                                                  TFHppleElement *elementChapterName = [chapterArr firstObject];
                                                  
                                                  chapterName = elementChapterName.text;
                                              }
                                              
                                              NSArray *sectionArr = [element searchWithXPathQuery:@"//a"];
                                              
                                              for (TFHppleElement *sectionEle in sectionArr) {
                                                  
                                                  NSString *sectionName = sectionEle.text;//章节名字
                                                  NSString *sectionNum = [[[sectionEle attributes] objectForKey:@"href"] get_num_fromTxt];
                                                  
                                                  if (sectionNum==nil)
                                                  {
                                                      sectionNum = @"-1";//失传， 比如鬼谷子的
                                                  }
                                                  
                                                  NSArray *meteChapterSectionPage ;
                                                  if (chapterName) {
                                                      meteChapterSectionPage = @[chapterName,sectionName,sectionNum];
                                                  }
                                                  else if (chapterName==nil)
                                                  {
                                                      meteChapterSectionPage = @[sectionName,sectionNum];
                                                  }
                                                  
                                                  [m_chapterSectionPage  addObject:meteChapterSectionPage];
                                              }
                                          
                                          
                                          }
                                          */
                                          
                                          //===key 检索数据库中是否已经存入这篇  booknum
                                          
                                          NSString *whereStr = [NSString stringWithFormat:@"bookNum = %ld",indexGo];
                                          
                                          NSMutableArray *mArr = [gujiObj searchWithWhere:whereStr
                                                                                  orderBy:nil
                                                                                   offset:0
                                                                                    count:5] ;//@"rowid = 1"
                                          
                                          if ([mArr count]==0) {
                                              NSLog(@"不可能不存在 %ld",indexGo);
                                          }
                                          else if ([mArr count]==1)
                                          {//已经存在一条。 正常情况。
                                              gujiObj *gujiObjY= (gujiObj *)[mArr firstObject];
                                              
                                              /*
                                              if (gujiObjY.chapterSectionPage==nil) {
                                                  gujiObjY.chapterSectionPage = m_chapterSectionPage;
                                                  
                                                  if ([gujiObjY updateToDB]) {
                                                      NSLog(@"%@:更新成功章节:%ld",gujiObjY.bookName,[gujiObjY.chapterSectionPage  count]);
                                                  }
                                              }
                                              else if ([gujiObjY.chapterSectionPage count]==[m_chapterSectionPage count])
                                              {
                                                  NSLog(@"两次running 一致");
                                                  
                                                  if (![gujiObjY.bookIntroTxt isEqualToString:bookIntroTxt]) {
                                                      gujiObjY.bookIntroTxt = bookIntroTxt;
                                                      
                                                      if ([gujiObjY updateToDB]) {
                                                          NSLog(@"更新introText:%@",gujiObjY.bookIntroTxt);
                                                      }
                                                      
                                                  }
                                                  
                                              }
                                              else
                                              {
                                                  NSLog(@"严重：章节数目前后get的不一致");
                                              }
                                               */
                                              
                                              if (gujiObjY.authorNum==0
                                                  &&authorNum!=0) {
                                                  gujiObjY.authorNum = authorNum;
                                                  
                                                  [gujiObjY saveToDB];
                                              }
                                              
                                              
                                          }
                                          else if ([mArr count]>1)
                                          {// 不应该出现  两条一样的数据！！
                                              NSLog(@" 不应该出现  两条一样的数据！！");
                                          }
                                          
                                          
                                          if (indexGo<=100)
                                          {
                                              NSLog(@"book %ld get finish",indexGo);
                                              
                                              [self runCatchOn:indexGo+1];
                                          }
                                      }];
}


-(void)loopAuthorNum:(NSInteger)indexGo
{
    if (self.authorNumArr==nil) {
        
        NSString *whereIs = [NSString stringWithFormat:@"authorNum != 0"];
        
        self.authorNumArr =         [gujiObj searchColumn:@"authorNum"
                                                    where:whereIs
                                                  orderBy:@"authorNum"
                                                   offset:0
                                                    count:500];
    }
    
    if (indexGo < [_authorNumArr count]) {
        
        [self runOnAllBook_authorObj:[[_authorNumArr objectAtIndex:indexGo] integerValue]
                                    :indexGo];
        
    }
    else
    {
        NSLog(@"finish loop author");
    }
    
}

-(void)runOnAllBook_authorObj:(NSInteger)authorNum
                             :(NSInteger)indexGo
{
   
    
    if (indexGo >= [self.authorNumArr count] ) {
        return;
    }
    
    NSString *uriStr = [NSString stringWithFormat:@"/author_%ld.aspx",authorNum];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [HttpVerClient GetHTML_guji_Author:encodedValue
                                      :^(BOOL bol, TFHppleElement * tagSon1, TFHppleElement * tagSon2) {
                                           
                                           if (!bol) {
                                               sleep(8);
                                               
                                               //网关超时  重新来
                                               [self loopAuthorNum:indexGo];
                                           }
                                           
                                           NSString * authorName ;
                                           NSString * authorHeadUrl;
                                           NSString * authorIntro;
                                          
                                           //===book 简介
                                           if (tagSon1) {
                                               
                                               NSArray *h1_arr = [(TFHppleElement *)tagSon1 searchWithXPathQuery:@"//h1"] ;
                                               
                                               TFHppleElement *h1_ele = [h1_arr firstObject];
                                               
                                               authorName = h1_ele.content;
                                           }
                                          
                                           if (tagSon2) {
                                               
                                               
                                               NSArray *img_arr = [(TFHppleElement *)tagSon2 searchWithXPathQuery:@"//img"] ;
                                               
                                               TFHppleElement *img_ele = [img_arr firstObject];
                                               
                                               authorHeadUrl = [[img_ele attributes] objectForKey:@"src"];
                                               
                                               //...
                                               NSArray *childTxtArr = [(TFHppleElement *)tagSon2 searchWithXPathQuery:@"//p"] ;
                                               
                                               if ([childTxtArr count]==0) {
                                                   authorIntro = [tagSon2.content clear_stringWrong_space];
                                               }
                                               else
                                               {
                                                   NSMutableArray *mArrchild = [NSMutableArray arrayWithArray:childTxtArr];
                                                   
                                                   
                                                   
                                                   NSMutableString *mStr = [NSMutableString new];
                                                   
                                                   //content 在div son2
                                                   for (TFHppleElement *eleX in mArrchild) {
                                                       
                                                       NSString *clearSpace = [eleX.content stringByReplacingOccurrencesOfString:@" "
                                                                                                                      withString:@""];
                                                       
                                                       if ([clearSpace length]>2) {
                                                           
                                                           [mStr appendString:clearSpace];
                                                       }
                                                   }
                                                   
                                                   authorIntro = mStr;// author简介
                                               }
                                               
                                               
                                               
                                           }
                                          
                                           
                                           //===key 检索数据库中是否已经存入这篇  booknum
                                           
                                           NSString *whereStr = [NSString stringWithFormat:@"authorNum = %ld",authorNum];
                                           
                                           NSMutableArray *mArr = [gujiAuthorObj searchWithWhere:whereStr
                                                                                   orderBy:nil
                                                                                    offset:0
                                                                                     count:5] ;//@"rowid = 1"
                                           
                                           if ([mArr count]==0) {
                                               
                                               gujiAuthorObj *gujiAuthor= ({
                                                   gujiAuthorObj *tempObj = [gujiAuthorObj new];
                                                   tempObj.authorNum = authorNum;
                                                   tempObj.authorName = authorName;
                                                   tempObj.authorIntro =authorIntro;
                                                   tempObj.authorHeadUrl = authorHeadUrl;
                                               
                                                   tempObj;
                                               });
                                               
                                               [gujiAuthor saveToDB];
                                           }
                                           else if ([mArr count]==1)
                                           {//已经存在一条。 正常情况。
                                               gujiAuthorObj *gujiAuthor= (gujiAuthorObj *)[mArr firstObject];
                                               
                                               if (![gujiAuthor.authorIntro isEqualToString:authorIntro]) {
                                                   gujiAuthor.authorIntro = authorIntro;
                                                   
                                                   [gujiAuthor saveToDB];
                                               }
                                               
                                           }
                                           else if ([mArr count]>1)
                                           {// 不应该出现  两条一样的数据！！
                                               NSLog(@" 不应该出现  两条一样的数据！！");
                                           }
                                           
                                           
                                           if (indexGo<=100)
                                           {
                                               NSLog(@"authorNum %ld get finish",authorNum);
                                               
                                               [self loopAuthorNum:indexGo+1];
                                           }
                                       }];
}

#pragma mark 获取篇章 内容 bookv
-(void)loopBookV_byColletionNum
{
    //收集所有：bookV_num  在@"GuJiBookTable"
    NSString *where_bookVNum = [NSString stringWithFormat:@"bookNum != \'\'"];
    
    NSArray *allArr_bookV_num = [gujiObj searchWithWhere:where_bookVNum
                                                 orderBy:@""
                                                  offset:0
                                                   count:5000];
    
    NSMutableArray *mArr_allBookV_Num = [NSMutableArray new];
    
    for (gujiObj *bookObj in allArr_bookV_num) {
        
        NSArray * chapterSectionPage = bookObj.chapterSectionPage;
        
        for (NSArray *C_S_P in chapterSectionPage) {
            
            NSString *bookv_numXX = [C_S_P lastObject];
            
            if (![bookv_numXX isEqualToString:@"-1"]) {
                [mArr_allBookV_Num addObject:bookv_numXX];
            }
            else
            {
                NSLog(@"存在-1");
            }
            
        }
        
    }
    
    
    //筛选掉已经存在的 bookV_num   在@"GuJiBookVTable"
    /*
    NSString *where_Exist_bookVNum = [NSString stringWithFormat:@"bookV_num != \'\'"];
    
    NSArray *exist_bookV_num = [gujiPageObj searchColumn:@"bookV_num"
                                                   where:where_Exist_bookVNum
                                                 orderBy:@"bookV_num"
                                                  offset:0
                                                   count:5000];
    for (NSString *bookV_numX in exist_bookV_num) {
        NSLog(@"已经存在：%@",bookV_numX);
        
        for (int i=0 ; i< [mArr_allBookV_Num count]; i++) {
            NSString *str_comparison = [mArr_allBookV_Num objectAtIndex:i];
            
            if ([str_comparison isEqualToString:bookV_numX]) {
                [mArr_allBookV_Num removeObjectAtIndex:i];
                break;
            }
            
        }
        
    }
    */
    
    //===
    self.allArr_bookV_num = mArr_allBookV_Num;//剩余还需要getV 的bookV_Num
    
    
    [self runCatchOn_articePage:0];
}

-(void)runCatchOn_articePage:(NSInteger)indexArrGo
{
    if (indexArrGo>=[self.allArr_bookV_num count]) {
        NSLog(@"已经run out了！！！");
        return;
    }
    
    NSInteger bookv_num = [[self.allArr_bookV_num objectAtIndex:indexArrGo] integerValue];
    
    if (bookv_num==-1) {
        [self runCatchOn_articePage:indexArrGo+1];
    }
    
    NSString *uriStr = [NSString stringWithFormat:@"/guwen/bookv_%ld.aspx",bookv_num];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [HttpVerClient GetHTML_guji_BookV:encodedValue
                                       :^(BOOL bol, id Shileft) {
                                           
                                           NSInteger bookv_num = [[self.allArr_bookV_num objectAtIndex:indexArrGo] integerValue];
                                           
                                           if (!bol) {
                                               sleep(8);
                                               
                                               //网关超时  重新来
                                               [self runCatchOn_articePage:indexArrGo];
                                           }
                                           
                                           //---111
                                           NSArray *rawContentArr = [(TFHppleElement *)Shileft searchWithXPathQuery:@"//div[@class='bookvson2']"];
                                           TFHppleElement *rawContent = [rawContentArr firstObject];
                                           
                                           //---222
                                           NSArray *a_bookNum_1Arr = [(TFHppleElement *)Shileft searchWithXPathQuery:@"//a[@style='border:0px;line-height:24px; height:24px;']"];
                                           TFHppleElement *ele_bookNum = [a_bookNum_1Arr firstObject];
                                           NSInteger bookNum = [[[[ele_bookNum attributes] objectForKey:@"href"] get_num_fromTxt] integerValue];
                                           
                                           //---333
                                           NSArray *fanyi_shangxi_EleArr = [(TFHppleElement *)Shileft searchWithXPathQuery:@"//div[@class='son5']"];//‘翻译’ 和 ‘赏析’
                                           
                                           //---444
                                           NSArray *gujiPageNameArr = [(TFHppleElement *)Shileft searchWithXPathQuery:@"//div[@class='son1']//h1"];
                                           if ([gujiPageNameArr count]!=1)
                                           {
                                                   NSLog(@"只应该有一个h1");
                                           }
                                           
                                           NSString *bookV_title = ((TFHppleElement *)[gujiPageNameArr firstObject]).content;
                                           
                                           NSMutableString *richTextPart = [NSMutableString new];//此篇内容text。
                                           NSMutableArray *mArr_fanyiFor = [NSMutableArray new];//翻译
                                           NSMutableArray *mArr_shangxiFor = [NSMutableArray new];//赏析
                                           
                                           //----
                                           //===原文  text
                                           //33333 ‘原文’ 下面的所有pTag 都是 shiwenText 判断
                                           //----
                                           
                                           
                                           NSArray *p_All_Tag = [rawContent searchWithXPathQuery:@"//p"];
                                           
                                           NSMutableArray *p_All_Tag_mutable = [NSMutableArray arrayWithArray:p_All_Tag];
                                           
                                           
                                           BOOL has_pTag_content = NO;
                                           
                                           if ([p_All_Tag_mutable count]>1) {
                                               
                                               [p_All_Tag_mutable removeObjectAtIndex:0];//去掉前1个 obj===作者
                                               
                                               has_pTag_content = YES;
                                           }
                                           
                                           
                                           //content 在div son2!!!!!!!!!!!!!!!!!!!!!
                                           if (has_pTag_content==NO)//没有p content
                                           {
                                               NSArray *childArr = [rawContent childrenWithTagName:@"text"];
                                               
                                               for (TFHppleElement *eleX in childArr) {
                                                   
                                                   //==移除作者 那行p
                                                   TFHppleElement *p_oneAuthorName = [p_All_Tag_mutable firstObject];
                                                   if ([p_oneAuthorName.text isEqualToString:eleX.content]) {
                                                       continue;
                                                   }
                                                   
                                                   NSString *clearSpace = [eleX.content stringByReplacingOccurrencesOfString:@" "
                                                                                                                  withString:@""];
                                                   
                                                   if ([clearSpace length]>2) {
                                                       
                                                       [richTextPart appendString:clearSpace];
                                                   }
                                               }
                                               
                                           }
                                           
                                           //存在 p_content!!!!!!!!!!!!!!!!!
                                           if (has_pTag_content
                                               && [p_All_Tag_mutable count]>0)
                                           {
                                               //内容在p content中
                                               for (TFHppleElement *eleP_content in p_All_Tag_mutable) {
                                                   
                                                   //==针对 <strong>标签！！！！！！！！！！！！
                                                   NSString *strong_str ;
                                                   if ([[eleP_content searchWithXPathQuery:@"//strong"] count]==1) {
                                                       
                                                       TFHppleElement *ele_strong = [[eleP_content searchWithXPathQuery:@"//strong"] firstObject];
                                                       
                                                       strong_str = [NSString stringWithFormat:@"**%@**",ele_strong.content];
                                                       
                                                       [richTextPart appendString:@"\n"];
                                                       [richTextPart appendString:strong_str];
                                                   }
                                                   
                                                   
                                                   NSArray *childArr_p = [eleP_content childrenWithTagName:@"text"];
                                                   
                                                   for (TFHppleElement *eleX in childArr_p) {
                                                       
                                                       NSString *clearSpace = eleX.content;//[eleX.content stringByReplacingOccurrencesOfString:@" "
                                                                                              //                        withString:@""];
                                                       
                                                       if ([clearSpace length]>2) {
                                                           
                                                           [richTextPart appendString:@"\n"];
                                                           [richTextPart appendString:clearSpace];
                                                       }
                                                   }
                                                   
                                                   
                                               }
                                           }
                                           
                                           
//                                           NSLog(@"mainText on %@ \n %@ ",uriStr,richTextPart);
                                           
                                           
                                           //===翻译&赏析  条目list（一般情况下，一个是译文，一个是多译字解析）----
                                           /*
                                           for (TFHppleElement *fanyi_shangxi_obj in fanyi_shangxi_EleArr) {
                                               //判断是翻译，还是赏析
                                               
                                               NSArray *a_tagArr = [fanyi_shangxi_obj searchWithXPathQuery:@"//a"];
                                               TFHppleElement *a_element = [a_tagArr firstObject];
                                               
                                               NSString *is_f_s = [[[a_element attributes] objectForKey:@"href"] get_fanyi_or_shangxi];
                                               
                                               NSInteger key_num = [[[[a_element attributes] objectForKey:@"href"] get_num_fromTxt] integerValue];
                                               
                                               //---
                                               NSArray *u_tagArr = [fanyi_shangxi_obj searchWithXPathQuery:@"//u"];
                                               TFHppleElement *u_element = [u_tagArr firstObject];
                                               
                                               NSString *titleStr = u_element.content;
                                               
                                               if ([is_f_s isEqualToString:@"bfanyi"]) {
                                                   
                                                   gujiFanyiObj *fanyi_NewObj = ({
                                                       gujiFanyiObj *fanyi_temp = [gujiFanyiObj new];
                                                       
                                                       fanyi_temp.book_num = bookNum;
                                                       fanyi_temp.bookV_num = bookv_num;
                                                       fanyi_temp.bookV_fanyi_num = key_num;
                                                       fanyi_temp.bookV_fanyi_title = titleStr;
                                                       
                                                       fanyi_temp;
                                                   });
                                                   
                                                   [fanyi_NewObj saveToDB];
                                                   
                                                   NSArray *arrFanyi = @[fanyi_NewObj.bookV_fanyi_title,@(fanyi_NewObj.bookV_fanyi_num)];
                                                   [mArr_fanyiFor addObject:arrFanyi];
                                                   
                                               }
                                               else if ([is_f_s isEqualToString:@"bshangxi"])
                                               {
                                                   gujiShangxiObj *shangxi_NewObj = ({
                                                       gujiShangxiObj *shangxi_temp = [gujiShangxiObj new];
                                                       
                                                       shangxi_temp.book_num = bookNum;
                                                       shangxi_temp.bookV_num = bookv_num;
                                                       shangxi_temp.bookV_shangxi_num = key_num;
                                                       shangxi_temp.bookV_shangxi_title = titleStr;
                                                       
                                                       shangxi_temp;
                                                   });
                                                   
                                                   [shangxi_NewObj saveToDB];
                                                   
                                                   NSArray *arrShangxi = @[shangxi_NewObj.bookV_shangxi_title,@(shangxi_NewObj.bookV_shangxi_num)];
                                                   [mArr_shangxiFor addObject:arrShangxi];
                                               }
                                               else{
                                                   NSLog(@"不可能还有 bfanyi  bshangxi 之外的情况吧  %@",is_f_s);
                                               }
                                               
                                           }
                                           */
                                           
                                           //-----
                                           //===key 检索数据库中是否已经存入这篇  booknum
                                           //-----
                                           NSString *whereStr = [NSString stringWithFormat:@"bookV_num = %ld",bookv_num];
                                           
                                           NSMutableArray *mArr = [gujiPageObj searchWithWhere:whereStr
                                                                                   orderBy:nil
                                                                                    offset:0
                                                                                     count:5] ;//@"rowid = 1"
                                           
                                           if ([mArr count]==0) {
                                               //第一次存入。。。
                                               NSLog(@"已经存在了！！！");
                                               /*
                                               gujiPageObj *newObj = ({
                                                   gujiPageObj *tempObj = [gujiPageObj new];
                                                   
                                                   tempObj.bookNum = bookNum;
                                                   tempObj.bookV_num = bookv_num;
                                                   tempObj.bookV_PageText = richTextPart;
                                                   tempObj.FanYi_List = mArr_fanyiFor;
                                                   tempObj.Shangxi_List =  mArr_shangxiFor;
                                                   
                                                   tempObj;
                                               });
                                               
                                               [newObj saveToDB];
                                                */
                                           }
                                           else if ([mArr count]==1)
                                           {//已经存在一条。 正常情况。
                                               gujiPageObj *oldObj = (gujiPageObj *)[mArr firstObject];
                                               
                                               /*
                                               if (oldObj.bookV_title==nil
                                                   ||![oldObj.bookV_title isEqualToString:bookV_title]) {
                                                   
                                                   oldObj.bookV_title = bookV_title;
                                                   
                                                   NSLog(@"%@",oldObj.bookV_title);
                                                   [oldObj saveToDB];
                                               }*/
                                               
                                               if (![oldObj.bookV_PageText isEqualToString:richTextPart]) {
                                                   
                                                   oldObj.bookV_PageText = bookV_title;
                                                   
                                                   NSLog(@"%@",oldObj.bookV_title);
                                                   
                                                   [oldObj saveToDB];
                                               }
                                               
                                           }
                                           else if ([mArr count]>1)
                                           {// 不应该出现  两条一样的数据！！
                                               NSLog(@" 不应该出现  两条一样的数据！！");
                                           }
                                           
                                           
                                           {
                                               NSLog(@"bookv %ld get finish",bookv_num);
                                               
                                               [self runCatchOn_articePage:indexArrGo+1];
                                           }
                                       }];
}


#pragma mark 获取古籍篇章的  翻译page
-(void)runCatchOn_fanYiPage:(NSInteger)bFanyiNum
{
    if (bFanyiNum>8023) {
        
        NSLog(@"finish get bFanyiNum");
        return;
    }
    
    NSString *uriStr = [NSString stringWithFormat:@"/guwen/bfanyi_%ld.aspx",bFanyiNum];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    
    [HttpVerClient GetHTML_guji_bFanyiPage:encodedValue
                                       :^(BOOL bol, TFHppleElement *shangxicontObjSame) {
                                           
                                           if (!bol&&shangxicontObjSame==nil) {
                                               sleep(8);
                                               
                                               //网关超时  重新来
                                               [self runCatchOn_fanYiPage:bFanyiNum];
                                               return ;
                                           }
                                           
                                           
                                           if (!bol&&[shangxicontObjSame isKindOfClass:[NSString class]]) {
                                               //next go
                                               [self runCatchOn_fanYiPage:bFanyiNum+1];
                                               
                                               return ;
                                           }
                                           
                                           NSMutableString *richTextPart = [NSMutableString new];//此篇内容text。
                 
                                           
                                           //----
                                           //===原文  text
                                           //33333 ‘原文’ 下面的所有pTag 都是 shiwenText 判断
                                           //----
                                           
                                           
                                           NSArray *p_All_Tag = [shangxicontObjSame searchWithXPathQuery:@"//p"];
                                           
                                           NSArray *text_All_Tag = [shangxicontObjSame childrenWithTagName:@"text"];
                                           
                                           NSMutableArray *p_All_Tag_mutable = [NSMutableArray arrayWithArray:p_All_Tag];
                                           
//                                           
//                                           BOOL has_pTag_content = NO;
//                                           
//                                           if ([p_All_Tag_mutable count]>2
//                                               &&[text_All_Tag count]<5) {
//                                               
//                                               [p_All_Tag_mutable removeObjectAtIndex:0];//去掉前1个 obj===作者
//                                               
//                                               has_pTag_content = YES;
//                                           }
//                                           
                                           
                                           //content 在div son2!!!!!!!!!!!!!!!!!!!!!
//                                           if (has_pTag_content==NO)//没有p content
                                           {
                                               NSArray *childArr = [shangxicontObjSame childrenWithTagName:@"text"];
                                               
                                               for (TFHppleElement *eleX in childArr) {
                                                   
                                                   //==移除作者 那行p
                                                   TFHppleElement *p_oneAuthorName = [p_All_Tag_mutable firstObject];
                                                   if ([p_oneAuthorName.text isEqualToString:eleX.content]) {
                                                       continue;
                                                   }
                                                   
                                                   NSString *clearB = [eleX.content clear_stringWrong_space];
                                                   
                                                   if ([clearB hasPrefix:@"参考资料"]) {
                                                       continue;
                                                   }
                                                   
                                                   if ([clearB length]>2) {
                                                       
                                                       [richTextPart appendString:@"\n　　"];
                                                       [richTextPart appendString:clearB];
                                                   }
                                               }
                                               
                                           }
                                           
                                           //存在 p_content!!!!!!!!!!!!!!!!!
//                                           if (has_pTag_content
//                                               && [p_All_Tag_mutable count]>0)
                                           {
                                               //内容在p content中
                                               for (TFHppleElement *eleP_content in p_All_Tag_mutable) {
                                                   
                                                   //==针对 内部<strong>标签！！！！！！！！！！！！
                                                   NSString *strong_str ;
                                                   if ([[eleP_content searchWithXPathQuery:@"//strong"] count]==1) {
                                                       
                                                       TFHppleElement *ele_strong = [[eleP_content searchWithXPathQuery:@"//strong"] firstObject];
                                                       
                                                       strong_str = [NSString stringWithFormat:@"**%@**",ele_strong.content];
                                                       
                                                       [richTextPart appendString:@"\n"];
                                                       [richTextPart appendString:strong_str];
                                                   }
                                                   //==针对 内部<a>标签！！！！！！！！！！！！
                                                   NSString *a_str ;
                                                   if ([[eleP_content searchWithXPathQuery:@"//a"] count]==1) {
                                                       
                                                       TFHppleElement *ele_a = [[eleP_content searchWithXPathQuery:@"//a"] firstObject];
                                                       
                                                       a_str = [NSString stringWithFormat:@"**%@**",ele_a.content];
                                                       
                                                       [richTextPart appendString:a_str];
                                                   }
                                                   
                                                   
                                                   NSArray *childArr_p = [eleP_content childrenWithTagName:@"text"];
                                                   
                                                   for (TFHppleElement *eleX in childArr_p) {
                                                       
                                                       //===针对p 内部的 textNode
                                                       NSArray *childArrTxt = [eleX childrenWithTagName:@"text"];
                                                       for (TFHppleElement *eleX in childArrTxt) {
                                                           
                                                           NSString *clearB = [eleX.content clear_stringWrong_space];
                                                           if ([clearB length]>2) {
                                                               
                                                               [richTextPart appendString:@"\n　　"];
                                                               [richTextPart appendString:clearB];
                                                           }
                                                       }
                                                       
                                                       NSString *clearSpace = eleX.content ;
                                                       
                                                       if ([clearSpace hasPrefix:@"作者"]) {
                                                           continue;
                                                       }
                                                       if ([clearSpace hasPrefix:@"参考资料"]) {
                                                           continue;
                                                       }
                                                       
                                                       if ([clearSpace length]>2) {
                                                           
                                                           [richTextPart appendString:@"\n"];
                                                           [richTextPart appendString:clearSpace];
                                                       }
                                                   }
                                                   
                                                   
                                               }
                                           }
                                           
                                           
                                           
                                           
                                           
                                           //===key 检索数据库中是否已经存入这篇  booknum
                                           
                                           NSString *whereStr = [NSString stringWithFormat:@"bookV_fanyi_num = %ld",bFanyiNum];
                                           
                                           NSMutableArray *mArr = [gujiFanyiObj searchWithWhere:whereStr
                                                                                   orderBy:nil
                                                                                    offset:0
                                                                                     count:5] ;//@"rowid = 1"
                                           
//                                           NSLog(@"richTextPart == %@",richTextPart);
                                           
                                           if ([mArr count]==0) {
                                               gujiFanyiObj *fanyiNew =({
                                                   
                                                   gujiFanyiObj *fanyiTemp = [gujiFanyiObj new];
                                                   
                                                   fanyiTemp.fanyiMainText = richTextPart;
                                                   
                                                   fanyiTemp;
                                               
                                               });
                                               
                                               if ([fanyiNew saveToDB]) {
                                                   
                                                   NSLog(@"bookV_fanyi_num on %@ \n %ld ",uriStr,bFanyiNum);
                                               }
                                           }
                                           else if ([mArr count]==1)
                                           {//已经存在一条。 正常情况。
                                               gujiFanyiObj *gujiFanyiObjY= (gujiFanyiObj *)[mArr firstObject];
                                               
                                               if (![gujiFanyiObjY.fanyiMainText isEqualToString:richTextPart]) {
                                                   gujiFanyiObjY.fanyiMainText = richTextPart;
                                                   
                                                   [gujiFanyiObjY saveToDB];
                                                   
                                                    NSLog(@"update bookV_fanyi_num on %@ \n %ld ",uriStr,bFanyiNum);
                                               }
                                               
                                           }
                                           else if ([mArr count]>1)
                                           {// 不应该出现  两条一样的数据！！
                                               NSLog(@" 不应该出现  两条一样的数据！！");
                                           }
                                           
                                           {
                                               NSLog(@"bookV_fanyi_num %ld get finish",bFanyiNum);
                                               
                                               [self runCatchOn_fanYiPage:bFanyiNum+1];
                                           }
                                       }];
}

-(void)runFix_fanyiPage
{
    NSString *whereis = [NSString stringWithFormat:@"fanyiMainText LIKE \'%%**本站**%%\'"];

    NSArray *all_needFix_arr = [gujiFanyiObj searchWithWhere:whereis
                                                   orderBy:@"bookV_fanyi_num"
                                                    offset:0
                                                     count:10000];
    
    for (gujiFanyiObj *bFanyiObj in all_needFix_arr) {
        
        NSString *fanyiMainText_old = bFanyiObj.fanyiMainText;
        
        NSArray *arr_get_first = [fanyiMainText_old componentsSeparatedByString:@"**本站**"];
        
//        NSLog(@"%@",[arr_get_first firstObject]);
        bFanyiObj.fanyiMainText = [arr_get_first firstObject];
        
        [bFanyiObj saveToDB];
    }
    
}



//过滤重复性，保持arr容器内部数值唯一。
-(NSMutableArray *)oneIt_mArr:(NSMutableArray *)mArr
{
    for (int i=0; i<[mArr count]; i++) {
        
        NSString *strPoet = [mArr objectAtIndex:i];
        
        for (int j = i+1; j<[mArr count]; j++) {
            
            NSString *strPoet_x = [mArr objectAtIndex:j];
            
            if ([strPoet_x isEqualToString:@"0"]
                ||[strPoet_x isEqualToString:@"-1"]) {
                [mArr removeObjectAtIndex:j];
                continue;
            }
            
            if ([strPoet isEqualToString:strPoet_x]) {
                [mArr removeObjectAtIndex:j];
            }
        }
    }
    
    return mArr;
}

/*********************/
//获取book封面地址
-(NSMutableArray *)get_bookCover_lists{
    
    if (self.mArr_bookcover==nil) {
        
        NSString *whereis = [NSString stringWithFormat:@"bookName != \'\'"];
        
        NSMutableArray *all_book_objs_marr = [gujiObj searchColumn:@"bookCoverUrl"
                                                             where:whereis
                                                           orderBy:@"rowid"
                                                            offset:0
                                                             count:1000];
        
        
        NSMutableArray *clear_mArr = [self oneIt_mArr:all_book_objs_marr];
        
        self.mArr_bookcover = clear_mArr;
    }
    
 
    return self.mArr_bookcover;
    
    
}

-(void)loop_download_bookcover:(int)index
{
    NSMutableArray *mArr_list = [self get_bookCover_lists];
    
    NSString *bookCoverUrl;
    
    if (index < [mArr_list count]){

                bookCoverUrl = [mArr_list objectAtIndex:index];
        
    }
    else
    {
        return;
    }

    
    
    NSURL *url_bookcover = [NSURL URLWithString:[NSString stringWithFormat:@"http://so.gushiwen.org%@",bookCoverUrl]];
    
    [[SDWebImageManager sharedManager] downloadWithURL:url_bookcover
                                               options:0
                                              progress:^(NSUInteger receivedSize, long long expectedSize) {
                                                  
                                              }
                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished) {
                                                 
                                                 if (finished) {
                                                     
                                                     if ([self storege_img_2_file:image :bookCoverUrl]) {
                                                         [self loop_download_bookcover:index+1];
                                                     }
                                                     else {
                                                     [self loop_download_bookcover:index+1];
                                                     }
                                                 }
                                                 
                                                 
                                             }];
    
}

-(BOOL)storege_img_2_file:(UIImage *)m_imgFore
                         :(NSString *)bookCoverUrl
{
    NSString *book_name = [[bookCoverUrl  componentsSeparatedByString:@"/"] lastObject];
    
    NSData *imagedata = UIImageJPEGRepresentation(m_imgFore,1.0);
    
//    NSArray*paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//    
//    NSString *documentsDirectory=[paths objectAtIndex:0];
    
    NSString* bookcover_path = [NSHomeDirectory() stringByAppendingPathComponent:@"db"];
    
    NSString *savedImagePath=[bookcover_path stringByAppendingPathComponent:book_name];
    
    BOOL bol = [imagedata writeToFile:savedImagePath atomically:YES];
    
 
    if (!bol) {
        NSLog(@"保存失败");
    }
    else
    {
        NSLog(@"保存成功");
    }
    return bol;
}

/******************/
//获取book封面地址
-(NSMutableArray *)get_authorHead_lists{
    
    if (self.mArr_authorHead==nil) {
        
        NSString *whereis = [NSString stringWithFormat:@"authorHeadUrl != \'\'"];
        
        NSMutableArray *all_book_objs_marr = [gujiAuthorObj searchColumn:@"authorHeadUrl"
                                                             where:whereis
                                                           orderBy:@"rowid"
                                                            offset:0
                                                             count:1000];
        
        
        NSMutableArray *clear_mArr = [self oneIt_mArr:all_book_objs_marr];
        
        self.mArr_authorHead = clear_mArr;
    }
    
    
    return self.mArr_authorHead;
    
    
}

-(void)loop_download_authorHead:(int)index
{
    NSMutableArray *mArr_list = [self get_authorHead_lists];
    
    NSString *authorHeadUrl;
    
    if (index < [mArr_list count]){
        
        authorHeadUrl = [mArr_list objectAtIndex:index];
        
        
    }
    else
    {
        return;
    }
    
    
    
    NSURL *url_authorHead = [NSURL URLWithString:[NSString stringWithFormat:@"http://so.gushiwen.org%@",authorHeadUrl]];
    
    [[SDWebImageManager sharedManager] downloadWithURL:url_authorHead
                                               options:0
                                              progress:^(NSUInteger receivedSize, long long expectedSize) {
                                                  
                                              }
                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished) {
                                                 
                                                 if (finished) {
                                                     
                                                     if ([self storege_authorhead_2_file:image :authorHeadUrl]) {
                                                         [self loop_download_authorHead:index+1];
                                                     }
                                                     else {
                                                         [self loop_download_authorHead:index+1];
                                                     }
                                                 }
                                                 
                                                 
                                             }];
    
}

-(BOOL)storege_authorhead_2_file:(UIImage *)m_imgFore
                         :(NSString *)authorHeadUrl
{
    NSString *authorHead = [[authorHeadUrl  componentsSeparatedByString:@"/"] lastObject];
    
    NSData *imagedata = UIImageJPEGRepresentation(m_imgFore,1.0);
    
 
    
    NSString* bookcover_path = [NSHomeDirectory() stringByAppendingPathComponent:@"authorHead"];
    
    NSString *savedImagePath=[bookcover_path stringByAppendingPathComponent:authorHead];
    
    BOOL bol = [imagedata writeToFile:savedImagePath atomically:YES];
    
    
    if (!bol) {
        NSLog(@"保存失败");
    }
    else
    {
        NSLog(@"保存成功");
    }
    return bol;
}

@end
