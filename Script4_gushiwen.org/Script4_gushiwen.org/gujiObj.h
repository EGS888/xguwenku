//
//  gujiObj.h
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-10.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LKDBHelper.h"

@interface gujiObj : NSObject

@property (nonatomic,assign)long bookNum;//古籍序号num

@property (nonatomic,assign)long authorNum;//作者序号num

//书籍标题  1. 唯一值。
@property (nonatomic,strong)NSString * bookName;//很大可能同名
//书籍封面  
@property (nonatomic,strong)NSString * bookCoverUrl;//很大可能同名

//摘要--- book_4.aspx 上的简介
@property (nonatomic,strong)NSString * bookIntroTxt;

//key正文index: 章名chapterName-->节名sectionName-->节pageUrl (第二种情况  节名--->节pageUrl)
@property (nonatomic,strong)NSArray * chapterSectionPage;//@[chapterName,sectionName,pageUrl]

//作者
@property (nonatomic,strong)NSString * authorName;
@property (nonatomic,strong)NSString * authorHeadUrl;

//翻译===
@property (nonatomic,strong)NSArray *FanYi_List;
//几种赏析===
@property (nonatomic,strong)NSArray *ShangXi_List;

@property (nonatomic,strong)NSArray *cate_JSZJ;//A分类: 经，史，子，集
@property (nonatomic,strong)NSArray *cate_subJSZJ;//B分类: 四部的小分类。

@end
