//
//  gujiObj.m
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-10.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import "gujiObj.h"
#import "LKDBHelper.h"

@implementation gujiObj

//重载选择 使用的LKDBHelper
+(LKDBHelper *)getUsingLKDBHelper
{
    static LKDBHelper* db;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString* dbpath = [NSHomeDirectory() stringByAppendingPathComponent:@"db/TaiXuWen.db"];
        db = [[LKDBHelper alloc] initWithDBPath:dbpath];
    });
    return db;
}

+(NSString *)getTableName
{
    //古书籍表
    return @"GuJiBookTable";
}




//列属性
+(void)columnAttributeWithProperty:(LKDBProperty *)property
{
    if([property.sqlColumnName isEqualToString:@"bookNum"])
    {
        property.isUnique = YES;
        property.isNotNull = YES;
    }
    
    if([property.sqlColumnName isEqualToString:@"chapterSectionPage"])
    {
        property.isNotNull = YES;
    }
    
    if([property.sqlColumnName isEqualToString:@"rowid"])
    {
        property.isUnique = YES;
        property.isNotNull = YES;
    }
    
}



//已经插入数据库
+(void)dbDidInserted:(NSObject *)entity result:(BOOL)result
{
    LKErrorLog(@"did insert : %@",NSStringFromClass(self));
}



@end
