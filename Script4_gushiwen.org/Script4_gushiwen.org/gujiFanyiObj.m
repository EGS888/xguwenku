//
//  gujiFanyiObj.m
//  Script4_gushiwen.org
//
//  Created by Engels on 15-3-2.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import "gujiFanyiObj.h"
#import "LKDBHelper.h"

@implementation gujiFanyiObj


//重载选择 使用的LKDBHelper
+(LKDBHelper *)getUsingLKDBHelper
{
    static LKDBHelper* db;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString* dbpath = [NSHomeDirectory() stringByAppendingPathComponent:@"db/TaiXuWen.db"];
        db = [[LKDBHelper alloc] initWithDBPath:dbpath];
    });
    return db;
}

+(NSString *)getTableName
{
    //古书籍 翻译表
    return @"GuJiFanyiTable";
}




//列属性
+(void)columnAttributeWithProperty:(LKDBProperty *)property
{
    if([property.sqlColumnName isEqualToString:@"bookV_fanyi_num"])
    {
        property.isUnique = YES;
        property.isNotNull = YES;
    }
    
   
    
    if([property.sqlColumnName isEqualToString:@"bookV_fanyi_title"])
    {
        property.isNotNull = YES;
    }
    
    if([property.sqlColumnName isEqualToString:@"fanyiMainText"])//当前篇内容页面内容
    {
        property.isNotNull = YES;
    }
    
    if([property.sqlColumnName isEqualToString:@"rowid"])
    {
        property.isUnique = YES;
        property.isNotNull = YES;
    }
    
}



//已经插入数据库
+(void)dbDidInserted:(NSObject *)entity result:(BOOL)result
{
    LKErrorLog(@"did insert : %@",NSStringFromClass(self));
}


@end
