//
//  ApiClient_Spider4Gushiwen.m
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-4.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import "ApiClient_Spider4Gushiwen.h"
#import "TFHpple.h"

#define kBaseURL_gushiwen @"http://so.gushiwen.org"

@implementation ApiClient_Spider4Gushiwen


+(ApiClient_Spider4Gushiwen *)shareClient
{
    static ApiClient_Spider4Gushiwen *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[ApiClient_Spider4Gushiwen alloc]  initWithBaseURL:[NSURL URLWithString:kBaseURL_gushiwen]];
        [_sharedClient initCustom];
    });
    
    return _sharedClient;
}
//===fix for some confuse problem
- (void)initCustom
{
    
    if (self) {
        //custom init method & var
        self.responseSerializer = [AFHTTPResponseSerializer new];
        
        //====警惕contentType 高频错位错误情况！！
        NSLog(@"set %@",self.responseSerializer.acceptableContentTypes);
        
        NSMutableSet *mSet = [NSMutableSet new];//[NSMutableSet setWithArray:[self.responseSerializer.acceptableContentTypes allObjects]];
        [mSet addObject:@"text/html"];
        
        self.responseSerializer.acceptableContentTypes = mSet;
        NSLog(@"set %@",self.responseSerializer.acceptableContentTypes);
        
        self.requestSerializer.timeoutInterval = 120;//两倍与defaultTimeoutInterval
        self.requestSerializer.cachePolicy = NSURLRequestReloadRevalidatingCacheData;
    }
}

-(void)GetHTML_Mingju:(NSString *)queryURI
                     :(backArr)blk;
{
    
    [[ApiClient_Spider4Gushiwen shareClient] GET:queryURI
                                      parameters:nil
                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             
                                              
//                                             NSString *rawHtml = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
//                                             NSLog(@"%@",rawHtml);
                                             
                                             TFHpple *hppleDoc = [[TFHpple alloc] initWithHTMLData:responseObject];
                                             
                                             NSArray *arr = [hppleDoc searchWithXPathQuery:@"//div[@class='sons']"];
//                                             NSLog(@"%@",arr);
                                             
                                             blk(YES,arr);
                                         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             NSLog(@"%@",error);
                                             blk(NO,error);
                                         }];

}


-(void)GetHTML_ShiWen_simple:(NSString *)queryURI
                            :(backArr)blk
{
    
    [[ApiClient_Spider4Gushiwen shareClient] GET:queryURI
                                      parameters:nil
                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             
//                                             NSString *rawHtml = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                                             //                                             NSLog(@"%@",rawHtml);
                                             
                                             TFHpple *hppleDoc = [[TFHpple alloc] initWithHTMLData:responseObject];
                                             
                                             NSArray *arr = [hppleDoc searchWithXPathQuery:@"//div[@class='sons']"];
                                             
                                             blk(YES,arr);
                                         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             NSLog(@"%@",error);
                                             blk(NO,error);
                                         }];
    
}

-(void)GetHTML_ShiWen_shiwenView:(NSString *)queryURI
                                :(backArr_3)blk
{
    
    [[ApiClient_Spider4Gushiwen shareClient] GET:queryURI
                                      parameters:nil
                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             
 
                                             
                                             TFHpple *hppleDoc = [[TFHpple alloc] initWithHTMLData:responseObject];
                                             
                                             NSArray *arrCenter = [hppleDoc searchWithXPathQuery:@"//center"];
                                             
                                             NSArray *arrSon1 = [hppleDoc searchWithXPathQuery:@"//h1"];
                                             
                                             NSArray *arrSon2 = [hppleDoc searchWithXPathQuery:@"//div[@class='son2']"];//文章取真实的 第二个
                                             
                                             if ([arrCenter count]==1) {
                                                 //不存在那个  view；；；
                                                 blk(arrCenter,nil,nil);
                                             }
                                             else if ([arrCenter count]==0)
                                             {//存在  该 viewShiwen
                                                 blk(nil,arrSon1,[arrSon2 objectAtIndex:1]);
                                             }
                                             else
                                             {
                                                 NSLog(@"格式之外？？");
                                                 blk(nil,nil,nil);
                                             }

                                         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             NSLog(@"%@",error);
                                             blk(nil,nil,nil);
                                         }];
    
}

-(void)GetHTML_ShiWen_shiwenView_fanyiShangxi:(NSString *)queryURI
                                             :(backArr_son5)blk
{
    
    [[ApiClient_Spider4Gushiwen shareClient] GET:queryURI
                                      parameters:nil
                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             
                                             
                                             
                                             TFHpple *hppleDoc = [[TFHpple alloc] initWithHTMLData:responseObject];
                                             
                                             NSArray *arrCenter = [hppleDoc searchWithXPathQuery:@"//center"];
                                             
                                             NSArray *arrSon5 = [hppleDoc searchWithXPathQuery:@"//div[@class='son5']"];//文章取真实的 第二个
                                             
                                             if ([arrCenter count]==1) {
                                                 //不存在那个  view；；；
                                                 blk(arrCenter,nil);
                                             }
                                             else if ([arrCenter count]==0)
                                             {//存在  该 viewShiwen
                                                 blk(nil,arrSon5);
                                             }
                                             else
                                             {
                                                 NSLog(@"格式之外？？");
                                                 blk(nil,nil);
                                             }
                                             
                                         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             NSLog(@"%@",error);
                                             blk(nil,nil);
                                         }];
    
}

#pragma mark  古籍 getVerb
-(void)GetHTML_guji_simple:(NSString *)queryURI
                            :(backArr)blk
{
    
    [[ApiClient_Spider4Gushiwen shareClient] GET:queryURI
                                      parameters:nil
                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             
//                                             NSString *rawHtml = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                                             //                                             NSLog(@"%@",rawHtml);
                                             
                                             TFHpple *hppleDoc = [[TFHpple alloc] initWithHTMLData:responseObject];
                                             
                                             NSArray *arr = [hppleDoc searchWithXPathQuery:@"//div[@class='sons']"];
                                             
                                             blk(YES,arr);
                                         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             NSLog(@"%@",error);
                                             blk(NO,error);
                                         }];
}

//book 章节页面
-(void)GetHTML_guji_Chapter:(NSString *)queryURI
                          :(backArr_gujiChapter)blk
{
    
    [[ApiClient_Spider4Gushiwen shareClient] GET:queryURI
                                      parameters:nil
                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             
                                             TFHpple *hppleDoc = [[TFHpple alloc] initWithHTMLData:responseObject];
                                             
                                             NSArray *arrSon2 = [hppleDoc searchWithXPathQuery:@"//div[@class='shileft']//div[@class='son2']"];
                                             
                                             NSArray *arrBookcont = [hppleDoc searchWithXPathQuery:@"//div[@class='bookcont']"];
                                             
                                             blk(YES,arrSon2,arrBookcont);
                                         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             NSLog(@"%@",error);
                                             blk(NO,nil,nil);
                                         }];
}

//book 作者 页面
-(void)GetHTML_guji_Author:(NSString *)queryURI
                           :(backArr_gujiAuthor)blk
{
    
    [[ApiClient_Spider4Gushiwen shareClient] GET:queryURI
                                      parameters:nil
                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             
                                             TFHpple *hppleDoc = [[TFHpple alloc] initWithHTMLData:responseObject];
                                             
                                             NSArray *arrSon1 = [hppleDoc searchWithXPathQuery:@"//div[@class='shileft']//div[@class='son1']"];
                                             
                                             NSArray *arrSon2 = [hppleDoc searchWithXPathQuery:@"//div[@class='shileft']//div[@class='son2']"];
                                             
                                             blk(YES,(TFHppleElement *)[arrSon1 firstObject],(TFHppleElement *)[arrSon2 firstObject]);
                                         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             NSLog(@"%@",error);
                                             blk(NO,nil,nil);
                                         }];
}

//poetry  poet 诗人 页面
-(void)GetHTML_poet_Author:(NSString *)queryURI
                          :(backArr_gujiAuthor)blk
{
    
    [[ApiClient_Spider4Gushiwen shareClient] GET:queryURI
                                      parameters:nil
                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             
                                             TFHpple *hppleDoc = [[TFHpple alloc] initWithHTMLData:responseObject];
                                             
                                             NSArray *arrSon1 = [hppleDoc searchWithXPathQuery:@"//div[@class='shileft']//div[@class='son1']"];
                                             
                                             NSArray *arrSon2 = [hppleDoc searchWithXPathQuery:@"//div[@class='shileft']//div[@class='son2']"];
                                             
                                             NSArray *arrCenter = [hppleDoc searchWithXPathQuery:@"//center"];
                                             
                                             if ([arrCenter count]>=1) {
                                                 blk(NO,@"not",nil);
                                             }
                                             else
                                             {
                                                 blk(YES,(TFHppleElement *)[arrSon1 firstObject],(TFHppleElement *)[arrSon2 firstObject]);
                                             }

                                         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             NSLog(@"%@",error);
                                             blk(NO,nil,nil);
                                         }];
}

//book 篇章内容页面
-(void)GetHTML_guji_BookV:(NSString *)queryURI
                           :(backArr_gujiBookV)blk
{
    
    [[ApiClient_Spider4Gushiwen shareClient] GET:queryURI
                                      parameters:nil
                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             
                                             TFHpple *hppleDoc = [[TFHpple alloc] initWithHTMLData:responseObject];
                                             
                                             NSArray *arrShileft = [hppleDoc searchWithXPathQuery:@"//div[@class='shileft']"];
                                             
                                             
                                             blk(YES,[arrShileft firstObject]);
                                         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             NSLog(@"%@",error);
                                             blk(NO,nil);
                                         }];
}

//book 篇章内容  翻译页面(。。。赏析也可以共用。。。)
-(void)GetHTML_guji_bFanyiPage:(NSString *)queryURI
                              :(backArr_gujiBFanyiPage)blk
{
    
    [[ApiClient_Spider4Gushiwen shareClient] GET:queryURI
                                      parameters:nil
                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             
                                             TFHpple *hppleDoc = [[TFHpple alloc] initWithHTMLData:responseObject];
                                             
                                             NSArray *arrCenter = [hppleDoc searchWithXPathQuery:@"//center"];
                                             NSArray *arrShileft = [hppleDoc searchWithXPathQuery:@"//div[@class='shangxicont']"];
                                             
                                             if ([arrShileft count]==1) {
                                                 blk(YES,[arrShileft firstObject]);
                                             }
                                             
                                             if ([arrCenter count]>=1) {
                                                 blk(NO,@"notExist");
                                             }

                                         }
                                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             NSLog(@"%@",error);
                                             blk(NO,nil);
                                         }];
}

@end
