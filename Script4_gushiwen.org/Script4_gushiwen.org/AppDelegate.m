 //
//  AppDelegate.m
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-4.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import "AppDelegate.h"
#import "ApiClient_Spider4Gushiwen.h"
#import "MingjuMgr.h"
#import "ShiciMgr.h"
#import "GujiMgr.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //start 捕获典籍 内容页面
    NSString* dbpath = [NSHomeDirectory() stringByAppendingPathComponent:@"db/TaiXuWen.db"];
    NSLog(@"%@",dbpath);
    
    
    //test
//    [[ApiClient_Spider4Gushiwen shareClient]  GetHTML_Mingju];
    
    //start捕获名句obj
//    [[MingjuMgr shareClient] runCatchOn:1 :114];
//    [[MingjuMgr shareClient] loopTag4Mingju:0 :0];
    
    //start捕获诗文obj
//    [[ShiciMgr shareClient] runCatchOn:1 :200];
    
//    [[ShiciMgr shareClient] loopTag4Shiwen:0
//                                          :0
//                                          :0];

//    [[ShiciMgr shareClient] loop_oneTag:1
//                                       :200
//                                       :0];

//    [[ShiciMgr shareClient] loop_oneTag_B:1
//                                       :200
//                                       :0];
    
//    [[ShiciMgr shareClient] loop_oneTag_C:1
//                                         :200
//                                         :0];
    
//    2015-02-13 04:15:23.544 Script4_gushiwen.org[8936:386388] 新增：苔歌  顾云
//    2015-02-13 04:15:23.565 Script4_gushiwen.org[8936:386388]
//#LKDBHelper ERROR: +[shiciObj dbDidInserted:result:]  [Line 61]
//    did insert : shiciObj
//    2015-02-13 04:15:23.565 Script4_gushiwen.org[8936:386388] 新增：池阳醉歌赠匡庐处士姚岩杰  顾云
//    [[ShiciMgr shareClient]  loop_oneTag_tangPoetName:1
//                                                     :324];
    //go to song
//    [[ShiciMgr shareClient] loop_oneTag_songPoetName:1
//                                                    :0];
    
//    [[ShiciMgr shareClient] loop_shiwenNum:0];
    
//    [[ShiciMgr shareClient] runCatchOn_poetNum:0];
    
    
//    [[ShiciMgr shareClient] loop_shiwenNum_getMainText:0];
    //start捕获典籍obj
//    [[GujiMgr shareClient] loopTag4Guji:0];
//    [[GujiMgr shareClient] runCatchOn:1];
    
    
//    [[GujiMgr shareClient] loopAuthorNum:0];
    
//    [[GujiMgr shareClient] loopBookV_byColletionNum];
    
    
    
//    [[ShiciMgr shareClient] loop_shiwenNum_getFanyiShangxi:0];
    
//    [[GujiMgr shareClient] runCatchOn_fanYiPage:103];
//    [[GujiMgr shareClient] runFix_fanyiPage];
    
//    [[ShiciMgr shareClient] runCatchOn_shiciFanYiPage:1];
    
    
//    [[GujiMgr shareClient] loop_download_bookcover:0];
    [[GujiMgr shareClient] loop_download_authorHead:0];
    
    [[ShiciMgr shareClient] loop_download_poetHead:0];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
