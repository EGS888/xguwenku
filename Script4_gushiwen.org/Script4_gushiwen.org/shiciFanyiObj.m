//
//  shiciFanyiObj.m
//  Script4_gushiwen.org
//
//  Created by EGS on 15-3-3.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import "shiciFanyiObj.h"
#import "LKDBHelper.h"

@implementation shiciFanyiObj


//重载选择 使用的LKDBHelper
+(LKDBHelper *)getUsingLKDBHelper
{
    static LKDBHelper* db;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString* dbpath = [NSHomeDirectory() stringByAppendingPathComponent:@"db/TaiXuWen.db"];
        db = [[LKDBHelper alloc] initWithDBPath:dbpath];
    });
    return db;
}

+(NSString *)getTableName
{
    //古诗文 翻译表
    return @"GuShiWenFanyiTable";
}




//列属性
+(void)columnAttributeWithProperty:(LKDBProperty *)property
{
    if([property.sqlColumnName isEqualToString:@"shiwenNum"])
    {
        property.isNotNull = YES;
    }
    
    if([property.sqlColumnName isEqualToString:@"shiwenFanyiNum"])
    {
        property.isUnique = YES;
        property.isNotNull = YES;
    }
    
    if([property.sqlColumnName isEqualToString:@"rowid"])
    {
        property.isUnique = YES;
        property.isNotNull = YES;
    }
    
}



//已经插入数据库
+(void)dbDidInserted:(NSObject *)entity result:(BOOL)result
{
    LKErrorLog(@"did insert : %@",NSStringFromClass(self));
}


@end
