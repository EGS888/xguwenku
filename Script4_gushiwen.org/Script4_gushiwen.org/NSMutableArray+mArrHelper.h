//
//  NSMutableArray+mArrHelper.h
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-10.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (mArrHelper)

-(BOOL)checkExist:(NSString *)valueStr;


@end
