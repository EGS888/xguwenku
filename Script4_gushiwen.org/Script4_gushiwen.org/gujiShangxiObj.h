//
//  gujiShangxiObj.h
//  Script4_gushiwen.org
//
//  Created by Engels on 15-3-2.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface gujiShangxiObj : NSObject

@property (nonatomic,assign)long book_num;//古籍序号 ====>从属于此

@property (nonatomic,assign)long bookV_num;//古籍篇章序号 bookV_num  ====>从属于此

@property (nonatomic,assign)long bookV_shangxi_num;//赏析 篇章序号

@property (nonatomic,strong)NSString *bookV_shangxi_title;//赏析 篇章title

@property (nonatomic,strong)NSString *shangxiMainText;//赏析

@end
