//
//  mingjuObj.h
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-10.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LKDBHelper.h"

@interface mingjuObj : NSObject


@property (nonatomic,assign)long mingjuNum;//名句num
@property (nonatomic,assign)long shiwenNum;//诗词num

//诗文标题  1. 唯一值。
@property (nonatomic,strong)NSString * poetryTitle;

//正文
@property (nonatomic,strong)NSString * mingjuText;

//作者
@property (nonatomic,strong)NSString * poetName;


@property (nonatomic,strong)NSArray *cateA;//大分类
@property (nonatomic,strong)NSArray *cateB;//小分类

@end
