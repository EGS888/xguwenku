//
//  ShiciMgr.m
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-9.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import "ShiciMgr.h"
#import "ApiClient_Spider4Gushiwen.h"
#import "TFHpple.h"
#import "NSString+Extensions.h"
#import "NSMutableArray+mArrHelper.h"

#import "shiciObj.h"
#import "shiciFanyiObj.h"
#import "shiciShangxiObj.h"
#import "shiciPoetObj.h"
#import "SDWebImageManager.h"

@implementation ShiciMgr

+(ShiciMgr *)shareClient
{
    static ShiciMgr *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[ShiciMgr alloc]  init];
    });
    
    return _sharedClient;
}

//启动catch。====》整体获取！！！ page==200 是个极限
-(void)runCatchOn:(NSInteger)currPage
                 :(NSInteger)finishPage
{
    
    
    NSString *uriStr = [NSString stringWithFormat:@"/type.aspx?p=%ld",currPage];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [HttpVerClient GetHTML_ShiWen_simple:encodedValue
                                        :^(BOOL bol, id responseData) {
                                            
                                            if (![responseData isKindOfClass:[NSArray class]]) {
                                                sleep(8);
                                                
                                                //网关超时  重新来
                                                [self runCatchOn:currPage
                                                                :200];
                                            }
                                            
                                            for (TFHppleElement *element in responseData) {
                                                
                                                //区域share Var
                                                NSString *shiwenTitle;
                                                NSInteger shiwenNum;
                                                NSString *shiwenSimple;
                                                NSString *poetName;
                                                NSInteger poetNum = -1;
                                                NSString *poetHeadImgUrl;
                                                
                                                //1111===诗文标题  诗文num
                                                NSArray *a1Arr = [element searchWithXPathQuery:@"//a[@style=' font-size:14px;']"];
                                                
                                                if ([a1Arr count]==1) {
                                                    TFHppleElement *obj = [a1Arr firstObject];
                                                    
                                                    shiwenTitle = obj.text;
                                                    
                                                    NSString *rawNum = [[obj attributes] objectForKey:@"href"];
                                                    shiwenNum =  [[rawNum get_num_fromTxt] integerValue];
                                                    
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                //2222====诗文摘要
                                                NSArray *p1Arr = [element searchWithXPathQuery:@"//p[@style='margin-bottom:0px;']"];
                                                
                                                if ([p1Arr count]==1) {
                                                    TFHppleElement *obj = [p1Arr firstObject];
                                                    
                                                    shiwenSimple = obj.text;
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                //3333=== 诗人名字
                                                NSArray *p2Arr = [element searchWithXPathQuery:@"//p[@style='color:#676767;']"];
                                                
                                                if ([p2Arr count]==1) {
                                                    TFHppleElement *obj = [p2Arr firstObject];
                                                    
                                                    poetName = [obj.text get_poetName];
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                
                                                //44444 诗人序号  头像url=====有时可能不存在
                                                NSArray *imgArr = [element searchWithXPathQuery:@"//img"];
                                                
                                                if ([imgArr count]!=0) {
                                                    //存在 作者头像===
                                                    
                                                    NSArray *a_all_Arr = [element searchWithXPathQuery:@"//a"];
                                                    TFHppleElement *elem = [a_all_Arr objectAtIndex:0];
                                                    
                                                    NSString *rawNum = [[elem attributes] objectForKey:@"href"];
                                                    poetNum =  [[rawNum get_num_fromTxt] integerValue];
                                                    
                                                    //==
                                                    TFHppleElement *elem2 = [imgArr objectAtIndex:0];
                                                    poetHeadImgUrl = [[elem2 attributes] objectForKey:@"src"];
//                                                    NSLog(@"诗人头像图片%@",poetHeadImgUrl);
                                                }
                                                else
                                                {
//                                                    NSLog(@"不存在 诗人头像图片");
                                                }
                                                
                                                //===key 检索数据库中是否已经存入这篇  古诗文num
                                                
                                                NSString *whereStr = [NSString stringWithFormat:@"shiwenNum = %ld",shiwenNum];
                                                
                                                NSMutableArray *mArr = [shiciObj searchWithWhere:whereStr orderBy:nil offset:0 count:5] ;//@"rowid = 1"
                                                
                                                if ([mArr count]==0) {
                                                    //未存在  生成一条新数据
                                                    shiciObj *shiciO = ({
                                                        shiciObj *shiciTemp = [shiciObj new];
                                                        
                                                        shiciTemp.shiwenNum = shiwenNum;
                                                        shiciTemp.poetryTitle = shiwenTitle;
                                                        shiciTemp.poetrySnippet = shiwenSimple;
                                                        shiciTemp.poetName = poetName;
                                                        
                                                        if (poetNum!=-1)
                                                            shiciTemp.poetNum =  poetNum;
                                                        
                                                        if ([poetHeadImgUrl length]>0)
                                                            shiciTemp.poetHeadUrl = poetHeadImgUrl;
                                                        
                                                        
                                                        
                                                        shiciTemp;
                                                    });
                                                    
                                                    //首次  存入=====
                                                    [shiciO saveToDB];
                                                    NSLog(@"新增：%@",shiciO.poetName);
                                                }
                                                else if ([mArr count]==1)
                                                {//已经存在一条。 正常情况。
                                                    shiciObj *shiciO = (shiciObj *)[mArr objectAtIndex:0];
                                                    
                                                    if (poetNum==-1
                                                        &&shiciO.poetNum!=-1) {
                                                        shiciO.poetNum = -1;
                                                        
                                                        
                                                        
                                                        //更新条目====
                                                        [shiciO updateToDB];
                                                        NSLog(@"更新：%@",shiciO.poetName);
                                                    }
                                                    
                                                    if ([poetHeadImgUrl length]>0
                                                        &&shiciO.poetHeadUrl==nil){
                                                        shiciO.poetHeadUrl = poetHeadImgUrl;
                                                        [shiciO updateToDB];
                                                        NSLog(@"更新：%@",shiciO.poetName);
                                                    }
                                                    
                                                    
                                                    
                                                }
                                                else if ([mArr count]>1)
                                                {// 不应该出现  两条一样的数据！！
                                                    NSLog(@" 不应该出现  两条一样的数据！！");
                                                }
                                                
                                            }
                                            
                                            if ([responseData count]>0) {
                                                [self runCatchOn:currPage+1
                                                                :200];//loop on page
                                            }
                                            else if ([responseData count]==0)
                                            {
                                                NSLog(@"finish page1---200");
                                            }
                                        }];
    
    
}


-(void)loopTag4Shiwen:(NSInteger)indexA
                     :(NSInteger)indexB
                     :(NSInteger)indexC
{
    NSArray *cateA  = @[@"先秦",@"两汉",@"魏晋",@"南北朝",@"隋代",@"唐代",@"五代",@"宋代",@"金朝",@"元代",@"明代",@"清代"];
    
    NSArray *cateB  = @[@"诗",@"词",@"曲",@"文言文",@"辞赋"];//形式
    
    NSArray *cateC  = @[@"写景",@"咏物",@"春天",@"夏天",@"秋天",@"冬天",@"写雨",@"写雪",@"写风",
                        @"写花",@"梅花",@"荷花",@"菊花",@"柳树",
                        @"月亮",@"山水",@"写山",@"写水",@"长江",@"黄河",
                        @"儿童",@"写鸟",@"写马",
                        @"田园",@"边塞",@"地名",@"抒情",@"爱国",@"离别",
                        @"送别",@"思乡",@"思念",@"爱情",
                        @"励志",@"哲理",@"闺怨",@"悼亡",@"写人",
                        @"老师",@"母亲",@"友情",@"战争",
                        @"读书",@"惜时",@"婉约",@"豪放",@"诗经",@"民谣",
                        @"节日",@"春节",@"元宵节",@"寒食节",@"清明节",@"端午节",@"七夕节",@"中秋节",@"重阳节",
                        @"忧国忧民",@"咏史怀古",
                        @"宋词精选",
                        @"小学古诗",@"初中古诗",@"高中古诗",
                        @"小学文言文",@"初中文言文",@"高中文言文",@"古诗十九首",@"唐诗三百首",@"古诗三百首",@"宋词三百首",
                        @"古文观止"];//类型 71

    if (indexA==0
        &&indexB==0
        &&indexC==0) {
        
        NSString *cateStrA = [cateA objectAtIndex:indexA];
        NSString *cateStrB = [cateB objectAtIndex:indexB];
        NSString *cateStrC = [cateC objectAtIndex:indexC];
        
        [self runCatchOn:1
                        :cateStrA
                        :cateStrB
                        :cateStrC
                        :indexA
                        :indexB
                        :indexC];
    }
    
    if (indexA+1 < [cateA count]) {
        //===loop A
        
       
        
        if (indexB+1 < [cateB count]) {
            //===loop B
            
            
            
            if (indexC+1 < [cateC count]) {
                //===loop C
                
                NSString *cateStrA = [cateA objectAtIndex:indexA];
                NSString *cateStrB = [cateB objectAtIndex:indexB];
                NSString *cateStrC = [cateC objectAtIndex:indexC+1];
                
                [self runCatchOn:1
                                :cateStrA
                                :cateStrB
                                :cateStrC
                                :indexA
                                :indexB
                                :indexC];
            }
            else if (indexC+1 >= [cateC count])
            {//C 触底 上推 B
                NSString *cateStrA = [cateA objectAtIndex:indexA];
                NSString *cateStrB = [cateB objectAtIndex:indexB+1];
                NSString *cateStrC = [cateC objectAtIndex:0];
                
                [self runCatchOn:1
                                :cateStrA
                                :cateStrB
                                :cateStrC
                                :indexA
                                :indexB+1
                                :0];
            }
            
        }
        else if (indexB+1 >= [cateB count])
        {//B 触底 上推 A
        
            if (indexA+1 ==12) {
                 NSLog(@"finish all  tag combi");
                
                return;
            }
            
            NSString *cateStrA = [cateA objectAtIndex:indexA+1];
            NSString *cateStrB = [cateB objectAtIndex:0];
            NSString *cateStrC = [cateC objectAtIndex:0];
            
            [self runCatchOn:1
                            :cateStrA
                            :cateStrB
                            :cateStrC
                            :indexA+1
                            :0
                            :0];
        }
        
        
    }
    else
    {
        NSLog(@"finish all");
    }
    
}

//启动catch。===》分类情况获取
-(void)runCatchOn:(NSInteger)currPage
                 :(NSString *)categoryA
                 :(NSString *)categoryB
                 :(NSString *)categoryC
                 :(NSInteger)indexA
                 :(NSInteger)indexB
                 :(NSInteger)indexC
{
    NSString *uriStr = [NSString stringWithFormat:@"/type.aspx?p=%ld&t=%@&c=%@&x=%@",currPage,categoryC,categoryA,categoryB];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [HttpVerClient GetHTML_ShiWen_simple:encodedValue
                                        :^(BOOL bol, id responseData) {
                                     
                                            if (![responseData isKindOfClass:[NSArray class]]) {
                                                sleep(8);
                                                
                                                //网关超时  重新来
                                                [self runCatchOn:currPage
                                                                :categoryA
                                                                :categoryB
                                                                :categoryC
                                                                :indexA
                                                                :indexB
                                                                :indexC];
                                            }
                                            
                                     for (TFHppleElement *element in responseData) {
                                         
                                         //区域share Var
                                         NSString *shiwenTitle;
                                         NSInteger shiwenNum;
                                         NSString *shiwenSimple;
                                         NSString *poetName;
                                         NSInteger poetNum = -1;
                                         NSString *poetHeadImgUrl;
                                         
                                         //1111===诗文标题  诗文num
                                         NSArray *a1Arr = [element searchWithXPathQuery:@"//a[@style=' font-size:14px;']"];
                                         
                                         if ([a1Arr count]==1) {
                                             TFHppleElement *obj = [a1Arr firstObject];
                                             
                                             shiwenTitle = obj.text;
                                             
                                             NSString *rawNum = [[obj attributes] objectForKey:@"href"];
                                             shiwenNum =  [[rawNum get_num_fromTxt] integerValue];
                                             
                                         }
                                         else
                                         {
                                             NSLog(@"只应该有一行标题");
                                         }
                                         
                                         //2222====诗文摘要
                                         NSArray *p1Arr = [element searchWithXPathQuery:@"//p[@style='margin-bottom:0px;']"];
                                         
                                         if ([p1Arr count]==1) {
                                             TFHppleElement *obj = [p1Arr firstObject];
                                             
                                             shiwenSimple = obj.text;
                                         }
                                         else
                                         {
                                             NSLog(@"只应该有一行标题");
                                         }
                                         
                                         //3333=== 诗人名字
                                         NSArray *p2Arr = [element searchWithXPathQuery:@"//p[@style='color:#676767;']"];
                                         
                                         if ([p2Arr count]==1) {
                                             TFHppleElement *obj = [p2Arr firstObject];
                                             
                                             poetName = [obj.text get_poetName];
                                         }
                                         else
                                         {
                                             NSLog(@"只应该有一行标题");
                                         }
                                         
                                         
                                         //44444 诗人序号  头像url=====有时可能不存在
                                         NSArray *imgArr = [element searchWithXPathQuery:@"//img"];
                                         
                                         if ([imgArr count]!=0) {
                                             //存在 作者头像===
                                             
                                             NSArray *a_all_Arr = [element searchWithXPathQuery:@"//a"];
                                             TFHppleElement *elem = [a_all_Arr objectAtIndex:0];
                                             
                                             NSString *rawNum = [[elem attributes] objectForKey:@"href"];
                                             poetNum =  [[rawNum get_num_fromTxt] integerValue];
                                             
                                             //==
                                             TFHppleElement *elem2 = [imgArr objectAtIndex:0];
                                             poetHeadImgUrl = [[elem2 attributes] objectForKey:@"src"];
                                         }
                                         else
                                         {
                                             NSLog(@"不存在 诗人头像图片");
                                         }
                                         
                                         //===key 检索数据库中是否已经存入这篇  古诗文num
                                         
                                         NSString *whereStr = [NSString stringWithFormat:@"shiwenNum = %ld",shiwenNum];
                                         
                                         NSMutableArray *mArr = [shiciObj searchWithWhere:whereStr orderBy:nil offset:0 count:5] ;//@"rowid = 1"
                                         
                                         if ([mArr count]==0) {
                                             //未存在  生成一条新数据
                                             shiciObj *shiciO = ({
                                                 shiciObj *shiciTemp = [shiciObj new];
                                                 
                                                 shiciTemp.shiwenNum = shiwenNum;
                                                 shiciTemp.poetryTitle = shiwenTitle;
                                                 shiciTemp.poetrySnippet = shiwenSimple;
                                                 shiciTemp.poetName = poetName;
                                                 
                                                 if (poetNum!=-1)
                                                 shiciTemp.poetNum =  poetNum;
                                                 
                                                 if ([poetHeadImgUrl length]>0)
                                                 shiciTemp.poetHeadUrl = poetHeadImgUrl;
                                                 
                                                 shiciTemp.str_ChaoDai = categoryA;
                                                 shiciTemp.cate_XingShi = @[categoryB];
                                                 shiciTemp.cate_LeiXing = @[categoryC];
                                                 
                                                 
                                                 shiciTemp;
                                             });
                                             
                                             //首次  存入=====
                                             [shiciO saveToDB];
                                         }
                                         else if ([mArr count]==1)
                                         {//已经存在一条。 正常情况。
                                             shiciObj *shiciO = (shiciObj *)[mArr objectAtIndex:0];
                                             
                                             if (poetNum==-1
                                                 &&shiciO.poetNum!=-1) {
                                                 shiciO.poetNum = -1;
                                                 
                                                 
                                                 
                                                 //更新条目====
                                                 [shiciO updateToDB];
                                                 NSLog(@"更新poetNum：%@",shiciO.poetName);
                                             }
                                             
                                             //更新头像
                                             if ([poetHeadImgUrl length]>0
                                                 &&shiciO.poetHeadUrl==nil){
                                                 shiciO.poetHeadUrl = poetHeadImgUrl;
                                                 [shiciO updateToDB];
                                                 NSLog(@"更新PoetHeadUrl：%@",shiciO.poetName);
                                             }
                                             
                                             //cate 形式
                                             if (shiciO.cate_XingShi==nil) {
                                                 shiciO.cate_XingShi = @[categoryB];
                                                 
                                                 [shiciO updateToDB];
                                                 NSLog(@"更新cate_XingShi：%@",shiciO.poetName);
                                             }
                                             else if ([shiciO.cate_XingShi count]>0)
                                             {
                                                 if (![shiciO.cate_XingShi checkExist:categoryB]) {
                                                     
                                                     NSMutableArray *mArrTemp = [NSMutableArray arrayWithArray:shiciO.cate_XingShi];
                                                     [mArrTemp addObject:categoryB];
                                                     
                                                     shiciO.cate_XingShi = [mArrTemp copy];
                                                     
                                                     [shiciO updateToDB];
                                                     NSLog(@"更新cate_XingShi：%@",shiciO.poetName);
                                                 }
                                             }
                                             
                                             //cate 类型
                                             if (shiciO.cate_LeiXing==nil) {
                                                 shiciO.cate_LeiXing = @[categoryC];
                                                 
                                                 [shiciO updateToDB];
                                                 NSLog(@"更新cate_LeiXing：%@",shiciO.poetName);
                                             }
                                             else if ([shiciO.cate_LeiXing count]>0)
                                             {
                                                 if (![shiciO.cate_LeiXing checkExist:categoryC]) {
                                                     
                                                     NSMutableArray *mArrTemp = [NSMutableArray arrayWithArray:shiciO.cate_LeiXing];
                                                     [mArrTemp addObject:categoryC];
                                                     
                                                     shiciO.cate_LeiXing = [mArrTemp copy];
                                                     
                                                     [shiciO updateToDB];
                                                     NSLog(@"更新cate_LeiXing：%@",shiciO.poetName);
                                                 }
                                             }
                                              
                                         }
                                         else if ([mArr count]>1)
                                         {// 不应该出现  两条一样的数据！！
                                             NSLog(@" 不应该出现  两条一样的数据！！");
                                         }
                                         
                                     }
                                     
                                     if ([responseData count]>0) {
                                         [self runCatchOn:currPage+1
                                                         :categoryA
                                                         :categoryB
                                                         :categoryC
                                                         :indexA
                                                         :indexB
                                                         :indexC];//loop on page
                                     }
                                     else if ([responseData count]==0)
                                     {
                                         NSLog(@"cate%@  %@  %@ get finish",categoryA,categoryB,categoryC);
                                         
                                         [self loopTag4Shiwen:indexA
                                                             :indexB
                                                             :indexC+1];
                                     }
                                 }];
}

//tag 在一个tag捕获 objShiwen========AAAAAAAA
-(void)loop_oneTag:(NSInteger)currPage
                  :(NSInteger)finishPage
                  :(NSInteger)indexA
{
    NSArray *cateA  = @[@"先秦",@"两汉",@"魏晋",@"南北朝",@"隋代",@"唐代",@"五代",@"宋代",@"金朝",@"元代",@"明代",@"清代"];
    
 
    
    if (indexA<[cateA count]) {
        
        [self runCatchOn:1
                        :[cateA objectAtIndex:indexA]
                        :indexA];
    }
    else
    {
        NSLog(@"finish  cateA");
    }

    
    
    
}

-(void)runCatchOn:(NSInteger)currPage
                 :(NSString *)categoryA
                 :(NSInteger)indexA
{
    
    NSString *uriStr = [NSString stringWithFormat:@"/type.aspx?p=%ld&c=%@",currPage,categoryA];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [HttpVerClient GetHTML_ShiWen_simple:encodedValue
                                        :^(BOOL bol, id responseData) {
                                            
                                            if (![responseData isKindOfClass:[NSArray class]]) {
                                                sleep(8);
                                                
                                                //网关超时  重新来
                                                [self runCatchOn:currPage
                                                                :categoryA
                                                                :indexA];
                                            }
                                            
                                            for (TFHppleElement *element in responseData) {
                                                
                                                //区域share Var
                                                NSString *shiwenTitle;
                                                NSInteger shiwenNum;
                                                NSString *shiwenSimple;
                                                NSString *poetName;
                                                NSInteger poetNum = -1;
                                                NSString *poetHeadImgUrl;
                                                
                                                //1111===诗文标题  诗文num
                                                NSArray *a1Arr = [element searchWithXPathQuery:@"//a[@style=' font-size:14px;']"];
                                                
                                                if ([a1Arr count]==1) {
                                                    TFHppleElement *obj = [a1Arr firstObject];
                                                    
                                                    shiwenTitle = obj.text;
                                                    
                                                    NSString *rawNum = [[obj attributes] objectForKey:@"href"];
                                                    shiwenNum =  [[rawNum get_num_fromTxt] integerValue];
                                                    
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                //2222====诗文摘要
                                                NSArray *p1Arr = [element searchWithXPathQuery:@"//p[@style='margin-bottom:0px;']"];
                                                
                                                if ([p1Arr count]==1) {
                                                    TFHppleElement *obj = [p1Arr firstObject];
                                                    
                                                    shiwenSimple = obj.text;
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                //3333=== 诗人名字
                                                NSArray *p2Arr = [element searchWithXPathQuery:@"//p[@style='color:#676767;']"];
                                                
                                                if ([p2Arr count]==1) {
                                                    TFHppleElement *obj = [p2Arr firstObject];
                                                    
                                                    poetName = [obj.text get_poetName];
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                
                                                //44444 诗人序号  头像url=====有时可能不存在
                                                NSArray *imgArr = [element searchWithXPathQuery:@"//img"];
                                                
                                                if ([imgArr count]!=0) {
                                                    //存在 作者头像===
                                                    
                                                    NSArray *a_all_Arr = [element searchWithXPathQuery:@"//a"];
                                                    TFHppleElement *elem = [a_all_Arr objectAtIndex:0];
                                                    
                                                    NSString *rawNum = [[elem attributes] objectForKey:@"href"];
                                                    poetNum =  [[rawNum get_num_fromTxt] integerValue];
                                                    
                                                    //==
                                                    TFHppleElement *elem2 = [imgArr objectAtIndex:0];
                                                    poetHeadImgUrl = [[elem2 attributes] objectForKey:@"src"];
                                                }
                                                else
                                                {
//                                                    NSLog(@"不存在 诗人头像图片");
                                                }
                                                
                                                //===key 检索数据库中是否已经存入这篇  古诗文num
                                                
                                                NSString *whereStr = [NSString stringWithFormat:@"shiwenNum = %ld",shiwenNum];
                                                
                                                NSMutableArray *mArr = [shiciObj searchWithWhere:whereStr
                                                                                         orderBy:nil
                                                                                          offset:0
                                                                                           count:5] ;//@"rowid = 1"
                                                
                                                if ([mArr count]==0) {
                                                    //未存在  生成一条新数据
                                                    shiciObj *shiciO = ({
                                                        shiciObj *shiciTemp = [shiciObj new];
                                                        
                                                        shiciTemp.shiwenNum = shiwenNum;
                                                        shiciTemp.poetryTitle = shiwenTitle;
                                                        shiciTemp.poetrySnippet = shiwenSimple;
                                                        shiciTemp.poetName = poetName;
                                                        
                                                        if (poetNum!=-1)
                                                            shiciTemp.poetNum =  poetNum;
                                                        
                                                        if ([poetHeadImgUrl length]>0)
                                                            shiciTemp.poetHeadUrl = poetHeadImgUrl;
                                                        
                                                        shiciTemp.str_ChaoDai = categoryA;
                                                        
                                                        
                                                        shiciTemp;
                                                    });
                                                    
                                                    //首次  存入=====

                                                    if (![shiciO saveToDB]) {
                                                        NSLog(@"新增失败");
                                                    }
                                                    else{
                                                        NSLog(@"新增：%@",shiciO.poetryTitle);
                                                    }
                                                }
                                                else if ([mArr count]==1)
                                                {//已经存在一条。 正常情况。
                                                    shiciObj *shiciO = (shiciObj *)[mArr objectAtIndex:0];
                                                    
                                                    if (poetNum==-1
                                                        &&shiciO.poetNum!=-1) {
                                                        shiciO.poetNum = -1;
                                                        
                                                        //更新条目====
                                                        if (![shiciO updateToDB]) {
                                                            NSLog(@"为何更新失败！！");
                                                        }
                                                        else
                                                        {
                                                            NSLog(@"更新：%@",shiciO.poetryTitle);
                                                        }
                                                        
                                                    }
                                                    
                                                    if (shiciO.str_ChaoDai!=nil
                                                        &&![shiciO.str_ChaoDai isEqualToString:categoryA]) {
                                                        NSLog(@"大错，怎么会有属于两个朝代！！");
                                                    }
                                                    else if (shiciO.str_ChaoDai==nil)
                                                    {
                                                        shiciO.str_ChaoDai = categoryA;
                                                        
                                                        //更新条目====
                                                        if (![shiciO updateToDB]) {
                                                            NSLog(@"为何更新失败！！");
                                                        }
                                                        else
                                                        {
                                                            NSLog(@"更新：%@",shiciO.poetryTitle);
                                                        }
                                                    }
                                                    
                                                    //更新头像
                                                    if ([poetHeadImgUrl length]>0
                                                        &&shiciO.poetHeadUrl==nil){
                                                        shiciO.poetHeadUrl = poetHeadImgUrl;
                                                        [shiciO updateToDB];
                                                        NSLog(@"更新PoetHeadUrl：%@",shiciO.poetName);
                                                    }
                                                }
                                                else if ([mArr count]>1)
                                                {// 不应该出现  两条一样的数据！！
                                                    NSLog(@" 不应该出现  两条一样的数据！！");
                                                }
                                                
                                            }
                                            
                                            if ([responseData count]>0) {
                                                [self runCatchOn:currPage+1
                                                                :categoryA
                                                                :indexA];//loop on page
                                            }
                                            else if ([responseData count]==0)
                                            {
                                                NSLog(@"cate%@ get finish",categoryA);
                                                
                                                [self loop_oneTag:1
                                                                 :200
                                                                 :indexA+1];//  +1
                                            }
                                        }];

    
}



//tag 在一个tag捕获 objShiwen========BBBBBBBB
-(void)loop_oneTag_B:(NSInteger)currPage
                  :(NSInteger)finishPage
                  :(NSInteger)indexB
{

    
    NSArray *cateB  = @[@"诗",@"词",@"曲",@"文言文",@"辞赋"];//形式
    
 
    
    if (indexB<[cateB count]) {
        
        [self runCatchOn_B:1
                        :[cateB objectAtIndex:indexB]
                        :indexB];
    }
    else
    {
        NSLog(@"finish  cateB");
    }
    
    
    
    
}

-(void)runCatchOn_B:(NSInteger)currPage
                 :(NSString *)categoryB
                 :(NSInteger)indexB
{
    
    NSString *uriStr = [NSString stringWithFormat:@"/type.aspx?p=%ld&x=%@",currPage,categoryB];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [HttpVerClient GetHTML_ShiWen_simple:encodedValue
                                        :^(BOOL bol, id responseData) {
                                            
                                            if (![responseData isKindOfClass:[NSArray class]]) {
                                                sleep(8);
                                                
                                                //网关超时  重新来
                                                [self runCatchOn_B:currPage
                                                                :categoryB
                                                                :indexB];
                                            }
                                            
                                            for (TFHppleElement *element in responseData) {
                                                
                                                //区域share Var
                                                NSString *shiwenTitle;
                                                NSInteger shiwenNum;
                                                NSString *shiwenSimple;
                                                NSString *poetName;
                                                NSInteger poetNum = -1;
                                                NSString *poetHeadImgUrl;
                                                
                                                //1111===诗文标题  诗文num
                                                NSArray *a1Arr = [element searchWithXPathQuery:@"//a[@style=' font-size:14px;']"];
                                                
                                                if ([a1Arr count]==1) {
                                                    TFHppleElement *obj = [a1Arr firstObject];
                                                    
                                                    shiwenTitle = obj.text;
                                                    
                                                    NSString *rawNum = [[obj attributes] objectForKey:@"href"];
                                                    shiwenNum =  [[rawNum get_num_fromTxt] integerValue];
                                                    
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                //2222====诗文摘要
                                                NSArray *p1Arr = [element searchWithXPathQuery:@"//p[@style='margin-bottom:0px;']"];
                                                
                                                if ([p1Arr count]==1) {
                                                    TFHppleElement *obj = [p1Arr firstObject];
                                                    
                                                    shiwenSimple = obj.text;
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                //3333=== 诗人名字
                                                NSArray *p2Arr = [element searchWithXPathQuery:@"//p[@style='color:#676767;']"];
                                                
                                                if ([p2Arr count]==1) {
                                                    TFHppleElement *obj = [p2Arr firstObject];
                                                    
                                                    poetName = [obj.text get_poetName];
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                
                                                //44444 诗人序号  头像url=====有时可能不存在
                                                NSArray *imgArr = [element searchWithXPathQuery:@"//img"];
                                                
                                                if ([imgArr count]!=0) {
                                                    //存在 作者头像===
                                                    
                                                    NSArray *a_all_Arr = [element searchWithXPathQuery:@"//a"];
                                                    TFHppleElement *elem = [a_all_Arr objectAtIndex:0];
                                                    
                                                    NSString *rawNum = [[elem attributes] objectForKey:@"href"];
                                                    poetNum =  [[rawNum get_num_fromTxt] integerValue];
                                                    
                                                    //==
                                                    TFHppleElement *elem2 = [imgArr objectAtIndex:0];
                                                    poetHeadImgUrl = [[elem2 attributes] objectForKey:@"src"];
                                                }
                                                else
                                                {
                                                    //                                                    NSLog(@"不存在 诗人头像图片");
                                                }
                                                
                                                //===key 检索数据库中是否已经存入这篇  古诗文num
                                                
                                                NSString *whereStr = [NSString stringWithFormat:@"shiwenNum = %ld",shiwenNum];
                                                
                                                NSMutableArray *mArr = [shiciObj searchWithWhere:whereStr
                                                                                         orderBy:nil
                                                                                          offset:0
                                                                                           count:5] ;//@"rowid = 1"
                                                
                                                if ([mArr count]==0) {
                                                    //未存在  生成一条新数据
                                                    shiciObj *shiciO = ({
                                                        shiciObj *shiciTemp = [shiciObj new];
                                                        
                                                        shiciTemp.shiwenNum = shiwenNum;
                                                        shiciTemp.poetryTitle = shiwenTitle;
                                                        shiciTemp.poetrySnippet = shiwenSimple;
                                                        shiciTemp.poetName = poetName;
                                                        
                                                        if (poetNum!=-1)
                                                            shiciTemp.poetNum =  poetNum;
                                                        
                                                        if ([poetHeadImgUrl length]>0)
                                                            shiciTemp.poetHeadUrl = poetHeadImgUrl;
                                                        
                                                        
                                                        
                                                        shiciTemp;
                                                    });
                                                    
                                                    //首次  存入=====
                                                    
                                                    if (![shiciO saveToDB]) {
                                                        NSLog(@"新增失败");
                                                    }
                                                    else{
                                                        NSLog(@"新增：%@",shiciO.poetryTitle);
                                                    }
                                                }
                                                else if ([mArr count]==1)
                                                {//已经存在一条。 正常情况。
                                                    shiciObj *shiciO = (shiciObj *)[mArr objectAtIndex:0];
                                                    
                                                    if (poetNum==-1
                                                        &&shiciO.poetNum!=-1) {
                                                        shiciO.poetNum = -1;
                                                        
                                                        //更新条目====
                                                        if (![shiciO updateToDB]) {
                                                            NSLog(@"为何更新失败！！");
                                                        }
                                                        else
                                                        {
                                                            NSLog(@"更新：%@",shiciO.poetryTitle);
                                                        }
                                                        
                                                    }
                                                    
                                                    //更新头像
                                                    if ([poetHeadImgUrl length]>0
                                                        &&shiciO.poetHeadUrl==nil){
                                                        shiciO.poetHeadUrl = poetHeadImgUrl;
                                                        [shiciO updateToDB];
                                                        NSLog(@"更新PoetHeadUrl：%@",shiciO.poetName);
                                                    }
                                                    
                                                    //cate 形式
                                                    if (shiciO.cate_XingShi==nil) {
                                                        shiciO.cate_XingShi = @[categoryB];
                                                        
                                                        //更新条目====
                                                        if (![shiciO updateToDB]) {
                                                            NSLog(@"为何更新失败！！");
                                                        }
                                                        else
                                                        {
                                                            NSLog(@"更新：%@",shiciO.poetryTitle);
                                                        }
                                                    }
                                                    else if ([shiciO.cate_XingShi count]>0)
                                                    {
                                                        if (![shiciO.cate_XingShi checkExist:categoryB]) {
                                                            
                                                            NSMutableArray *mArrTemp = [NSMutableArray arrayWithArray:shiciO.cate_XingShi];
                                                            [mArrTemp addObject:categoryB];
                                                            
                                                            shiciO.cate_XingShi = [mArrTemp copy];
                                                            
                                                            //更新条目====
                                                            if (![shiciO updateToDB]) {
                                                                NSLog(@"为何更新失败！！");
                                                            }
                                                            else
                                                            {
                                                                NSLog(@"更新：%@",shiciO.poetryTitle);
                                                            }
                                                        }
                                                    }
                                                    
 
                                                    
                                                    
                                                }
                                                else if ([mArr count]>1)
                                                {// 不应该出现  两条一样的数据！！
                                                    NSLog(@" 不应该出现  两条一样的数据！！");
                                                }
                                                
                                            }
                                            
                                            if ([responseData count]>0) {
                                                [self runCatchOn_B:currPage+1
                                                                :categoryB
                                                                :indexB];//loop on page
                                            }
                                            else if ([responseData count]==0)
                                            {
                                                NSLog(@"cate%@ get finish",categoryB);
                                                
                                                [self loop_oneTag_B:1
                                                                 :200
                                                                 :indexB+1];//  +1
                                            }
                                        }];
    
    
}

//tag 在一个tag捕获 objShiwen========CCCCCCCC
-(void)loop_oneTag_C:(NSInteger)currPage
                  :(NSInteger)finishPage
                  :(NSInteger)indexC
{
    
    NSArray *cateC  = @[@"写景",@"咏物",@"春天",@"夏天",@"秋天",@"冬天",@"写雨",@"写雪",@"写风",
                        @"写花",@"梅花",@"荷花",@"菊花",@"柳树",
                        @"月亮",@"山水",@"写山",@"写水",@"长江",@"黄河",
                        @"儿童",@"写鸟",@"写马",
                        @"田园",@"边塞",@"地名",@"抒情",@"爱国",@"离别",
                        @"送别",@"思乡",@"思念",@"爱情",
                        @"励志",@"哲理",@"闺怨",@"悼亡",@"写人",
                        @"老师",@"母亲",@"友情",@"战争",
                        @"读书",@"惜时",@"婉约",@"豪放",@"诗经",@"民谣",
                        @"节日",@"春节",@"元宵节",@"寒食节",@"清明节",@"端午节",@"七夕节",@"中秋节",@"重阳节",
                        @"忧国忧民",@"咏史怀古",
                        @"宋词精选",
                        @"小学古诗",@"初中古诗",@"高中古诗",
                        @"小学文言文",@"初中文言文",@"高中文言文",@"古诗十九首",@"唐诗三百首",@"古诗三百首",@"宋词三百首",
                        @"古文观止"];//类型 71
    
    if (indexC<[cateC count]) {
        
        [self runCatchOn_C:1
                        :[cateC objectAtIndex:indexC]
                        :indexC];
    }
    else
    {
        NSLog(@"finish  cateC");
    }
    
    
    
    
}

-(void)runCatchOn_C:(NSInteger)currPage
                 :(NSString *)categoryC
                 :(NSInteger)indexC
{
    
    NSString *uriStr = [NSString stringWithFormat:@"/type.aspx?p=%ld&t=%@",currPage,categoryC];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [HttpVerClient GetHTML_ShiWen_simple:encodedValue
                                        :^(BOOL bol, id responseData) {
                                            
                                            if (![responseData isKindOfClass:[NSArray class]]) {
                                                sleep(8);
                                                
                                                //网关超时  重新来
                                                [self runCatchOn_C:currPage
                                                                :categoryC
                                                                :indexC];
                                            }
                                            
                                            for (TFHppleElement *element in responseData) {
                                                
                                                //区域share Var
                                                NSString *shiwenTitle;
                                                NSInteger shiwenNum;
                                                NSString *shiwenSimple;
                                                NSString *poetName;
                                                NSInteger poetNum = -1;
                                                NSString *poetHeadImgUrl;
                                                
                                                //1111===诗文标题  诗文num
                                                NSArray *a1Arr = [element searchWithXPathQuery:@"//a[@style=' font-size:14px;']"];
                                                
                                                if ([a1Arr count]==1) {
                                                    TFHppleElement *obj = [a1Arr firstObject];
                                                    
                                                    shiwenTitle = obj.text;
                                                    
                                                    NSString *rawNum = [[obj attributes] objectForKey:@"href"];
                                                    shiwenNum =  [[rawNum get_num_fromTxt] integerValue];
                                                    
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                //2222====诗文摘要
                                                NSArray *p1Arr = [element searchWithXPathQuery:@"//p[@style='margin-bottom:0px;']"];
                                                
                                                if ([p1Arr count]==1) {
                                                    TFHppleElement *obj = [p1Arr firstObject];
                                                    
                                                    shiwenSimple = obj.text;
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                //3333=== 诗人名字
                                                NSArray *p2Arr = [element searchWithXPathQuery:@"//p[@style='color:#676767;']"];
                                                
                                                if ([p2Arr count]==1) {
                                                    TFHppleElement *obj = [p2Arr firstObject];
                                                    
                                                    poetName = [obj.text get_poetName];
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                
                                                //44444 诗人序号  头像url=====有时可能不存在
                                                NSArray *imgArr = [element searchWithXPathQuery:@"//img"];
                                                
                                                if ([imgArr count]!=0) {
                                                    //存在 作者头像===
                                                    
                                                    NSArray *a_all_Arr = [element searchWithXPathQuery:@"//a"];
                                                    TFHppleElement *elem = [a_all_Arr objectAtIndex:0];
                                                    
                                                    NSString *rawNum = [[elem attributes] objectForKey:@"href"];
                                                    poetNum =  [[rawNum get_num_fromTxt] integerValue];
                                                    
                                                    //==
                                                    TFHppleElement *elem2 = [imgArr objectAtIndex:0];
                                                    poetHeadImgUrl = [[elem2 attributes] objectForKey:@"src"];
                                                }
                                                else
                                                {
                                                    //                                                    NSLog(@"不存在 诗人头像图片");
                                                }
                                                
                                                //===key 检索数据库中是否已经存入这篇  古诗文num
                                                
                                                NSString *whereStr = [NSString stringWithFormat:@"shiwenNum = %ld",shiwenNum];
                                                
                                                NSMutableArray *mArr = [shiciObj searchWithWhere:whereStr
                                                                                         orderBy:nil
                                                                                          offset:0
                                                                                           count:5] ;//@"rowid = 1"
                                                
                                                if ([mArr count]==0) {
                                                    //未存在  生成一条新数据
                                                    shiciObj *shiciO = ({
                                                        shiciObj *shiciTemp = [shiciObj new];
                                                        
                                                        shiciTemp.shiwenNum = shiwenNum;
                                                        shiciTemp.poetryTitle = shiwenTitle;
                                                        shiciTemp.poetrySnippet = shiwenSimple;
                                                        shiciTemp.poetName = poetName;
                                                        
                                                        if (poetNum!=-1)
                                                            shiciTemp.poetNum =  poetNum;
                                                        
                                                        if ([poetHeadImgUrl length]>0)
                                                            shiciTemp.poetHeadUrl = poetHeadImgUrl;
                                                        
//                                                        shiciTemp.str_ChaoDai = categoryA;
                                                        
                                                        
                                                        shiciTemp;
                                                    });
                                                    
                                                    //首次  存入=====
                                                    
                                                    if (![shiciO saveToDB]) {
                                                        NSLog(@"新增失败");
                                                    }
                                                    else{
                                                        NSLog(@"新增：%@",shiciO.poetryTitle);
                                                    }
                                                }
                                                else if ([mArr count]==1)
                                                {//已经存在一条。 正常情况。
                                                    shiciObj *shiciO = (shiciObj *)[mArr objectAtIndex:0];
                                                    
                                                    if (poetNum==-1
                                                        &&shiciO.poetNum!=-1) {
                                                        shiciO.poetNum = -1;
                                                        
                                                        //更新条目====
                                                        if (![shiciO updateToDB]) {
                                                            NSLog(@"为何更新失败！！");
                                                        }
                                                        else
                                                        {
                                                            NSLog(@"更新：%@",shiciO.poetryTitle);
                                                        }
                                                        
                                                    }
                                                    
                                                    //更新头像
                                                    if ([poetHeadImgUrl length]>0
                                                        &&shiciO.poetHeadUrl==nil){
                                                        shiciO.poetHeadUrl = poetHeadImgUrl;
                                                        [shiciO updateToDB];
                                                        NSLog(@"更新PoetHeadUrl：%@",shiciO.poetName);
                                                    }
                                                    
                                                    //cate 类型
                                                    if (shiciO.cate_LeiXing==nil) {
                                                        shiciO.cate_LeiXing = @[categoryC];
                                                        
                                                        //更新条目====
                                                        if (![shiciO updateToDB]) {
                                                            NSLog(@"为何更新失败！！");
                                                        }
                                                        else
                                                        {
                                                            NSLog(@"更新：%@",shiciO.poetryTitle);
                                                        }
                                                    }
                                                    else if ([shiciO.cate_LeiXing count]>0)
                                                    {
                                                        if (![shiciO.cate_LeiXing checkExist:categoryC]) {
                                                            
                                                            NSMutableArray *mArrTemp = [NSMutableArray arrayWithArray:shiciO.cate_LeiXing];
                                                            [mArrTemp addObject:categoryC];
                                                            
                                                            shiciO.cate_LeiXing = [mArrTemp copy];
                                                            
                                                            //更新条目====
                                                            if (![shiciO updateToDB]) {
                                                                NSLog(@"为何更新失败！！");
                                                            }
                                                            else
                                                            {
                                                                NSLog(@"更新：%@",shiciO.poetryTitle);
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                                else if ([mArr count]>1)
                                                {// 不应该出现  两条一样的数据！！
                                                    NSLog(@" 不应该出现  两条一样的数据！！");
                                                }
                                                
                                            }
                                            
                                            if ([responseData count]>0) {
                                                [self runCatchOn_C:currPage+1
                                                                :categoryC
                                                                :indexC];//loop on page
                                            }
                                            else if ([responseData count]==0)
                                            {
                                                NSLog(@"cate%@ get finish",categoryC);
                                                
                                                [self loop_oneTag_C:1
                                                                 :200
                                                                 :indexC+1];//  +1
                                            }
                                        }];
    
    
}



//===唐朝====》 根据作者 捕获诗篇
-(void)loop_oneTag_tangPoetName:(NSInteger)currPage
                           :(NSInteger)indexPoet
{
    if (self.mArr_allPoet_tang==nil)
    self.mArr_allPoet_tang = [self getAll_TangPoetName];
    
    if (indexPoet<[_mArr_allPoet_tang count]) {
        
        [self runCatchOn_tangPoetName:1
                                 :[_mArr_allPoet_tang objectAtIndex:indexPoet]
                                 :indexPoet];
    }
    else
    {
        NSLog(@"finish  all_PoetName");
        
//        //go to song
//        [self loop_oneTag_songPoetName:1
//                                      :0];
    }
    
}

-(void)runCatchOn_tangPoetName:(NSInteger)currPage
                   :(NSString *)poetNamePara
                   :(NSInteger)indexPoet
{
    
    NSString *uriStr = [NSString stringWithFormat:@"/search.aspx?type=author&page=%ld&value=%@",currPage,poetNamePara];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [HttpVerClient GetHTML_ShiWen_simple:encodedValue
                                        :^(BOOL bol, id responseData) {
                                            
                                            if (![responseData isKindOfClass:[NSArray class]]) {
                                                sleep(8);
                                                
                                                //网关超时  重新来
                                                [self runCatchOn_tangPoetName:currPage
                                                                             :poetNamePara
                                                                             :indexPoet];
                                            }
                                            
                                            NSMutableArray *mArr_Sons = [NSMutableArray arrayWithArray:responseData];
                                            
                                            if ([mArr_Sons count]==11) {
                                                
                                                TFHppleElement *element_a = [mArr_Sons objectAtIndex:0];
                                                
                                                //===poetNum 111111111
                                                NSArray *a1Arr = [element_a searchWithXPathQuery:@"//a[@style=' font-size:14px;']"];
                                                
                                                TFHppleElement *ele_numPoet = [a1Arr firstObject];
                                                NSString *rawNumPoet = [[ele_numPoet attributes] objectForKey:@"href"];
                                                _poetNum_first = [[rawNumPoet get_num_fromTxt] integerValue];
                                                
                                                //===poetName  22222222222
                                                NSArray *a2Arr = [element_a searchWithXPathQuery:@"//span[@style='color:#B00815']"];
                                                
                                                TFHppleElement *ele_namePoet = [a2Arr firstObject];
                                                _poetName_first = ele_namePoet.text;
                                                
                                                if (![_poetName_first isEqualToString:poetNamePara]) {
                                                    NSLog(@"诗人名字重叠，少见了");
                                                }
                                                
                                                //====poetUrl  33333333
                                                NSArray *a3Arr = [element_a searchWithXPathQuery:@"//img"];
                                                
                                                TFHppleElement *ele_urlPoet = [a3Arr firstObject];
                                                _poetHeadImgUrl_first = [[ele_urlPoet attributes] objectForKey:@"src"];
                                                
                                                
                                                //===444444
                                                [mArr_Sons removeObjectAtIndex:0];//移除---作者一行
                                            }
                                            
                                            for (TFHppleElement *element in mArr_Sons) {
                                                
                                                //区域share Var
                                                NSString *shiwenTitle;
                                                NSInteger shiwenNum;
                                                NSString *shiwenSimple;
                                                NSString *poetName;
                                                
                                                
                                                //1111===诗文标题  诗文num
                                                NSArray *a1Arr = [element searchWithXPathQuery:@"//a[@style=' font-size:14px;']"];
                                                
                                                if ([a1Arr count]==1) {
                                                    TFHppleElement *obj = [a1Arr firstObject];
                                                    
                                                    shiwenTitle = [obj.text clear_stringWrong];
                                                    
                                                    NSString *rawNum = [[obj attributes] objectForKey:@"href"];
                                                    shiwenNum =  [[rawNum get_num_fromTxt] integerValue];
                                                    
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                //2222====诗文摘要
                                                NSArray *p1Arr = [element searchWithXPathQuery:@"//p[@style='margin-bottom:0px;']"];
                                                
                                                if ([p1Arr count]==1) {
                                                    TFHppleElement *obj = [p1Arr firstObject];
                                                    
                                                    shiwenSimple = [obj.text clear_stringWrong];
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                //3333=== 诗人名字
                                                NSArray *p2Arr = [element searchWithXPathQuery:@"//span[@style='color:#B00815']"];
                                                
                                                if ([p2Arr count]==1) {
                                                    TFHppleElement *obj = [p2Arr firstObject];
                                                    
                                                    poetName = [obj.text clear_stringWrong];
                                                    
                                                    if (![poetNamePara isEqualToString:poetName]) {
                                                        NSLog(@"不可能作者不一致！！");
                                                    }
                                                }
                                                else
                                                {
                                                    TFHppleElement *obj = [p2Arr firstObject];
                                                    
                                                    poetName = [obj.text clear_stringWrong];
                                                    
                                                    if (![poetNamePara isEqualToString:poetName]) {
                                                        NSLog(@"不可能作者不一致！！");
                                                    }
                                                    
                                                    NSLog(@"本应该只有一行标题");
                                                }
                                                
                                                
                                                //===key 检索数据库中是否已经存入这篇  古诗文num
                                                
                                                NSString *whereStr = [NSString stringWithFormat:@"shiwenNum = %ld",shiwenNum];
                                                
                                                NSMutableArray *mArr = [shiciObj searchWithWhere:whereStr
                                                                                         orderBy:nil
                                                                                          offset:0
                                                                                           count:5] ;//@"rowid = 1"
                                                
                                                if ([mArr count]==0) {
                                                    //未存在  生成一条新数据
                                                    shiciObj *shiciO = ({
                                                        shiciObj *shiciTemp = [shiciObj new];
                                                        
                                                        shiciTemp.shiwenNum = shiwenNum;
                                                        shiciTemp.poetryTitle = shiwenTitle;
                                                        shiciTemp.poetrySnippet = shiwenSimple;
                                                        shiciTemp.poetName = poetName;
                                                        
                                                        if (_poetNum_first!=-1)
                                                            shiciTemp.poetNum =  _poetNum_first;
                                                        
                                                        if ([_poetHeadImgUrl_first length]>0)
                                                            shiciTemp.poetHeadUrl = _poetHeadImgUrl_first;
                                                        
                                                       shiciTemp.str_ChaoDai = @"唐朝";
                                                        
                                                        
                                                        shiciTemp;
                                                    });
                                                    
                                                    //首次  存入=====
                                                    
                                                    if (![shiciO saveToDB]) {
                                                        NSLog(@"新增失败");
                                                    }
                                                    else{
                                                        NSLog(@"新增：%@  %@",shiciO.poetryTitle,shiciO.poetName);
                                                    }
                                                }
                                                else if ([mArr count]==1)
                                                {//已经存在一条。 正常情况。
                                                    shiciObj *shiciO = (shiciObj *)[mArr objectAtIndex:0];
                                                    
                                                    if (_poetNum_first>0
                                                        &&(shiciO.poetNum==-1
                                                           ||!shiciO.poetNum)) {
                                                        
                                                        shiciO.poetNum = _poetNum_first;
                                                        
                                                        //更新条目====
                                                        if (![shiciO updateToDB]) {
                                                            NSLog(@"为何更新失败！！");
                                                        }
                                                        else
                                                        {
                                                            NSLog(@"更新：%@  poetNum:%ld ",shiciO.poetName,shiciO.poetNum);
                                                        }
                                                        
                                                    }
                                                    
                                                    //更新头像
                                                    if ([_poetHeadImgUrl_first length]>0
                                                        &&shiciO.poetHeadUrl==nil){
                                                        shiciO.poetHeadUrl = _poetHeadImgUrl_first;
                                                        [shiciO updateToDB];
                                                        NSLog(@"更新:%@ PoetHeadUrl：%@",shiciO.poetName,shiciO.poetHeadUrl);
                                                    }
                                                    
                                                }
                                                else if ([mArr count]>1)
                                                {// 不应该出现  两条一样的数据！！
                                                    NSLog(@" 不应该出现  两条一样的数据！！");
                                                }
                                                
                                            }
                                            
                                            if ([responseData count]>0) {
                                                [self runCatchOn_tangPoetName:currPage+1
                                                                  :poetNamePara
                                                                  :indexPoet];//loop on page
                                            }
                                            else if ([responseData count]==0)
                                            {
                                                NSLog(@"poet %@ get finish",poetNamePara);
                                                
                                                [self loop_oneTag_tangPoetName:1
                                                                   :indexPoet+1];//  +1
                                            }
                                        }];
    
    
}

#pragma mark helper method
//针对唐宋两朝作者===单个作者捕获===》
-(NSMutableArray *)getAll_TangPoetName
{
    NSString *whereStr_tang = [NSString stringWithFormat:@"str_ChaoDai = %@",@"\'唐代\'"];
    
 
    
    NSMutableArray *mArr_tang = [shiciObj searchWithWhere:whereStr_tang
                                             orderBy:nil
                                              offset:0
                                               count:5000];
    

    NSLog(@"唐代shiwen数目 %lu",(unsigned long)[mArr_tang count] );
    
    NSMutableArray *mArr_tangPoet =[NSMutableArray new];
    
    for (shiciObj *obj_tang in mArr_tang) {
        NSString *str1  = [obj_tang.poetName clear_stringWrong];
        [mArr_tangPoet addObject:[str1 clearNum]];
        
    }
    
    
    
    NSMutableArray *tangPoetArr = [self oneIt_mArr:mArr_tangPoet];
    
    return tangPoetArr;
}

-(NSMutableArray *)getAll_SongPoetName
{
    NSString *whereStr_song = [NSString stringWithFormat:@"str_ChaoDai = %@",@"\'宋代\'"];
    
    
    NSMutableArray *mArr_song = [shiciObj searchWithWhere:whereStr_song
                                                  orderBy:nil
                                                   offset:0
                                                    count:5000];
    
    NSLog(@"宋代shiwen数目 %lu",(unsigned long)[mArr_song count]);
    
    
    NSMutableArray *mArr_songPoet =[NSMutableArray new];
    
    for (shiciObj *obj_song in mArr_song) {
        NSString *str1  = [obj_song.poetName clear_stringWrong];
        [mArr_songPoet addObject:[str1 clearNum]];
        
    }
    
    NSMutableArray *songPoetArr = [self oneIt_mArr:mArr_songPoet];
    
    
    return songPoetArr;
}

//过滤重复性，保持arr容器内部数值唯一。
-(NSMutableArray *)oneIt_mArr:(NSMutableArray *)mArr
{
    for (int i=0; i<[mArr count]; i++) {
        
        NSString *strPoet = [mArr objectAtIndex:i];
        
        for (int j = i+1; j<[mArr count]; j++) {
            
            NSString *strPoet_x = [mArr objectAtIndex:j];
            
            if ([strPoet_x isEqualToString:@"0"]
                ||[strPoet_x isEqualToString:@"-1"]) {
                [mArr removeObjectAtIndex:j];
                continue;
            }
            
            if ([strPoet isEqualToString:strPoet_x]) {
                [mArr removeObjectAtIndex:j];
            }
        }
    }
    
    return mArr;
}

//====宋朝---单个作者  捕获作品
-(void)loop_oneTag_songPoetName:(NSInteger)currPage
                               :(NSInteger)indexPoet
{
    if (self.mArr_allPoet_song==nil)
        self.mArr_allPoet_song = [self getAll_SongPoetName];
    
    if (indexPoet<[_mArr_allPoet_song count]) {
        
        [self runCatchOn_songPoetName:1
                                     :[_mArr_allPoet_song objectAtIndex:indexPoet]
                                     :indexPoet];
    }
    else
    {
        NSLog(@"finish  all_PoetName");
    }
    
}

-(void)runCatchOn_songPoetName:(NSInteger)currPage
                              :(NSString *)poetNamePara
                              :(NSInteger)indexPoet
{
    
    NSString *uriStr = [NSString stringWithFormat:@"/search.aspx?type=author&page=%ld&value=%@",currPage,poetNamePara];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [HttpVerClient GetHTML_ShiWen_simple:encodedValue
                                        :^(BOOL bol, id responseData) {
                                            
                                            if (![responseData isKindOfClass:[NSArray class]]) {
                                                sleep(8);
                                                
                                                //网关超时  重新来
                                                [self runCatchOn_songPoetName:currPage
                                                                             :poetNamePara
                                                                             :indexPoet];
                                            }
                                            
                                            NSMutableArray *mArr_Sons = [NSMutableArray arrayWithArray:responseData];
                                            
                                            if ([mArr_Sons count]==11) {
                                                
                                                TFHppleElement *element_a = [mArr_Sons objectAtIndex:0];
                                                
                                                //===poetNum 111111111
                                                NSArray *a1Arr = [element_a searchWithXPathQuery:@"//a[@style=' font-size:14px;']"];
                                                
                                                TFHppleElement *ele_numPoet = [a1Arr firstObject];
                                                NSString *rawNumPoet = [[ele_numPoet attributes] objectForKey:@"href"];
                                                _poetNum_first_song = [[rawNumPoet get_num_fromTxt] integerValue];
                                                
                                                //===poetName  22222222222
                                                NSArray *a2Arr = [element_a searchWithXPathQuery:@"//span[@style='color:#B00815']"];
                                                
                                                TFHppleElement *ele_namePoet = [a2Arr firstObject];
                                                _poetName_first_song = [[ele_namePoet.text clear_stringWrong] clearNum];
                                                
                                                if (![_poetName_first_song isEqualToString:poetNamePara]) {
                                                    NSLog(@"诗人名字重叠，少见了");
                                                }
                                                
                                                //====poetUrl  33333333
                                                NSArray *a3Arr = [element_a searchWithXPathQuery:@"//img"];
                                                
                                                TFHppleElement *ele_urlPoet = [a3Arr firstObject];
                                                _poetHeadImgUrl_first_song = [[ele_urlPoet attributes] objectForKey:@"src"];
                                                
                                                
                                                //===444444
                                                [mArr_Sons removeObjectAtIndex:0];//移除---作者一行
                                            }
                                            
                                            for (TFHppleElement *element in mArr_Sons) {
                                                
                                                //区域share Var
                                                NSString *shiwenTitle;
                                                NSInteger shiwenNum;
                                                NSString *shiwenSimple;
                                                NSString *poetName;
                                                
                                                
                                                //1111===诗文标题  诗文num
                                                NSArray *a1Arr = [element searchWithXPathQuery:@"//a[@style=' font-size:14px;']"];
                                                
                                                if ([a1Arr count]==1) {
                                                    TFHppleElement *obj = [a1Arr firstObject];
                                                    
                                                    shiwenTitle = [obj.text clear_stringWrong];
                                                    
                                                    NSString *rawNum = [[obj attributes] objectForKey:@"href"];
                                                    shiwenNum =  [[rawNum get_num_fromTxt] integerValue];
                                                    
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                //2222====诗文摘要
                                                NSArray *p1Arr = [element searchWithXPathQuery:@"//p[@style='margin-bottom:0px;']"];
                                                
                                                if ([p1Arr count]==1) {
                                                    TFHppleElement *obj = [p1Arr firstObject];
                                                    
                                                    shiwenSimple = [obj.text clear_stringWrong];
                                                }
                                                else
                                                {
                                                    NSLog(@"只应该有一行标题");
                                                }
                                                
                                                //3333=== 诗人名字
                                                NSArray *p2Arr = [element searchWithXPathQuery:@"//span[@style='color:#B00815']"];
                                                
                                                if ([p2Arr count]==1) {
                                                    TFHppleElement *obj = [p2Arr firstObject];
                                                    
                                                    poetName = [obj.text clear_stringWrong];
                                                    
                                                    if (![poetNamePara isEqualToString:poetName]) {
                                                        NSLog(@"不可能作者不一致！！");
                                                    }
                                                }
                                                else
                                                {
                                                    TFHppleElement *obj = [p2Arr firstObject];
                                                    
                                                    poetName = [obj.text clear_stringWrong];
                                                    
                                                    if (![poetNamePara isEqualToString:poetName]) {
                                                        NSLog(@"不可能作者不一致！！");
                                                    }
                                                    
                                                    NSLog(@"本应该只有一行标题");
                                                }
                                                
                                                
                                                //===key 检索数据库中是否已经存入这篇  古诗文num
                                                
                                                NSString *whereStr = [NSString stringWithFormat:@"shiwenNum = %ld",shiwenNum];
                                                
                                                NSMutableArray *mArr = [shiciObj searchWithWhere:whereStr
                                                                                         orderBy:nil
                                                                                          offset:0
                                                                                           count:5] ;//@"rowid = 1"
                                                
                                                if ([mArr count]==0) {
                                                    //未存在  生成一条新数据
                                                    shiciObj *shiciO = ({
                                                        shiciObj *shiciTemp = [shiciObj new];
                                                        
                                                        shiciTemp.shiwenNum = shiwenNum;
                                                        shiciTemp.poetryTitle = shiwenTitle;
                                                        shiciTemp.poetrySnippet = shiwenSimple;
                                                        shiciTemp.poetName = poetName;
                                                        
                                                        if (_poetNum_first!=-1)
                                                            shiciTemp.poetNum =  _poetNum_first;
                                                        
                                                        if ([_poetHeadImgUrl_first length]>0)
                                                            shiciTemp.poetHeadUrl = _poetHeadImgUrl_first;
                                                        
                                                        shiciTemp.str_ChaoDai = @"宋朝";
                                                        
                                                        
                                                        shiciTemp;
                                                    });
                                                    
                                                    //首次  存入=====
                                                    
                                                    if (![shiciO saveToDB]) {
                                                        NSLog(@"新增失败");
                                                    }
                                                    else{
                                                        NSLog(@"新增：%@  %@",shiciO.poetryTitle,shiciO.poetName);
                                                    }
                                                }
                                                else if ([mArr count]==1)
                                                {//已经存在一条。 正常情况。
                                                    shiciObj *shiciO = (shiciObj *)[mArr objectAtIndex:0];
                                                    
                                                    if (_poetNum_first>0
                                                        &&(shiciO.poetNum==-1
                                                           ||!shiciO.poetNum)) {
                                                            
                                                            shiciO.poetNum = _poetNum_first;
                                                            
                                                            //更新条目====
                                                            if (![shiciO updateToDB]) {
                                                                NSLog(@"为何更新失败！！");
                                                            }
                                                            else
                                                            {
                                                                NSLog(@"更新：%@  poetNum:%ld ",shiciO.poetName,shiciO.poetNum);
                                                            }
                                                            
                                                        }
                                                    
                                                    //更新头像
                                                    if ([_poetHeadImgUrl_first length]>0
                                                        &&shiciO.poetHeadUrl==nil){
                                                        shiciO.poetHeadUrl = _poetHeadImgUrl_first;
                                                        [shiciO updateToDB];
                                                        NSLog(@"更新:%@ PoetHeadUrl：%@",shiciO.poetName,shiciO.poetHeadUrl);
                                                    }
                                                    
                                                }
                                                else if ([mArr count]>1)
                                                {// 不应该出现  两条一样的数据！！
                                                    NSLog(@" 不应该出现  两条一样的数据！！");
                                                }
                                                
                                            }
                                            
                                            if ([responseData count]>0) {
                                                [self runCatchOn_songPoetName:currPage+1
                                                                             :poetNamePara
                                                                             :indexPoet];//loop on page
                                            }
                                            else if ([responseData count]==0)
                                            {
                                                NSLog(@"poet %@ get finish",poetNamePara);
                                                
                                                [self loop_oneTag_songPoetName:1
                                                                              :indexPoet+1];//  +1
                                            }
                                        }];
    
    
}


//====轮训  shiwenNum   1---->72738  （目标总计是：共72109篇）
-(void)loop_shiwenNum:(NSInteger)indexGo
{
    if (_Arr_notExistNum==nil) {
        _Arr_notExistNum  = [self get_existShiwenNum_arr];
    }

    if (indexGo<[_Arr_notExistNum count]) {
        [self runCatchOn_poetryNum:[[_Arr_notExistNum objectAtIndex:indexGo] integerValue]
                                  :indexGo];
    }
    else
    {
        NSLog(@"finish for numShiwen");
    }
    
    
}

-(void)runCatchOn_poetryNum:(NSInteger)shiwenNum
                           :(NSInteger)indexGo
{
    
    NSString *uriStr = [NSString stringWithFormat:@"/view_%ld.aspx",shiwenNum];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [HttpVerClient GetHTML_ShiWen_shiwenView:encodedValue
                                            :^(id tagWrong, id tagSon1, id tagSon2) {
                                            
                                                
                                                
                                            if (tagWrong==nil&&
                                                tagSon1==nil&&
                                                tagSon2==nil) {
                                                sleep(8);
                                                
                                                //网关超时  重新来
                                                [self runCatchOn_poetryNum:shiwenNum
                                                                          :indexGo];
                                                return ;
                                            }
                                            
                                            //====不存在 viewShiwen
                                                if (tagWrong !=nil) {
                                                    NSLog(@"没有shiwenNum：%ld",shiwenNum);
                                                    [self loop_shiwenNum:indexGo+1];
                                                    return ;
                                                    
                                                }

                                            //====存在  viewShiwen
                                                if (tagSon1!=nil&&tagSon2!=nil){
                                                    NSLog(@"当前shiwenNum：%ld",shiwenNum);
                                                    
                                                    //区域share Var
                                                    NSString *shiwenTitle;//诗文 name
                                                    
                                                    NSString *str_chaodai;//朝代
                                                    NSString *poetName;//作者
                                                    NSInteger poetNum = 0;//作者序号
                                                    NSMutableString *shiwenText_m = [NSMutableString new];// gushiwen 原文
                                                    
                                                    TFHppleElement *ele_H1 = (TFHppleElement *)[tagSon1 firstObject];
                                                    TFHppleElement *ele_son2 = (TFHppleElement *)tagSon2;
                                                    
                                                    //1111
                                                    shiwenTitle = ele_H1.text;
                                                    
                                                    //2222
                                                    NSArray *p_chaodai_zuozhe = [ele_son2 searchWithXPathQuery:@"//p[@style='margin:0px; font-size:12px;line-height:160%;']"];
                                                    
                                                    TFHppleElement *pChaodai = [p_chaodai_zuozhe objectAtIndex:0];
                                                    TFHppleElement *pZuozhe = [p_chaodai_zuozhe objectAtIndex:1];
                                                    
                                                    str_chaodai = [pChaodai.text clear_stringWrong];//没有a tag 可能
                                                    
                                                    NSArray *a_poetNum = [pZuozhe searchWithXPathQuery:@"//a"];
                                                    if ([a_poetNum count]==1) {
                                                        
                                                        TFHppleElement *elePoetNum = [a_poetNum firstObject];
                                                        
                                                        poetName = [elePoetNum.text clear_stringWrong];
                                                        
                                                        poetNum = [[[[elePoetNum attributes] objectForKey:@"href"] get_num_fromTxt] integerValue];
                                                    }
                                                    else if(pZuozhe.text != nil)
                                                    {
                                                        poetName = [pZuozhe.text clear_stringWrong];
                                                    }
                                                    else
                                                    {
                                                        NSLog(@"不可能情况，作者name 格式");
                                                    }
                                                    
                                                    //33333 ‘原文’ 下面的所有pTag 都是 shiwenText 判断
                                                    /**/
                                                    
                                                     NSArray *p_All_Tag = [ele_son2 searchWithXPathQuery:@"//p"];
                                                     
                                                     NSMutableArray *p_All_Tag_mutable = [NSMutableArray arrayWithArray:p_All_Tag];
                                                     
                                                     
                                                     BOOL has_pTag_content = NO;
                                                     if ([p_All_Tag_mutable count]>3) {
                                                     
                                                         [p_All_Tag_mutable removeObjectAtIndex:0];
                                                         [p_All_Tag_mutable removeObjectAtIndex:0];
                                                         [p_All_Tag_mutable removeObjectAtIndex:0];//去掉前三个 obj
                                                         
                                                         has_pTag_content = YES;
                                                     }
                                                    
                                                    
                                                    //content 在div son2
                                                    if (has_pTag_content==NO)//没有p content
                                                    {
                                                        NSArray *childArr = [ele_son2 childrenWithTagName:@"text"];
//                                                        NSLog(@"%@",childArr);
                                                        
                                                        for (TFHppleElement *eleX in childArr) {
                                                            
                                                            NSString *clearSpace = [eleX.content stringByReplacingOccurrencesOfString:@" "
                                                                                                                           withString:@""];
                                                            
                                                            if ([clearSpace length]>2) {
                                                                
                                                                [shiwenText_m appendString:clearSpace];
                                                            }
                                                        }
                                                        
                                                    }
                                                    
                                                    //存在 p_content
                                                    if (has_pTag_content
                                                        && [p_All_Tag_mutable count]>0)
                                                    {
                                                        //内容在p content中
                                                        for (TFHppleElement *eleP_content in p_All_Tag_mutable) {
                                                            
                                                            NSString * section_text = [eleP_content.text stringByReplacingOccurrencesOfString:@" "
                                                                                                                                   withString:@""];
                                                            
                                                            [shiwenText_m appendString:section_text];
                                                            
                                                        }
                                                    }
                                                    
                                                    
                                                     
                                                     
                                                    
                                                    NSLog(@"mainText:%@",shiwenText_m);
                                                    
                                                    
                                                    
                                                    
                                                    //====44444444444存储or更新
                                                    //===key 检索数据库中是否已经存入这篇  古诗文num
                                                    
                                                    NSString *whereStr = [NSString stringWithFormat:@"shiwenNum = %ld",shiwenNum];
                                                    
                                                    NSMutableArray *mArr = [shiciObj searchWithWhere:whereStr
                                                                                             orderBy:nil
                                                                                              offset:0
                                                                                               count:5] ;//@"rowid = 1"
                                                    
                                                    if ([mArr count]==0) {
                                                        //未存在  生成一条新数据
                                                        shiciObj *shiciO = ({
                                                            shiciObj *shiciTemp = [shiciObj new];
                                                            
                                                            shiciTemp.shiwenNum = shiwenNum;
                                                            shiciTemp.poetryTitle = shiwenTitle;
//                                                            shiciTemp.poetryText = shiwenText_m;//mainText
                                                            shiciTemp.poetName = poetName;
                                                            shiciTemp.poetNum = poetNum;
                                                            shiciTemp.str_ChaoDai = str_chaodai;
                                                            
                                                            shiciTemp;
                                                        });
                                                        
                                                        //首次  存入=====
                                                        
                                                        if (![shiciO saveToDB]) {
                                                            NSLog(@"新增失败");
                                                        }
                                                        else{
                                                            NSLog(@"新增：%@  %@",shiciO.poetryTitle,shiciO.poetName);
                                                        }
                                                    }
                                                    else if ([mArr count]==1)
                                                    {//已经存在一条。 正常情况。
                                                        shiciObj *shiciO = (shiciObj *)[mArr objectAtIndex:0];
                                                        
                                                        if (poetNum>0
                                                            &&(shiciO.poetNum==-1
                                                               ||!shiciO.poetNum)) {
                                                                
                                                                shiciO.poetNum = poetNum;
                                                                
                                                                //更新条目====
                                                                if (![shiciO updateToDB]) {
                                                                    NSLog(@"为何更新失败！！");
                                                                }
                                                                else
                                                                {
                                                                    NSLog(@"更新：%@  poetNum:%ld ",shiciO.poetName,shiciO.poetNum);
                                                                }
                                                                
                                                            }
                                                        
                                                        
                                                        
                                                    }
                                                    else if ([mArr count]>1)
                                                    {// 不应该出现  两条一样的数据！！
                                                        NSLog(@" 不应该出现  两条一样的数据！！");
                                                    }
                                                    
                                                    
                                                    
                                                    [self loop_shiwenNum:indexGo+1];
                                                }
                                                else
                                                {
                                                    NSLog(@"不可能情况，view 格式应该保持");
                                                    
                                                    [self loop_shiwenNum:indexGo+1];
                                                }
                                                
                                        }];
    
    
}

-(NSArray *)get_existShiwenNum_arr
{

    NSString *where_shiwenNum = [NSString stringWithFormat:@"shiwenNum != 0"];
    
    
    NSMutableArray *mArr_exist = [shiciObj searchColumn:@"shiwenNum"
                                            where:where_shiwenNum
                                          orderBy:@"shiwenNum"
                                           offset:0
                                            count:80000];
    
//    NSLog(@"shiwen数目 %lu",(unsigned long)[mArr_exist count]);
    
    
    NSMutableArray *mArr_notExistNum =[NSMutableArray new];
    

    NSInteger j_start = 0 ;
    for (NSInteger i = 1; i< 72738; i++) {
        
        NSString *runNumVerb = [NSString stringWithFormat:@"%ld",i];
        
        for (NSInteger j = j_start; j < 56333; j++) {
            
            NSString *shiwenNumStr = [mArr_exist objectAtIndex:j];
            
            
            
            if ([shiwenNumStr integerValue]>i) {
//                NSLog(@"未存在 %ld",i);
                
                [mArr_notExistNum addObject:@(i)];
                
                //j  重新 站在 新起点
                
                j_start = j-2;
                
                break;
                
            }
            else if ([shiwenNumStr isEqualToString:runNumVerb])
            {
                
                break;
            }
            
        }
    }
    
//    NSLog(@"%@",mArr_notExistNum);
    
    return mArr_notExistNum;
    
}

//***************
//zzzzzzzz111111111
//====轮训  shiwenNum   1---->72738  （目标总计是：共72109篇）
//http://so.gushiwen.org/view_72742.aspx 还有存在的诗篇。！！
//***************
-(void)loop_shiwenNum_getMainText:(NSInteger)indexGo
{
    if (_Arr_ExistNum==nil) {
        _Arr_ExistNum  = [self get_allShiwenNum_arr];
    }
    
    if (indexGo<[_Arr_ExistNum count]) {
        [self runCatchOn_poetryNum_getMainText:[[_Arr_ExistNum objectAtIndex:indexGo] integerValue]
                                              :indexGo];
    }
    else
    {
        NSLog(@"finish for getMainText");
    }
    
    
}

-(void)runCatchOn_poetryNum_getMainText:(NSInteger)shiwenNum
                                       :(NSInteger)indexGo
{
    
    NSString *uriStr = [NSString stringWithFormat:@"/view_%ld.aspx",shiwenNum];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [HttpVerClient GetHTML_ShiWen_shiwenView:encodedValue
                                            :^(id tagWrong, id tagSon1, id tagSon2) {
                                                
                                                
                                                
                                                if (tagWrong==nil&&
                                                    tagSon1==nil&&
                                                    tagSon2==nil) {
                                                    sleep(8);
                                                    
                                                    //网关超时  重新来
                                                    [self runCatchOn_poetryNum_getMainText:shiwenNum
                                                                                          :indexGo];
                                                    return ;
                                                }
                                                
                                                //====不存在 viewShiwen
                                                if (tagWrong !=nil) {
                                                    NSLog(@"不应该==没有shiwenNum：%ld",shiwenNum);
                                                    [self loop_shiwenNum_getMainText:indexGo+1];
                                                    return ;
                                                    
                                                }
                                                
                                                //====存在  viewShiwen
                                                if (tagSon1!=nil&&tagSon2!=nil){
                                                    
                                                    //区域share Var
                                                    NSString *shiwenTitle;//诗文 name
                                                    
                                                    NSString *str_chaodai;//朝代
                                                    NSString *poetName;//作者
                                                    NSInteger poetNum = 0;//作者序号
                                                    NSMutableString *shiwenText_m = [NSMutableString new];// gushiwen 原文
                                                    
                                                    TFHppleElement *ele_H1 = (TFHppleElement *)[tagSon1 firstObject];
                                                    TFHppleElement *ele_son2 = (TFHppleElement *)tagSon2;
                                                    
                                                    //1111
                                                    shiwenTitle = ele_H1.text;
                                                    
                                                    //2222
                                                    NSArray *p_chaodai_zuozhe = [ele_son2 searchWithXPathQuery:@"//p[@style='margin:0px; font-size:12px;line-height:160%;']"];
                                                    
                                                    TFHppleElement *pChaodai = [p_chaodai_zuozhe objectAtIndex:0];
                                                    TFHppleElement *pZuozhe = [p_chaodai_zuozhe objectAtIndex:1];
                                                    
                                                    str_chaodai = [pChaodai.text clear_stringWrong];//没有a tag 可能
                                                    
                                                    NSArray *a_poetNum = [pZuozhe searchWithXPathQuery:@"//a"];
                                                    if ([a_poetNum count]==1) {
                                                        
                                                        TFHppleElement *elePoetNum = [a_poetNum firstObject];
                                                        
                                                        poetName = [elePoetNum.text clear_stringWrong];
                                                        
                                                        poetNum = [[[[elePoetNum attributes] objectForKey:@"href"] get_num_fromTxt] integerValue];
                                                    }
                                                    else if(pZuozhe.text != nil)
                                                    {
                                                        poetName = [pZuozhe.text clear_stringWrong];
                                                    }
                                                    else
                                                    {
                                                        NSLog(@"不可能情况，作者name 格式");
                                                    }
                                                    
                                                    //33333 ‘原文’ 下面的所有pTag 都是 shiwenText 判断
                                                    /**/
                                                    
                                                    NSArray *p_All_Tag = [ele_son2 searchWithXPathQuery:@"//p"];
                                                    
                                                    NSMutableArray *p_All_Tag_mutable = [NSMutableArray arrayWithArray:p_All_Tag];
                                                    
                                                    
                                                    BOOL has_pTag_content = NO;
                                                    if ([p_All_Tag_mutable count]>3) {
                                                        
                                                        [p_All_Tag_mutable removeObjectAtIndex:0];
                                                        [p_All_Tag_mutable removeObjectAtIndex:0];
                                                        [p_All_Tag_mutable removeObjectAtIndex:0];//去掉前三个 obj
                                                        
                                                        has_pTag_content = YES;
                                                    }
                                                    
                                                    
                                                    //content 在div son2!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                                    if (has_pTag_content==NO)//没有p content
                                                    {
                                                        NSArray *childArr = [ele_son2 childrenWithTagName:@"text"];
                                                        
                                                        for (TFHppleElement *eleX in childArr) {
                                                            
                                                            
                                                            
                                                            NSString *clearSpace = [eleX.content stringByReplacingOccurrencesOfString:@" "
                                                                                                                           withString:@""];
                                                            
                                                            if ([clearSpace length]>2) {
                                                                
                                                                [shiwenText_m appendString:clearSpace];
                                                            }
                                                        }
                                                        
                                                    }
                                                    
                                                    //存在 p_content!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                                    if (has_pTag_content
                                                        && [p_All_Tag_mutable count]>0)
                                                    {
                                                        //内容在p content中
                                                        for (TFHppleElement *eleP_content in p_All_Tag_mutable) {
                                                            
                                                            
                                                            NSArray *childArr_p = [eleP_content childrenWithTagName:@"text"];
                                                            
                                                            for (TFHppleElement *eleX in childArr_p) {
                                                                
                                                                //==针对 <strong>标签！！！！！！！！！！！！
                                                                NSString *strong_str ;
                                                                if ([[eleP_content searchWithXPathQuery:@"//strong"] count]==1) {
                                                                    
                                                                    TFHppleElement *ele_strong = [[eleP_content searchWithXPathQuery:@"//strong"] firstObject];
                                                                    
                                                                    strong_str = [NSString stringWithFormat:@"**%@**",ele_strong.content];
                                                                    
                                                                    [shiwenText_m appendString:@"\n"];
                                                                    [shiwenText_m appendString:strong_str];
                                                                }
                                                                
                                                                
                                                                NSString *clearSpace = [eleX.content stringByReplacingOccurrencesOfString:@" "
                                                                                                                               withString:@""];
                                                                
                                                                if ([clearSpace length]>2) {
                                                                    
                                                                    [shiwenText_m appendString:@"\n"];
                                                                    [shiwenText_m appendString:clearSpace];
                                                                }
                                                            }
                                                            
                                                            
                                                        }
                                                    }
                                                    
                                                    
                                                    
                                                    
                                                    
//                                                    NSLog(@"mainText:%@",shiwenText_m);
                                                    
                                                    
                                                    
                                                    
                                                    //====44444444444存储or更新
                                                    //===key 检索数据库中是否已经存入这篇  古诗文num
                                                    
                                                    NSString *whereStr = [NSString stringWithFormat:@"shiwenNum = %ld",shiwenNum];
                                                    
                                                    NSMutableArray *mArr = [shiciObj searchWithWhere:whereStr
                                                                                             orderBy:nil
                                                                                              offset:0
                                                                                               count:5] ;//@"rowid = 1"
                                                    
                                                    if ([mArr count]==0) {
                                                        NSLog(@"都应该已经存在了！！不该进来");
                                                        //未存在  生成一条新数据
                                                        /*
                                                        shiciObj *shiciO = ({
                                                            shiciObj *shiciTemp = [shiciObj new];
                                                            
                                                            shiciTemp.shiwenNum = shiwenNum;
                                                            shiciTemp.poetryTitle = shiwenTitle;
                                                            //                                                            shiciTemp.poetryText = shiwenText_m;//mainText
                                                            shiciTemp.poetName = poetName;
                                                            shiciTemp.poetNum = poetNum;
                                                            shiciTemp.str_ChaoDai = str_chaodai;
                                                            
                                                            shiciTemp;
                                                        });
                                                        
                                                        //首次  存入=====
                                                        
                                                        if (![shiciO saveToDB]) {
                                                            NSLog(@"新增失败");
                                                        }
                                                        else{
                                                            NSLog(@"新增：%@  %@",shiciO.poetryTitle,shiciO.poetName);
                                                        }*/
                                                    }
                                                    else if ([mArr count]==1)
                                                    {//已经存在一条。 正常情况。
                                                        shiciObj *shiciO = (shiciObj *)[mArr objectAtIndex:0];
                                                        
                                                        /*
                                                        if (poetNum>0
                                                            &&(shiciO.poetNum==-1
                                                               ||!shiciO.poetNum)) {
                                                                
                                                                shiciO.poetNum = poetNum;
                                                                
                                                                //更新条目====
                                                                if (![shiciO updateToDB]) {
                                                                    NSLog(@"为何更新失败！！");
                                                                }
                                                                else
                                                                {
                                                                    NSLog(@"更新：%@  poetNum:%ld ",shiciO.poetName,shiciO.poetNum);
                                                                }
                                                                
                                                            }
                                                        */
                                                        
                                                        if (![shiciO.poetryText isEqualToString:shiwenText_m]) {
                                                                
                                                                shiciO.poetryText = shiwenText_m;
                                                                
                                                                //更新条目====
                                                                if (![shiciO updateToDB]) {
                                                                    NSLog(@"为何更新失败！！");
                                                                }
                                                                else
                                                                {
                                                                    NSLog(@"更新mainText：%ld currIndex:%ld",shiciO.shiwenNum,indexGo);
                                                                }
                                                                
                                                            }
                                                        
                                                    }
                                                    else if ([mArr count]>1)
                                                    {// 不应该出现  两条一样的数据！！
                                                        NSLog(@" 不应该出现  两条一样的数据！！");
                                                    }
                                                    
                                                    
                                                    
                                                    [self loop_shiwenNum_getMainText:indexGo+1];
                                                }
                                                else
                                                {
                                                    NSLog(@"不可能情况，view 格式应该保持");
                                                    
                                                    [self loop_shiwenNum_getMainText:indexGo+1];
                                                }
                                                
                                            }];
    
    
}

-(NSArray *)get_allShiwenNum_arr
{
    
    NSString *where_shiwenNum = [NSString stringWithFormat:@"shiwenNum != \'\'"];
    
    
    NSMutableArray *mArr_exist = [shiciObj searchColumn:@"shiwenNum"
                                                  where:where_shiwenNum
                                                orderBy:@"shiwenNum"
                                                 offset:0
                                                  count:80000];
    
    NSLog(@"shiwen总数目 %lu",(unsigned long)[mArr_exist count]);
    
   
    
    return mArr_exist;
    
}


//***************
//zzzzzzzz222222222222
//====轮训  诗文的翻译和赏析   1---->72738  （目标总计是：共72109篇）
//***************
-(void)loop_shiwenNum_getFanyiShangxi:(NSInteger)indexGo
{
    if (_Arr_ExistNum==nil) {
        _Arr_ExistNum  = [self get_allShiwenNum_arr];
    }
    
    if (indexGo<[_Arr_ExistNum count]) {
        
        [self runCatchOn_poetryNum_getFanyiShangxi:[[_Arr_ExistNum objectAtIndex:indexGo] integerValue]
                                                  :indexGo];
        
    }
    else
    {
        NSLog(@"finish for getFanyiShangxi");
    }
    
    
}

-(void)runCatchOn_poetryNum_getFanyiShangxi:(NSInteger)shiwenNum
                                           :(NSInteger)indexGo
{
    
    NSString *uriStr = [NSString stringWithFormat:@"/view_%ld.aspx",shiwenNum];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [HttpVerClient GetHTML_ShiWen_shiwenView_fanyiShangxi:encodedValue
                                                         :^(id tagWrong, id tagSon5) {
                                                
                                                
                                                
                                                if (tagWrong==nil&&
                                                    tagSon5==nil) {
                                                    sleep(8);
                                                    
                                                    //网关超时  重新来
                                                    [self runCatchOn_poetryNum_getFanyiShangxi:shiwenNum
                                                                                          :indexGo];
                                                    return ;
                                                }
                                                
                                                //====不存在 viewShiwen
                                                if (tagWrong !=nil) {
                                                    NSLog(@"不应该==没有shiwenNum：%ld",shiwenNum);
                                                    [self loop_shiwenNum_getFanyiShangxi:indexGo+1];
                                                    return ;
                                                    
                                                }
                                                
                                                //====存在  viewShiwen
                                                if (tagSon5!=nil){
                                                    
                                                    //翻译111  赏析222
                                                    NSArray *fanyi_shangxi_EleArr = tagSon5;
                                                    NSMutableArray *mArr_fanyiFor = [NSMutableArray new];//翻译
                                                    NSMutableArray *mArr_shangxiFor = [NSMutableArray new];//赏析
                                                    NSString *poetName ;
                                                    NSInteger poetNum = 0;
                                                    //===翻译&赏析  条目list（一般情况下，一个是译文，一个是多译字解析）----
                                                    
                                                    for (TFHppleElement *fanyi_shangxi_obj in fanyi_shangxi_EleArr) {
                                                        //---判断是翻译，还是赏析
                                                        
                                                        NSArray *a_tagArr = [fanyi_shangxi_obj searchWithXPathQuery:@"//a"];
                                                        TFHppleElement *a_element = [a_tagArr firstObject];
                                                        
                                                        NSString *is_f_s = [[[a_element attributes] objectForKey:@"href"] get_fanyi_or_shangxi];
                                                        
                                                        NSInteger key_num = [[[[a_element attributes] objectForKey:@"href"] get_num_fromTxt] integerValue];
                                                        
                                                        //---
                                                        NSArray *u_tagArr = [fanyi_shangxi_obj searchWithXPathQuery:@"//u"];
                                                        TFHppleElement *u_element = [u_tagArr firstObject];
                                                        
                                                        NSString *titleStr = u_element.content;
                                                        
                                                        
                                                        if ([is_f_s isEqualToString:@"fanyi"]) {
                                                            
                                                            shiciFanyiObj *fanyi_NewObj = ({
                                                                shiciFanyiObj *fanyi_temp = [shiciFanyiObj new];
                                                                
                                                                fanyi_temp.shiwenNum = shiwenNum;
                                                                fanyi_temp.shiwenFanyiNum = key_num;
                                                                fanyi_temp.poetryFanyiTitle = titleStr;
                                                                
                                                                fanyi_temp;
                                                            });
                                                            
                                                            [fanyi_NewObj saveToDB];
                                                            
                                                            NSArray *arrFanyi = @[fanyi_NewObj.poetryFanyiTitle,@(fanyi_NewObj.shiwenFanyiNum)];
                                                            [mArr_fanyiFor addObject:arrFanyi];
                                                            
                                                        }
                                                        else if ([is_f_s isEqualToString:@"shangxi"])
                                                        {
                                                            shiciShangxiObj *shangxi_NewObj = ({
                                                                shiciShangxiObj *shangxi_temp = [shiciShangxiObj new];
                                                                
                                                                shangxi_temp.shiwenNum = shiwenNum;
                                                                shangxi_temp.shiwenShangxiNum = key_num;
                                                                shangxi_temp.poetryShangxiTitle = titleStr;
                                                                
                                                                shangxi_temp;
                                                            });
                                                            
                                                            [shangxi_NewObj saveToDB];
                                                            
                                                            NSArray *arrShangxi = @[shangxi_NewObj.poetryShangxiTitle,@(shangxi_NewObj.shiwenShangxiNum)];
                                                            [mArr_shangxiFor addObject:arrShangxi];
                                                        }
                                                        else if ([is_f_s isEqualToString:@"author"]){
                                                            poetName = is_f_s;
                                                            poetNum = key_num;
                                                        }
                                                        else
                                                        {
                                                            NSLog(@"不可能还有 bfanyi  bshangxi 之外的情况吧  %@",is_f_s);
                                                        }
                                                        
                                                    }
                                                    
                                                    
                                                    
                                                    //====444444存储or更新 ===key 检索数据库中是否已经存入这篇  古诗文num
                                                    
                                                    NSString *whereStr = [NSString stringWithFormat:@"shiwenNum = %ld",shiwenNum];
                                                    
                                                    NSMutableArray *mArr = [shiciObj searchWithWhere:whereStr
                                                                                             orderBy:nil
                                                                                              offset:0
                                                                                               count:5] ;//@"rowid = 1"
                                                    
                                                    if ([mArr count]==0) {
                                                        NSLog(@"都应该已经存在了！！不该进来");
                                                        
                                                    }
                                                    else if ([mArr count]==1)
                                                    {//已经存在一条。 正常情况。
                                                        shiciObj *shiciO = (shiciObj *)[mArr objectAtIndex:0];
                                                        
                                                        if (shiciO.FanYi_List==nil) {
                                                                
                                                                shiciO.FanYi_List = mArr_fanyiFor;
                                                                
                                                                //更新条目====
                                                                if (![shiciO updateToDB]) {
                                                                    NSLog(@"为何更新失败！！");
                                                                }
                                                                else
                                                                {
                                                                    NSLog(@"更新：%@  poetNum:%ld ",shiciO.poetName,shiciO.poetNum);
                                                                }
                                                            }
                                                        
                                                        if (shiciO.ShangXi_List==nil) {
                                                            
                                                            shiciO.ShangXi_List = mArr_shangxiFor;
                                                            
                                                            //更新条目====
                                                            if (![shiciO updateToDB]) {
                                                                NSLog(@"为何更新失败！！");
                                                            }
                                                            else
                                                            {
                                                                NSLog(@"更新：%@  poetNum:%ld ",shiciO.poetName,shiciO.poetNum);
                                                            }
                                                        }
                                                        
                                                        if (shiciO.poetName==nil
                                                            ||shiciO.poetNum == 0) {
                                                            
                                                            if (poetName)
                                                            shiciO.poetName = poetName;
                                                            
                                                            if (poetNum)
                                                            shiciO.poetNum = poetNum;
                                                        }
                                                    }
                                                    else if ([mArr count]>1)
                                                    {// 不应该出现  两条一样的数据！！
                                                        NSLog(@" 不应该出现  两条一样的数据！！");
                                                    }
                                                    
                                                    [self loop_shiwenNum_getFanyiShangxi:indexGo+1];
                                                }
                                                else
                                                {
                                                    NSLog(@"不可能情况，view 格式应该保持");
                                                    
                                                    [self loop_shiwenNum_getFanyiShangxi:indexGo+1];
                                                }
                                                
                                            }];
    
    
}

//循环get 诗人Obj===>  1---->978  indexLoop

-(void)runCatchOn_poetNum:(NSInteger)poetNum
{
    if (poetNum >  978 ) {
        NSLog(@"finish");
        return;
    }
    
    NSString *uriStr = [NSString stringWithFormat:@"/author_%ld.aspx",poetNum];
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    [HttpVerClient GetHTML_poet_Author:encodedValue
                                      :^(BOOL bol, TFHppleElement * tagSon1, TFHppleElement * tagSon2) {
                                          
                                          if (!bol&&tagSon2==nil&&tagSon1==nil) {
                                              sleep(8);
                                              
                                              //网关超时  重新来
                                              [self runCatchOn_poetNum:poetNum];
                                              return ;
                                          }
                                          if (!bol&& [tagSon1 isKindOfClass:[NSString class]]) {
                                              
                                              [self runCatchOn_poetNum:poetNum+1];
                                              return ;
                                          }
                                          
                                          NSString * poetName ;
                                          NSString * poetHeadUrl;
                                          NSString * poetIntro;
                                          
                                          //===book 简介
                                          if (tagSon1) {
                                              
                                              NSArray *h1_arr = [(TFHppleElement *)tagSon1 searchWithXPathQuery:@"//h1"] ;
                                              
                                              TFHppleElement *h1_ele = [h1_arr firstObject];
                                              
                                              poetName = h1_ele.content;
                                          }
                                          
                                          if (tagSon2) {
                                              
                                              
                                              NSArray *img_arr = [(TFHppleElement *)tagSon2 searchWithXPathQuery:@"//img"] ;
                                              
                                              TFHppleElement *img_ele = [img_arr firstObject];
                                              
                                              poetHeadUrl = [[img_ele attributes] objectForKey:@"src"];
                                              
                                              //...
                                              NSArray *childTxtArr = [(TFHppleElement *)tagSon2 searchWithXPathQuery:@"//p"] ;
                                              
                                              if ([childTxtArr count]==0) {
                                                  poetIntro = [tagSon2.content clear_stringWrong_space];
                                              }
                                              else
                                              {
                                                  NSMutableArray *mArrchild = [NSMutableArray arrayWithArray:childTxtArr];
                                                  
                                                  
                                                  
                                                  NSMutableString *mStr = [NSMutableString new];
                                                  
                                                  //content 在div son2
                                                  for (TFHppleElement *eleX in mArrchild) {
                                                      
                                                      NSString *clearSpace = [eleX.content stringByReplacingOccurrencesOfString:@" "
                                                                                                                     withString:@""];
                                                      
                                                      if ([clearSpace length]>2) {
                                                          
                                                          [mStr appendString:clearSpace];
                                                      }
                                                  }
                                                  
                                                  poetIntro = mStr;// author简介
                                              }
                                              
                                              
                                              
                                          }
                                          
                                          
                                          //===key 检索数据库中是否已经存入这篇  booknum
                                          
                                          NSString *whereStr = [NSString stringWithFormat:@"authorNum = %ld",poetNum];
                                          
                                          NSMutableArray *mArr = [shiciPoetObj  searchWithWhere:whereStr
                                                                                        orderBy:nil
                                                                                         offset:0
                                                                                          count:5] ;//@"rowid = 1"
                                          
                                          if ([mArr count]==0) {
                                              
                                              shiciPoetObj *shiciPoet= ({
                                                  shiciPoetObj *tempObj = [shiciPoetObj new];
                                                  tempObj.poetNum = poetNum;
                                                  tempObj.poetName = poetName;
                                                  tempObj.poetIntro = poetIntro;
                                                  tempObj.poetHeadUrl = poetHeadUrl;
                                                  
                                                  tempObj;
                                              });
                                              
                                              [shiciPoet saveToDB];
                                          }
                                          else if ([mArr count]==1)
                                          {//已经存在一条。 正常情况。
                                              shiciPoetObj *shiciPoet = (shiciPoetObj *)[mArr firstObject];
                                              
                                              if (![shiciPoet.poetIntro isEqualToString:poetIntro]) {
                                                  shiciPoet.poetIntro = poetIntro;
                                                  
                                                  [shiciPoet saveToDB];
                                              }
                                              
                                          }
                                          else if ([mArr count]>1)
                                          {// 不应该出现  两条一样的数据！！
                                              NSLog(@" 不应该出现  两条一样的数据！！");
                                          }
                                          
                                          
                                              NSLog(@"poetNum %ld get finish",poetNum);
                                              
                                              [self runCatchOn_poetNum:poetNum+1];
                                      }];
}

#pragma mark 获取 shiwen的  翻译page

-(void)runCatchOn_shiciFanYiPage:(NSInteger)fanyiNum
{
    if (fanyiNum>26470) {
        
        NSLog(@"finish get fanyiNum");
        return;
    }
    
    NSString *uriStr = [NSString stringWithFormat:@"/fanyi_%ld.aspx",fanyiNum];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    
    [HttpVerClient GetHTML_guji_bFanyiPage:encodedValue
                                          :^(BOOL bol, TFHppleElement *shangxicontObjSame) {
                                              
                                              if (!bol&&shangxicontObjSame==nil) {
                                                  sleep(8);
                                                  
                                                  //网关超时  重新来
                                                  [self runCatchOn_shiciFanYiPage:fanyiNum];
                                                  return ;
                                              }
                                              
                                              
                                              if (!bol&&[shangxicontObjSame isKindOfClass:[NSString class]]) {
                                                  //next go
                                                  [self runCatchOn_shiciFanYiPage:fanyiNum+1];
                                                  
                                                  return ;
                                              }
                                              
                                              NSMutableString *richTextPart = [NSMutableString new];//此篇内容text。
                                              
                                              
                                              //----
                                              //===原文  text
                                              //33333 ‘原文’ 下面的所有pTag 都是 shiwenText 判断
                                              //----
                                              
                                              
                                              NSArray *p_All_Tag = [shangxicontObjSame searchWithXPathQuery:@"//p"];
                                              
                                              NSArray *text_All_Tag = [shangxicontObjSame childrenWithTagName:@"text"];
                                              
                                              NSMutableArray *p_All_Tag_mutable = [NSMutableArray arrayWithArray:p_All_Tag];
                                              
                                              
                                              //content 在div son2!!!!!!!!!!!!!!!!!!!!!
                                              {
                                                  NSArray *childArr = [shangxicontObjSame childrenWithTagName:@"text"];
                                                  
                                                  for (TFHppleElement *eleX in childArr) {
                                                      
                                                      //==移除作者 那行p
                                                      TFHppleElement *p_oneAuthorName = [p_All_Tag_mutable firstObject];
                                                      if ([p_oneAuthorName.text isEqualToString:eleX.content]) {
                                                          continue;
                                                      }
                                                      
                                                      NSString *clearB = [eleX.content clear_stringWrong_space];
                                                      
                                                      if ([clearB hasPrefix:@"参考资料"]) {
                                                          continue;
                                                      }
                                                      if ([clearB hasPrefix:@"本站"]) {
                                                          continue;
                                                      }
                                                      if ([clearB hasPrefix:@"作者"]) {
                                                          continue;
                                                      }
                                                      if ([clearB hasPrefix:@"本页内容整理自网络"]) {
                                                          continue;
                                                      }
                                                      
                                                      
                                                      if ([clearB length]>2) {
                                                          
                                                          [richTextPart appendString:@"\n　　"];
                                                          [richTextPart appendString:clearB];
                                                      }
                                                  }
                                                  
                                              }
                                              
                                              //存在 p_content!!!!!!!!!!!!!!!!!
                                              {
                                                  //内容在p content中
                                                  for (TFHppleElement *eleP_content in p_All_Tag_mutable) {
                                                      
                                                      //==针对 内部<strong>标签！！！！！！！！！！！！
                                                      NSString *strong_str ;
                                                      if ([[eleP_content searchWithXPathQuery:@"//strong"] count]==1) {
                                                          
                                                          TFHppleElement *ele_strong = [[eleP_content searchWithXPathQuery:@"//strong"] firstObject];
                                                          
                                                          strong_str = [NSString stringWithFormat:@"**%@**",ele_strong.content];
                                                          
                                                          [richTextPart appendString:@"\n"];
                                                          [richTextPart appendString:strong_str];
                                                      }
                                                      //==针对 内部<a>标签！！！！！！！！！！！！
                                                      NSString *a_str ;
                                                      if ([[eleP_content searchWithXPathQuery:@"//a"] count]==1) {
                                                          
                                                          TFHppleElement *ele_a = [[eleP_content searchWithXPathQuery:@"//a"] firstObject];
                                                          
                                                          a_str = [NSString stringWithFormat:@"**%@**",ele_a.content];
                                                          
                                                          if ([a_str isEqualToString:@"**本站**"]) {
                                                              continue;
                                                          }
                                                          
                                                          [richTextPart appendString:a_str];
                                                      }
                                                      
                                                      
                                                      NSArray *childArr_p = [eleP_content childrenWithTagName:@"text"];
                                                      
                                                      for (TFHppleElement *eleX in childArr_p) {
                                                          
                                                          //===针对p 内部的 textNode
                                                          NSArray *childArrTxt = [eleX childrenWithTagName:@"text"];
                                                          for (TFHppleElement *eleX in childArrTxt) {
                                                              
                                                              NSString *clearB = [eleX.content clear_stringWrong_space];
                                                              if ([clearB length]>2) {
                                                                  
                                                                  [richTextPart appendString:@"\n　　"];
                                                                  [richTextPart appendString:clearB];
                                                              }
                                                          }
                                                          
                                                          NSString *clearSpace = eleX.content ;
                                                          
                                                          if ([clearSpace hasPrefix:@"作者"]) {
                                                              continue;
                                                          }
                                                          if ([clearSpace hasPrefix:@"参考资料"]) {
                                                              continue;
                                                          }
                                                          if ([clearSpace hasPrefix:@"本页内容整理自网络"]) {
                                                              continue;
                                                          }
                                                          if ([clearSpace hasPrefix:@"免费发布仅供学习参考"]) {
                                                              continue;
                                                          }
                                                          
                                                          if ([clearSpace length]>2) {
                                                              
                                                              [richTextPart appendString:@"\n"];
                                                              [richTextPart appendString:clearSpace];
                                                          }
                                                      }
                                                      
                                                      
                                                  }
                                              }
                                              
                                              
                                              
                                              
                                              
                                              //===key 检索数据库中是否已经存入这篇  booknum
                                              
                                              NSString *whereStr = [NSString stringWithFormat:@"shiwenFanyiNum = %ld",fanyiNum];
                                              
                                              NSMutableArray *mArr = [shiciFanyiObj searchWithWhere:whereStr
                                                                                           orderBy:nil
                                                                                            offset:0
                                                                                             count:5] ;
                                              
                                              
                                              if ([mArr count]==0) {
                                                  
                                                  NSLog(@"不应该进来");
//                                                  shiciFanyiObj *fanyiNew =({
//                                                      
//                                                      shiciFanyiObj *fanyiTemp = [shiciFanyiObj new];
//                                                      
//                                                      fanyiTemp.poetryFanyiText  = richTextPart;
//                                                      
//                                                      fanyiTemp;
//                                                      
//                                                  });
//                                                  
//                                                  if ([fanyiNew saveToDB]) {
//                                                      
//                                                      NSLog(@"bookV_fanyi_num on %@ \n %ld ",uriStr,fanyiNum);
//                                                  }
                                              }
                                              else if ([mArr count]==1)
                                              {//已经存在一条。 正常情况。
                                                  shiciFanyiObj *fanyiObjY= (shiciFanyiObj *)[mArr firstObject];
                                                  
                                                  if (![fanyiObjY.poetryFanyiText isEqualToString:richTextPart]) {
                                                      fanyiObjY.poetryFanyiText = richTextPart;
                                                      
                                                      [fanyiObjY saveToDB];
                                                      
                                                      NSLog(@"update bookV_fanyi_num on %@ \n %ld ",uriStr,fanyiNum);
                                                  }
                                                  
                                              }
                                              else if ([mArr count]>1)
                                              {// 不应该出现  两条一样的数据！！
                                                  NSLog(@" 不应该出现  两条一样的数据！！");
                                              }
                                              
                                              {
                                                  NSLog(@"shici_fanyi_num %ld get finish",fanyiNum);
                                                  
                                                  [self runCatchOn_shiciFanYiPage:fanyiNum+1];
                                              }
                                          }];
}


/******************/
//获取book封面地址
-(NSMutableArray *)get_poetHead_lists{
    
    if (self.mArr_poetHead==nil) {
        
        NSString *whereis = [NSString stringWithFormat:@"poetHeadUrl != \'\'"];
        
        NSMutableArray *all_book_objs_marr = [shiciPoetObj searchColumn:@"poetHeadUrl"
                                                                   where:whereis
                                                                 orderBy:@"rowid"
                                                                  offset:0
                                                                   count:1000];
        
        
        NSMutableArray *clear_mArr = [self oneIt_mArr:all_book_objs_marr];
        
        self.mArr_poetHead = clear_mArr;
    }
    
    
    return self.mArr_poetHead;
    
    
}

-(void)loop_download_poetHead:(int)index
{
    NSMutableArray *mArr_list = [self get_poetHead_lists];
    
    NSString *poetHeadUrl;
    
    if (index < [mArr_list count]){
        
        poetHeadUrl = [mArr_list objectAtIndex:index];
        
    }
    else
    {
        return;
    }
    
    
    
    NSURL *url_poetHead = [NSURL URLWithString:[NSString stringWithFormat:@"http://so.gushiwen.org%@",poetHeadUrl]];
    
    [[SDWebImageManager sharedManager] downloadWithURL:url_poetHead
                                               options:0
                                              progress:^(NSUInteger receivedSize, long long expectedSize) {
                                                  
                                              }
                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished) {
                                                 
                                                 if (finished) {
                                                     
                                                     if ([self storege_poethead_2_file:image :poetHeadUrl]) {
                                                         [self loop_download_poetHead:index+1];
                                                     }
                                                     else {
                                                         [self loop_download_poetHead:index+1];
                                                     }
                                                 }
                                                 
                                                 
                                             }];
    
}

-(BOOL)storege_poethead_2_file:(UIImage *)m_imgFore
                                :(NSString *)poetHeadUrl
{
    NSString *authorHead = [[poetHeadUrl  componentsSeparatedByString:@"/"] lastObject];
    
    NSData *imagedata = UIImageJPEGRepresentation(m_imgFore,1.0);
    
    
    
    NSString* bookcover_path = [NSHomeDirectory() stringByAppendingPathComponent:@"poetHead"];
    
    NSString *savedImagePath=[bookcover_path stringByAppendingPathComponent:authorHead];
    
    BOOL bol = [imagedata writeToFile:savedImagePath atomically:YES];
    
    
    if (!bol) {
        NSLog(@"保存失败");
    }
    else
    {
        NSLog(@"保存成功");
    }
    return bol;
}
@end


