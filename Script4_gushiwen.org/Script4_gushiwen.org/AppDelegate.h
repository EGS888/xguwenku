//
//  AppDelegate.h
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-4.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

