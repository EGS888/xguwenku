//
//  MingjuMgr.m
//  Script4_gushiwen.org
//
//  Created by EGS on 15-2-9.
//  Copyright (c) 2015年 EGS. All rights reserved.
//

#import "MingjuMgr.h"
#import "ApiClient_Spider4Gushiwen.h"
#import "TFHpple.h"
#import "NSString+Extensions.h"
#import "NSMutableArray+mArrHelper.h"

#import "mingjuObj.h"

@implementation MingjuMgr

+(MingjuMgr *)shareClient
{
    static MingjuMgr *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[MingjuMgr alloc]  init];
    });
    
    return _sharedClient;
}


//启动catch。====》整体获取
-(void)runCatchOn:(NSInteger)currPage
                 :(NSInteger)finishPage
{

        
        NSString *uriStr = [NSString stringWithFormat:@"/mingju/Default.aspx?p=%ld&c=&t=",currPage];
        
        
        NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [HttpVerClient GetHTML_Mingju:encodedValue
                                     :^(BOOL bol, id responseData) {
                                         
                                         for (TFHppleElement *element in responseData) {
                                             
                                             NSArray *contentArr = [element searchWithXPathQuery:@"//a"];
                                             
                                             //名句 text
                                             TFHppleElement *mingju = [contentArr objectAtIndex:0];
                                             NSString *MingjuTxt = mingju.text;
                                             NSLog(@"%@",[MingjuTxt cutSpace]);
                                             
                                             
                                             //名句 num
                                             NSString *rawNum = [[mingju attributes] objectForKey:@"href"];
                                             NSString * mingjuN_str =  [rawNum get_num_fromTxt];
                                             
                                             //名句 作者 、 诗文title 、 诗文num
                                             TFHppleElement *zuozhe = [contentArr objectAtIndex:1];
                                             NSString *rawNum2 = [[zuozhe attributes] objectForKey:@"href"];
                                             NSString * shiwenN_str =  [rawNum2 get_num_fromTxt];
                                             
                                             NSString *author_poetryTitle = zuozhe.text;
                                             
                                             NSArray *a_p_splite = [author_poetryTitle componentsSeparatedByString:@"《"];
                                             
                                             NSString *author = [[a_p_splite objectAtIndex:0] cutSpace];
                                             NSString *poetryTitle = [[a_p_splite objectAtIndex:1] cutCharacter_1];
                                             
                                             mingjuObj *mingjuO = ({
                                                 mingjuObj *mingjuTemp = [mingjuObj new];
                                                 
                                                 mingjuTemp.mingjuNum = [mingjuN_str intValue];
                                                 mingjuTemp.shiwenNum = [shiwenN_str intValue];
                                                 mingjuTemp.mingjuText = MingjuTxt;
                                                 mingjuTemp.poetryTitle = poetryTitle;
                                                 mingjuTemp.poetName = author;
                                                 mingjuTemp;
                                             });
                                             
                                             [mingjuO saveToDB];//同步存入吧！！
                                             
                                         }
                                         
                                         if (currPage<finishPage
                                             &&[responseData count]>0) {
                                             [self runCatchOn:currPage+1 :finishPage];
                                         }
                                         else
                                         {
                                             NSLog(@"mingju get finish");
                                         }
                                     }];
 
    
    
}


-(void)loopTag4Mingju:(NSInteger)indexA
                     :(NSInteger)indexB
{
    NSArray *cateA  = @[@"抒情",@"四季",@"山水",@"天气",@"人物",@"人生",@"生活",@"节日",@"动物",@"植物",@"食物"];
    NSArray *cateB1 = @[@"爱情",@"友情",@"离别",@"思念",@"思乡",@"伤感",@"孤独",@"闺怨",@"悼亡",@"怀古",@"爱国",@"感恩"];
    NSArray *cateB2 = @[@"春天",@"夏天",@"秋天",@"冬天"];
    NSArray *cateB3 = @[@"庐山",@"泰山",@"江河",@"长江",@"黄河",@"西湖",@"瀑布"];
    NSArray *cateB4 = @[@"写风",@"写云",@"写雨",@"写雪",@"彩虹",@"太阳",@"月亮",@"星星"];
    NSArray *cateB5 = @[@"女子",@"父亲",@"母亲",@"老师",@"儿童"];
    NSArray *cateB6 = @[@"励志",@"哲理",@"青春",@"时光",@"梦想",@"读书",@"战争"];
    NSArray *cateB7 = @[@"乡村",@"田园",@"边塞",@"写桥"];
    NSArray *cateB8 = @[@"春节",@"元宵节",@"寒食节",@"清明节",@"端午节",@"七夕节",@"中秋节",@"重阳节"];
    NSArray *cateB9 = @[@"写鸟",@"写马",@"写猫"];
    NSArray *cateB10 = @[@"梅花",@"梨花",@"桃花",@"荷花",@"菊花",@"柳树",@"叶子",@"竹子"];
    NSArray *cateB11 = @[@"写酒",@"写茶",@"荔枝"];
    
    NSArray *cateB_arr = @[cateB1,cateB2,cateB3,cateB4,cateB5,cateB6,cateB7,cateB8,cateB9,cateB10,cateB11];
    
    if (indexA< [cateA count]) {
        
        
        
        if (indexB >= [[cateB_arr objectAtIndex:indexA] count]) {
            
            if (indexA==10) {
                    NSLog(@"finish all");
                
                return;
            }
            
            //B 分类枚举完=== A 前进一步
            NSString *cateStrA = [cateA objectAtIndex:indexA+1];
            NSString *cateStrB = [[cateB_arr objectAtIndex:indexA+1] objectAtIndex:0];
            
            [self runCatchOn:1
                            :cateStrA
                            :cateStrB
                            :indexA+1
                            :0];//loop on cateA B
        }
        else
        {
            NSString *cateStrA = [cateA objectAtIndex:indexA];
            NSString *cateStrB = [[cateB_arr objectAtIndex:indexA] objectAtIndex:indexB];
            
            [self runCatchOn:1
                            :cateStrA
                            :cateStrB
                            :indexA
                            :indexB];//loop on cateA B
        }
        
    }else
    {
        NSLog(@"finish all");
    }
    
}

//启动catch。===》分类情况获取
-(void)runCatchOn:(NSInteger)currPage
                 :(NSString *)categoryA
                 :(NSString *)categoryB
                 :(NSInteger)indexA
                 :(NSInteger)indexB
{
    NSString *uriStr = [NSString stringWithFormat:@"/mingju/Default.aspx?p=%ld&c=%@&t=%@",currPage,categoryA,categoryB];
    
    
    NSString *encodedValue = [uriStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [HttpVerClient GetHTML_Mingju:encodedValue
                                 :^(BOOL bol, id responseData) {
                                     
                                     for (TFHppleElement *element in responseData) {
                                         
                                         NSArray *contentArr = [element searchWithXPathQuery:@"//a"];
                                         
                                         //名句 text
                                         TFHppleElement *mingju = [contentArr objectAtIndex:0];
                                         NSString *MingjuTxt = mingju.text;
                                         NSLog(@"%@",[MingjuTxt cutSpace]);
                                         
                                         
                                         //名句 num
                                         NSString *rawNum = [[mingju attributes] objectForKey:@"href"];
                                         NSString * mingjuN_str =  [rawNum get_num_fromTxt];
                                         
                                         NSString *whereStr = [NSString stringWithFormat:@"mingjuNum = %@",mingjuN_str];
                                         
                                         NSMutableArray *mArr = [mingjuObj searchWithWhere:whereStr orderBy:nil offset:0 count:5] ;//@"rowid = 1"
                                         
                                         if ([mArr count]==1) {
                                             mingjuObj *mingO = (mingjuObj *)[mArr objectAtIndex:0];
                                             
                                             if (mingO.cateA==nil) {
                                                 NSArray *arr = @[categoryA];
                                                 mingO.cateA = [NSMutableArray arrayWithArray:arr];
                                             }
                                             else if ([mingO.cateA count]>0)
                                             {
                                                 
                                                 
                                                 if (![mingO.cateA checkExist:categoryA]) {
                                                     
                                                     NSMutableArray *mArr = [NSMutableArray arrayWithArray:mingO.cateA];
                                                     [mArr addObject:categoryA];
                                                     
                                                     mingO.cateA = [mArr copy];
                                                 }
                                             }
                                             
                                             
                                             if (mingO.cateB==nil) {
                                                 NSArray *arr = @[categoryB];
                                                 mingO.cateB = [NSMutableArray arrayWithArray:arr];
                                             }
                                             else if ([mingO.cateB count]>0)
                                             {
                                                 if (![mingO.cateB checkExist:categoryB]) {
#warning 警惕cateB 这个不是 可变数组
                                                     NSMutableArray *mArr = [NSMutableArray arrayWithArray:mingO.cateB];
                                                     [mArr addObject:categoryB];
                                                     
                                                     mingO.cateB = [mArr copy];
                                                 }
                                             }
                                             
                                             [mingO saveToDB];
                                         }
                                         else
                                         {
                                             NSLog(@"err 不应该有多个");
                                         }
                                         
                                     }
                                     
                                     if ([responseData count]>0) {
                                         [self runCatchOn:currPage+1
                                                         :categoryA
                                                         :categoryB
                                                         :indexA
                                                         :indexB];//loop on page
                                     }
                                     else if ([responseData count]==0)
                                     {
                                         NSLog(@"cate%@ %@ get finish",categoryA,categoryB);
                                         
                                         [self loopTag4Mingju:indexA
                                                             :indexB+1];
                                     }
                                 }];
}


@end
