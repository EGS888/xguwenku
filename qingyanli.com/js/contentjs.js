/**
 * Created by EGS on 15-4-28.
 */


//content-page 页面初始化
$(function(){

    $("#description-text-id #switch-btn-id").click(function(){

        if ($("#main-text-id").hasClass("col-md-7")) {
            //放大：右侧翻译区域

            //正文区域  7--5
            $("#main-text-id").removeClass("col-md-7");
            $("#main-text-id").addClass("col-md-5");
            $("#egs-main-content-id").addClass("egs-weak-content");
            $("#poet-detail-id").addClass("egs-weak-content");

            //翻译区域  5--7
            $("#description-text-id").removeClass("col-md-5");
            $("#description-text-id").addClass("col-md-7");
            $("#description-text-id").addClass("egs-focusdetail-font");

            //切换按钮，样式切换
            $(this).addClass("active");
            $("#switch-btn-id .fa-expand").addClass("fa-compress");
        }
        else {

            //缩小：右侧翻译区域

            //正文区域  5--7
            $("#main-text-id").removeClass("col-md-5");
            $("#main-text-id").addClass("col-md-7");
            $("#egs-main-content-id").removeClass("egs-weak-content");
            $("#poet-detail-id").removeClass("egs-weak-content");

            //翻译区域  7--5
            $("#description-text-id").removeClass("col-md-7");
            $("#description-text-id").addClass("col-md-5");
            $("#description-text-id").removeClass("egs-focusdetail-font");

            //切换按钮，样式切换
            $(this).removeClass("active");
            $("#switch-btn-id .fa-expand").removeClass("fa-compress");
        }

    });

});