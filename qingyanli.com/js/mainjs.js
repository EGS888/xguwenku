

//main io 页面初始化
$(function(){

    //诗词全集btnNav==>
    $("#nav-header-id-shici").click(function(){

        $(this).addClass("active");
        $("#nav-header-id-book").removeClass("active");

        //诗词名句 -- 古籍名句
        $("#header-random-text-shici").removeClass("egs-nav-invisible");
        $("#header-random-text-book").addClass("egs-nav-invisible");

        //诗词body -- 古籍body
        $("#mainbody-shici").removeClass("egs-nav-invisible");
        $("#mainbody-book").addClass("egs-nav-invisible");

        //背景色==诗词
        $("body").css("background-color","#f2f5e9");

        //清除
        $("#search-btn-id").removeClass("active");
        $("#search-btn-id .fa-search").removeClass("fa-remove");
        $("#search-box-id-shici").removeClass("active");
        $("#search-box-id-book").removeClass("active");
    });

    //古典书籍btnNav==>
    $("#nav-header-id-book").click(function(){

        $(this).addClass("active");
        $("#nav-header-id-shici").removeClass("active");

        //诗词名句 -- 古籍名句
        $("#header-random-text-shici").addClass("egs-nav-invisible");
        $("#header-random-text-book").removeClass("egs-nav-invisible");

        //诗词body -- 古籍body
        $("#mainbody-shici").addClass("egs-nav-invisible");
        $("#mainbody-book").removeClass("egs-nav-invisible");

        //背景色==书籍
        $("body").css("background-color","rgb(249, 252, 249)");

        //清除
        $("#search-btn-id").removeClass("active");
        $("#search-btn-id .fa-search").removeClass("fa-remove");
        $("#search-box-id-shici").removeClass("active");
        $("#search-box-id-book").removeClass("active");
    });

    $("#search-btn-id").click(function(){

        if($("#mainbody-shici").hasClass("egs-nav-invisible")){
            //书籍

            if($(this).hasClass("active"))
            {

                $(this).removeClass("active");
                $("#search-btn-id .fa-search").removeClass("fa-remove");
                $("#search-box-id-book").removeClass("active");
            }
            else
            {

                $(this).addClass("active");
                $("#search-btn-id .fa-search").addClass("fa-remove");
                $("#search-box-id-book").addClass("active");
            }


        }
        else{

            //诗词
            if($(this).hasClass("active"))
            {

                $(this).removeClass("active");
                $("#search-btn-id .fa-search").removeClass("fa-remove");
                $("#search-box-id-shici").removeClass("active");
            }
            else
            {

                $(this).addClass("active");
                $("#search-btn-id .fa-search").addClass("fa-remove");
                $("#search-box-id-shici").addClass("active");
            }
        }




    });

});